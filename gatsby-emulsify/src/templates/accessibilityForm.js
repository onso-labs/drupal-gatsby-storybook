import React from 'react'
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { Layout } from '../../stories/04-templates/layout.stories';
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { CustomForm } from '../../stories/03-organisms/custom-form/custom-form.stories';
import { SearchForm } from '../../stories/03-organisms/search-form/search-form.stories';
import { EmailForm } from '../../stories/03-organisms/email-form/email-form.stories';
import { AskYourQuestionTop } from '../../stories/03-organisms/ask-your-question-top/ask-your-question-top.stories';
import { AffiliationForm } from '../../stories/03-organisms/affiliation-form/affiliation-form.stories';
class Form extends React.Component {

  constructor(props) {
    super(props);
    this.state = { show: false, title: '', question: '', categoryitem: this.props.pageContext.element[0].tid, btn: "Submit", error: false }
  }

  render() {
    return (
      <Layout
        pageTitle={
          <PageTitle PageTitle="Accessibility Form" />
        }
        sidebar={
          <>
            <CTA />
          </>
        }
        content={
          <>
            <AskYourQuestionTop />
            <CustomForm />
            <SearchForm />
            <EmailForm />
            <AffiliationForm />
          </>
        } >
      </Layout>
    )
  }
}

export default Form;
