/* Theme of the week page */
import React, { Component } from "react";
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { SecondaryMenu } from '../../stories/03-organisms/secondary-menu/secondary-menu.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { linkList, blockTitle } from '../common/data.js';
import { ThemeWeekLandingContent } from '../../stories/03-organisms/theme-week-landing-content/theme-week-landing-content.stories';

export default class IndexPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      activeKey: "1",
      load: false
    };

  }

  /* Setting panel activation key */
  onKeyChange = (activeKey) => {
    if (activeKey != undefined) {
      this.setState({
        activeKey,
      }, () => {
        window.scrollTo(0, 0);
        setTimeout(() => {
          var top = document.getElementsByClassName('theme-active')[0].getBoundingClientRect().top;
          console.log(top);
          window.scrollTo(0, top);
        }, 500)
      });
    }

  };
  componentDidMount = () => {
    console.log("in theme week")
    this.setState({ load: true })
    setTimeout(() => {
      this.scroll();
    }, 100)
  }
  componentDidUpdate() {
    console.log("in did update")
    //  this.scroll();
  }
  componentWillMount = () => {
    //this.scroll();
  }
  scroll = () => {
    if (typeof window !== `undefined`) {
      const queryString = window.location.search;
      if (queryString != "") {
        const urlParams = new URLSearchParams(queryString);
        let theme = urlParams.get('theme');
        theme = theme.trim();
        const themeData = this.props.pageContext.element;
        let index = themeData.findIndex(o => o.entityLabel.trim() == theme);
        if (index > 0) {
          this.setState({
            activeKey: index + 1
          }, () => {
            window.scrollTo(0, 0);
            setTimeout(() => {
              var top = document.getElementsByClassName('theme-active')[0].getBoundingClientRect().top;
              console.log(top);
              window.scrollTo(0, top);
            }, 500)
          });
        }
      }
    }
  }
  render() {
    const themeData = this.props.pageContext.element
    console.log(">>>>theme", themeData)
    return (
      <>
        {this.state.load == true && (
          <Layout
            crumbLabel="Themes of the Week"
            pageTitle={
              <PageTitle PageTitle="Themes of the week" />
            }
            sidebar={
              <>
                <CTA />
                <SecondaryMenu />
                <SecondaryMenu title={blockTitle} list={linkList} />
              </>
            }

            content={
              <>
                <ThemeWeekLandingContent themeWeekContent={themeData} activeKey={this.state.activeKey} onKeyChange={this.onKeyChange} />
              </>
            }

          >


          </Layout >
        )}

      </>
    )
  }

}


