/* Recent Q&A page */
import React, { Component } from "react";
import { NewQAContent } from '../Components/NewQAContent';
import { Layout } from "../Components/layout"
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { SecondaryMenu } from '../../stories/03-organisms/secondary-menu/secondary-menu.stories';
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { linkList, blockTitle } from '../common/data.js'
export default class HealthAnswersTemplate extends Component {
  render() {
    const newQAData = this.props.pageContext.newQA;
    const updatedData = this.props.pageContext.updatedQA;
    var newupdatedData = updatedData.map(v => ({
      fieldLastReviewedOn: v.fieldLastReviewedOn, created: v.created, entityUrl: v.entityUrl,
      nid: v.nid, status: v.status, title: v.title
    }));
    const combineData = newQAData.concat(newupdatedData).filter((v, i, a) => a.findIndex(t => (t.nid === v.nid)) === i);
    combineData.sort((a, b) => b.fieldLastReviewedOn.date.localeCompare(a.fieldLastReviewedOn.date));
    return (
      <React.Fragment>
        <Layout
          crumbLabel="Answered questions"
          link="/"
          title="Recent Q&As"
          pageTitle={
            <>
              <PageTitle PageTitle="Recent Q&As" />
            </>
          }
          sidebar={
            <>
              <CTA />
              <SecondaryMenu title={blockTitle} list={linkList} />
            </>
          }
          content={
            <>
              <NewQAContent data={combineData} title="newQa" />
            </>
          } />
      </React.Fragment>
    );
  }
}

