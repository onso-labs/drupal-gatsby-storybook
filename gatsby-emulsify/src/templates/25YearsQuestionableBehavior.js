import React from 'react';
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { SecondaryMenu } from '../../stories/03-organisms/secondary-menu/secondary-menu.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { Card } from '../../stories/02-molecules/card/card.stories';
import { SocialShare } from '../../stories/02-molecules/menus/socialshare/social-share-menu.stories';
import { FactSheetResources } from '../../stories/03-organisms/fact-sheet-resources/fact-sheet-resources.stories';
import config from '../config';
import axios from 'axios';
import { SuccessMessage } from '../../stories/02-molecules/success-message/success-message.stories'
export default class YearsQuestionableBehavior extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            DESIRED_TEXT: props.location.state != null ? props.location.state.DESIRED_TEXT : ''
        }
    }
    componentWillUnmount = () => {
        if (this.props.location.state != null) {
            window.history.replaceState(null, '')
        }
    }
    componentDidMount = async () => {
        if (this.props.location.state != null) {
            window.history.replaceState(null, '')
        }

    }
    render() {
        const data = this.props.pageContext.element.results[0];
        let mailurl = "/printmail/" + data.nid + ""
        let pdfurl = `${config.REST_API_URL}/node/${data.nid}/printable/pdf`
        return (
            <Layout
                crumbLabel="25 Years of Questionable Behavior"
                pageTitle={<>
                    <PageTitle PageTitle="25 Years of Questionable Behavior" />
                </>
                } newQA={
                    <>
                        <div className="success-wrap-nobackground">
                            {this.state.DESIRED_TEXT != "" && this.state.DESIRED_TEXT != null && (
                                <SuccessMessage message={this.state.DESIRED_TEXT} />
                            )}
                        </div>
                    </>
                }
                sidebar={
                    <>
                        <CTA />
                    </>
                }
                content={
                    <>
                        <div class="page-content">
                            <div dangerouslySetInnerHTML={{ __html: data.body.value }} />
                        </div>
                        <SocialShare url={"/print/" + data.nid + ""} mailUrl={mailurl} pdfUrl={pdfurl} />
                    </>
                } >
            </Layout>
        );
    }
}
