import React, { Component } from "react";
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { SecondaryMenu } from '../../stories/03-organisms/secondary-menu/secondary-menu.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { linkList, blockTitle } from '../common/data.js';
import { QuestionBlock } from '../../stories/02-molecules/question-block/question-block.stories';
import { AnswerBlock } from '../../stories/02-molecules/answer-block/answer-block.stories';

export default class IndexPage extends React.Component {


  render() {

    const themeData=this.props.pageContext.element
    
    return (
        <Layout
          crumbLabel="Health Answers"
          link="/health-answers/updates"
          title={themeData.title}
        pageTitle={
          <PageTitle PageTitle={themeData.title} />
        }
        sidebar={
          <>
            <CTA />
            <SecondaryMenu />
            <SecondaryMenu title={blockTitle} list={linkList} />
          </>
        }
    
        content={
          <>
            <QuestionBlock themeQuestions={themeData.fieldQuestion}/>
            <AnswerBlock />
          </>
        }
    
      >
    
    
      </Layout >
      )
  }

}


