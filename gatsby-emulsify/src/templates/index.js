/* Home page */
import React from "react";
import { ThemeWeekHero } from '../../stories/03-organisms/theme-week-hero/theme-week-hero.stories';
import { Layout } from "../Components/layout"
import { WeeklyPoll } from '../../stories/03-organisms/weekly-poll/weekly-poll.stories';
import { Testimonial } from '../../stories/03-organisms/testimonial/testimonial.stories';
import { AboutAlice } from '../../stories/03-organisms/alice-section/alice-section.stories'
import { NewHealthTopicSection } from '../../stories/03-organisms/new-health-topic-section/new-health-topic-section.stories';
import { Quizzes } from '../../stories/03-organisms/quizzes/quizzes.stories';
import config from '../config';
import axios from 'axios';
import { QuestionBlock } from '../../stories/02-molecules/question-block/question-block.stories';
import { WeeklyPollResult } from '../../stories/03-organisms/weekly-poll-result/weekly-poll-result.stories';
import { Links } from '../../stories/01-atoms/link/link.stories';
import { SuccessMessage } from '../../stories/02-molecules/success-message/success-message.stories'
export default class IndexPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      field_testimonial: '',
      health_topic: {},
      weeklyData: {}, cid: 0,
      load: false,
      weeklyPolls: [],
      DESIRED_TEXT: props.location.state != null ? props.location.state.DESIRED_TEXT : ''
    }
  }

  componentDidMount = async () => {
    if (this.props.location.state != null) {
      window.history.replaceState(null, '')
    }
    await this.getData();
    await this.setState({ load: true, weeklyPolls: this.props.pageContext.poll })
    this.state.weeklyPolls[0].cid = 0
  }
  componentWillUnmount = () => {
    if (this.props.location.state != null) {
      window.history.replaceState(null, '')
    }
  }
  componentWillReceiveProps = async () => {
    await this.getData();
    await this.setState({ load: true, weeklyPolls: this.props.pageContext.poll })
    this.state.weeklyPolls[0].cid = 0
  }

  getData = () => {
    /* API call for testimonial data of home page */
    axios(`${config.REST_API_URL}testimonial_rotator?_format=json`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    })
      .then((data) => {
        this.setState({ field_testimonial: data.data[0].field_testimonial })
      });
    /* API call for health answer block of home page */
    axios(`${config.REST_API_URL}api/health-topic-home-page?_format=json`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    })
      .then((data) => {
        this.setState({ health_topic: data.data[0] })
      });
  }
  /* Called when click on weekly poll radio button click */
  onRadioClick = async (pid, cid, e) => {
    if (this.state.weeklyPolls.length > 0) {
      this.state.weeklyPolls[0].cid = cid
    }
    this.setState({ cid: cid })
  }

  /* call when click on vote button */
  onvoteclick = async (pid, e) => {
    if (e != undefined) {
      e.preventDefault();
    }
    var variables = {
      "chid": this.state.cid,
      "pid": pid,
      "uid": "1"
    }
    let resultMessage = [];
    if (this.state.weeklyData != undefined) {
      resultMessage = this.state.weeklyData['resultMessage']
    }
    //poll[0].entityId
    await axios(`${config.REST_API_URL}rest-api/gaa_poll_vote_mutation?_format=json`, {
      method: 'POST',
      data: variables,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    })
      // .then(r => r.json())
      .then((data) => {
        console.log('data returned:', data);
        let body = {
          "id": data.data
        }
        axios(`${config.REST_API_URL}rest-api/gaa_poll_votes?_format=json`, {
          method: 'POST',
          data: body,
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },

        })
          .then((data) => {
            if (resultMessage == undefined) {
              resultMessage = [];
            }
            resultMessage.push("Your vote is being tallied. Meanwhile, here's how other readers voted.");
            data.data.resultMessage = resultMessage.slice(0, 1);
            this.setState({ weeklyData: data.data })
          }).catch(err => {
            console.log("err", err);
          })

      }).catch(err => {
        console.log("err", err);
      })

  }
  render() {
    const newqaData = this.props.pageContext.newqa
    const themeData = this.props.pageContext.theme
    const lastupdateData = this.props.pageContext.lastupdated
    const quizzData = this.props.pageContext.quizz
    const testimonial = this.props.pageContext.testimonial
    const poll = this.props.pageContext.poll
    const allaboutalice = this.props.pageContext.allaboutalice
    const homepagehealthanswer = this.props.pageContext.homepagehealthanswer
    console.log(lastupdateData)
    return (
      <>
        {this.state.load == true && (
          <Layout

            update={lastupdateData[0].changed}
            newQA={
              <>
              <div className="success-wrap">
                {this.state.DESIRED_TEXT != "" && this.state.DESIRED_TEXT != null&&(
                  <SuccessMessage message={this.state.DESIRED_TEXT} />
                )}
              <NewHealthTopicSection newqaLists={newqaData.length > 0 ? newqaData[0].fieldHealthAnswer : []} />
              </div>
              </>
            }

            
            sidebar={
              <>
                <WeeklyPoll weeklyPolls={this.state.weeklyPolls.length > 0 ? this.state.weeklyPolls[0] : []} pid={this.state.weeklyPolls.length > 0 ? this.state.weeklyPolls[0].entityId : 0}
                  radiosData={this.state.weeklyPolls.length > 0 ? this.state.weeklyPolls[0].choice.length > 0 ? this.state.weeklyPolls[0].choice : [] : []} show={true}
                  onchange={this.onRadioClick} onVoteClick={this.onvoteclick} />

                {
                  Object.keys(this.state.weeklyData).length !== 0 ? <div>
                    <WeeklyPollResult pollResult={this.state.weeklyData} resultDatas={this.state.weeklyData.field_poll_options} />
                  </div> : ''
                }
                <div className="weeklypoll__more">

                  <Links linkTextValue="More polls" linkRedirectTo="/polls" />

                </div>
                <Quizzes quiz={quizzData} />
              </>
            }
            content={
              <>
                <QuestionBlock themeQuestions={homepagehealthanswer[0]} type="health_topic" />
                <AboutAlice statsTitle={allaboutalice[0].title} statsBody={allaboutalice[0].body.value} />
                <Testimonial userData={this.state.field_testimonial != null ? this.state.field_testimonial : ''} />
                {themeData.length > 0 ? <ThemeWeekHero themes={themeData.length > 0 ? themeData[0] : {}} themeLinks={themeData.length > 0 ? themeData[0].queryFieldHealthAnswer?.entities : []} /> : ''}

              </>
            } >
          </Layout>
        )}

      </>
    )
  }

}


