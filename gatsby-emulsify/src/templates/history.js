/* Go ask alice history page */
import React, { Component } from "react";
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { SecondaryMenu } from '../../stories/03-organisms/secondary-menu/secondary-menu.stories';
import { QuestionBlock } from '../../stories/02-molecules/question-block/question-block.stories';
import { AnswerBlock } from '../../stories/02-molecules/answer-block/answer-block.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { linkList, blockTitle } from '../common/data.js'
import { GAAHistory } from '../../stories/03-organisms/gaa-history/gaa-history.stories';

export default class HealthAnswersTemplate extends Component {
  render() {
    const historyData = this.props.pageContext;

    return (
      <Layout
        crumbLabel="About Alice!"
        link="/about-alice/all-about"
        title="Go Ask Alice! History"
        pageTitle={
          <PageTitle PageTitle="Go Ask Alice! History" />
        }
        sidebar={
          <>
            <CTA />
            <SecondaryMenu />
            <SecondaryMenu title={blockTitle} list={linkList} />
          </>
        }

        content={
          <>
            <GAAHistory historyData={historyData.element} />
          </>
        }

      >
      </Layout >
    );
  }
}

