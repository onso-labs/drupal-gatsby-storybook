/* Footer comments and correction page */
import React, { Component } from "react";
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { linkList, blockTitle } from '../common/data';
import { SecondaryMenu } from '../../stories/03-organisms/secondary-menu/secondary-menu.stories';
import { CustomForm } from '../../stories/03-organisms/custom-form/custom-form.stories'
import config from '../config';
import axios from 'axios';
import { navigate } from 'gatsby';
export default class IndexPage extends React.Component {
  constructor() {
    super();
    this.state = {
      error: false,
      email: '',
      message: '',
      btn: "Submit",
      reCaptchaResponse: '',
      reCaptchaError:false
    }
  }

  /* Form Submit */
  submit = (e) => {
    e.preventDefault()
    if (this.state.email.trim() == '' || this.state.message.trim() == '' ) {
      this.setState({ error: true })
    }else if(this.state.reCaptchaResponse.trim()== '') {
      this.setState({ reCaptchaError: true })
    }  else {
      this.setState({ btn: "Please Wait", error: false, reCaptchaError: false })
      let body = {
        "type": "feedback",

        "field_email": [this.state.email],
        "body": {
          "value": this.state.message
        }
      }
      axios(`${config.REST_API_URL}node/?_format=json`, {
        method: 'POST',
        data: body,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      })
        .then((data) => {
          console.log('data returned:', data)
          if (data.status == 201) {
            this.setState({
              error: false,
              btn: "Submit",
              email: '',
              message: '',
              required: false,
              reCaptchaResponse: '',
              reCaptchaError:false
            })
            navigate('/', {
              state: { DESIRED_TEXT: "Thank you for submitting a comment." },
            });
          }
        })
    }
  }
  /* Setting value of each field of form */
  handleChange = (e, type) => {
    this.setState({ [type]: e.target.value })
  }
  /* Setting captcha value*/
  verifyCallback = (value) => {
    this.setState({
      reCaptchaResponse: value
    });
  }
  render() {
    let data
    const element = this.props.pageContext.element
    element.map((item, i) => {
      if (item.fieldFormtype == "feedback") {
        data = item
      }
    })
    return (
      <Layout
        crumbLabel="Comments & Corrections"
        pageTitle={
          <PageTitle PageTitle="Comments & Corrections" />
        }
        sidebar={
          <>
            <CTA />
            <SecondaryMenu />
            <SecondaryMenu title={blockTitle} list={linkList} />
          </>
        }
        content={
          <>
            <div className="page-content">
              <div dangerouslySetInnerHTML={{ __html: data !== undefined ? data.body ? data.body.value : '' : '' }} />
            </div>
            <CustomForm type="comment" email={this.state.email} message={this.state.message} verifyCallback={this.verifyCallback} handlechange={this.handleChange} handleSubmit={this.submit} btnName={this.state.btn} error={this.state.error}reCaptchaError={this.state.reCaptchaError} />
          </>
        } >
      </Layout>)
  }

}


