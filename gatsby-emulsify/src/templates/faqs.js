/* Faqs Page */
import React, { Component } from "react";
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { FAQsContent } from '../../stories/03-organisms/faq-content/faq-content.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { SocialShare } from '../../stories/02-molecules/menus/socialshare/social-share-menu.stories';
import { SecondaryMenu } from '../../stories/03-organisms/secondary-menu/secondary-menu.stories';
import { linkList, blockTitle } from '../common/data.js';
import config from '../config';
import { SuccessMessage } from '../../stories/02-molecules/success-message/success-message.stories';
export default class Faqs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeKey: "0",
      DESIRED_TEXT: props.location.state != null ? props.location.state.DESIRED_TEXT : ''
    };

  }
  componentWillUnmount = () => {
    if (this.props.location.state != null) {
      window.history.replaceState(null, '')
    }
  }
  componentDidMount = async () => {
    if (this.props.location.state != null) {
      window.history.replaceState(null, '')
    }
  }
  /* Setting panel activation key */
  onKeyChange = (activeKey) => {
    if (activeKey != undefined) {
      if (this.state.activeKey == activeKey) {
        activeKey = null
      }
      this.setState({
        activeKey
      }, () => {
        if (typeof window !== `undefined`) {
          //id={`faq${panelkey}`}
          let product = "faq" + activeKey;
          product = product.trim();
          console.log(product);
          if (document.getElementById(product) != null) {
            window.scrollTo(0, 0);
            setTimeout(
              () => {
                var top = document.getElementById(product).getBoundingClientRect().top;
                console.log(top);
                window.scrollTo(0, top);
              },
              100
            );
          }

        }
      });

    }

  };
  render() {
    const faqsData = this.props.pageContext.element.length > 0 ? this.props.pageContext.element[0] : [];
    /* Redirect Urls for social share */
    let mailurl = "/printmail/" + faqsData.nid + ""
    let pdfurl = `${config.REST_API_URL}node/${faqsData.nid}/printable/pdf`
    return (
      <Layout
        crumbLabel="About Alice!"
        link="/about-alice/all-about"
        title="FAQs"
        pageTitle={
          <>
            <PageTitle PageTitle="FAQs" />
          </>
        }
        newQA={
         <>
             <div className="success-wrap-nobackground">
                 {this.state.DESIRED_TEXT != "" && this.state.DESIRED_TEXT != null && (
                     <SuccessMessage message={this.state.DESIRED_TEXT} />
                 )}
             </div>
         </>
     }

        sidebar={
          <>
            <CTA />
            <SecondaryMenu />
            <SecondaryMenu title={blockTitle} list={linkList} />
          </>
        }
        content={
          <>
            <FAQsContent faqs={faqsData.fieldFaq} activeKey={this.state.activeKey} onKeyChange={this.onKeyChange} />
            <div class="social-menus">
              {/* <SocialShare url={`${config.REST_API_URL}node/${faqsData.nid}/printable/print` + ""} mailUrl={mailurl} pdfUrl={`
              ${config.REST_API_URL}print/view/pdf/gq_faqs/page_2?`} /> */}
              <SocialShare url={null} mailUrl={null} pdfUrl={null} />
            </div>
          </>
        } >
      </Layout>
    );
  }
}

