/* Health answer social share mail page */
import React, { Component } from "react";
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { linkList, blockTitle } from '../common/data'
import { SecondaryMenu } from '../../stories/03-organisms/secondary-menu/secondary-menu.stories';
import { EmailForm } from '../../stories/03-organisms/email-form/email-form.stories'
import config from '../config';
import axios from 'axios';
import { navigate } from "gatsby";
import { SuccessMessage } from '../../stories/02-molecules/success-message/success-message.stories';
export default class IndexPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            message: '',
            subject: 'Someone has sent you a message from Go Ask Alice!',
            sendto: '',
            disable: true,
            error: false,
            isValid: false,
            emailError: false,
            messageError: false,
            sendtoError: false,
            btnName: "Send Email",
            reCaptchaResponse: '',
            reCaptchaError: false,
            DESIRED_TEXT: props.location.state != null ? props.location.state.DESIRED_TEXT : ''
        }
    }
    componentWillUnmount = () => {
        if (this.props.location.state != null) {
            window.history.replaceState(null, '')
        }
    }
    componentDidMount = async () => {
        if (this.props.location.state != null) {
            window.history.replaceState(null, '')
        }
    }
    /* Setting captcha value*/
    verifyCallback = (value) => {
        this.setState({
            reCaptchaResponse: value
        });
    }

    /* Validate form */
    validate = () => {
        let formIsValid = true;
        let mail = this.state.sendto.replace(/\n/g, ",").split(",");
        let mails = mail;
        let sendtoValidate = [];
        let validateSendTo;
        mails.map(key => sendtoValidate.push(/^(([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})+\s?,)*(([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}))$/.test(key)))
        sendtoValidate.filter(key => (key === false) ? validateSendTo = false : validateSendTo = true)
        if (this.state.email == '') {
            formIsValid = false
            this.setState({ emailError: true, isValid: false })
        } else if (!(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/.test(this.state.email))) {
            formIsValid = false
            this.setState({ emailError: true, isValid: false })
        } else {
            this.setState({ emailError: false, isValid: true })
        }
        if (this.state.message == '') {
            formIsValid = false
            this.setState({ messageError: true, isValid: false })
        } else {
            this.setState({ messageError: false, isValid: true })
        }
        if (this.state.sendto == '') {
            formIsValid = false
            this.setState({ sendtoError: true, isValid: false })
        } else if (!validateSendTo) {
            formIsValid = false
            this.setState({ sendtoError: true, isValid: false })
        } else {
            this.setState({ sendtoError: false, isValid: true })
        }
        if (this.state.subject == '') {
            formIsValid = false
            this.setState({ subjectError: true, isValid: false })
        } else {
            this.setState({ subjectError: false, isValid: true })
        }
        if (this.state.reCaptchaResponse == '') {
            formIsValid = false
        }

        return formIsValid
    }

    /*submit form */
    submit = (e) => {
        e.preventDefault()
        if (this.validate()) {
            this.setState({ btnName: "Please Wait", error: false, reCaptchaError: false })
            let mail = this.state.sendto.replace(/\n/g, ",").split(",");
            let mails = mail;
            let body = {
                "id": this.props.pageContext.element.nid,
                "email": this.state.email,
                "name": this.state.name,
                "recipient": mails,
                "message": this.state.message,
                "subject": this.state.subject
            }
            axios(`${config.REST_API_URL}rest-api/gaa_print_email_content`, {
                method: 'POST',
                data: body,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
            })
                .then((data) => {
                    console.log('data returned:', data)
                    if (data.status == 200) {
                        this.setState({
                            name: '',
                            email: '',
                            message: '',
                            subject: 'Someone has sent you a message from Go Ask Alice!',
                            sendto: '',
                            disable: true,
                            error: false,
                            isValid: false,
                            emailError: false,
                            messageError: false,
                            sendtoError: false,
                            btnName: "Send Email",
                            reCaptchaResponse: "",
                            reCaptchaError: false
                        })
                        navigate(this.props.pageContext.paths, {
                            state: { DESIRED_TEXT: "Thank you for spreading the word about Go Ask Alice!" },
                        });
                    }
                })
        }
    }

    /* Setting value of each field of form */
    handleChange = (e, type) => {
        this.setState({ [type]: e.target.value })
    }

    /* redirect back to the original page once sunmitted */
    redirect = (e) => {
        if (e != undefined) {
            e.preventDefault();
        }
        navigate(this.props.pageContext.paths);
    }

    render() {
        let data
        return (
            <Layout
                crumbLabel=" "
                pageTitle={
                    <PageTitle PageTitle="Send by email" />
                }
                newQA={
                    <>
                        <div className="success-wrap-nobackground">
                            {this.state.DESIRED_TEXT != "" && this.state.DESIRED_TEXT != null && (
                                <SuccessMessage message={this.state.DESIRED_TEXT} />
                            )}
                        </div>
                    </>
                }
                sidebar={
                    <>
                        <CTA />
                        <SecondaryMenu />
                        <SecondaryMenu title={blockTitle} list={linkList} />
                    </>
                }
                content={
                    <>
                        <EmailForm name={this.state.name} email={this.state.email} message={this.state.message} verifyCallback={this.verifyCallback} reCaptchaError={this.state.reCaptchaError} handlechange={this.handleChange} handleSubmit={this.submit} sendto={this.state.sendto} subject={this.state.subject} error={this.state.error} emailError={this.state.emailError} messageError={this.state.messageError} sendtoError={this.state.sendtoError} subjectError={this.state.subjectError} btnName={this.state.btnName} page={this.props.pageContext.title} path={this.props.pageContext.paths} handleRedirect={this.redirect} />
                    </>
                } >
            </Layout>)
    }

}


