import React from 'react'
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { Layout } from '../../stories/04-templates/layout.stories';
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { ThemeWeekLandingContent } from '../../stories/03-organisms/theme-week-landing-content/theme-week-landing-content.stories';
import { ThemeWeekHero } from '../../stories/03-organisms/theme-week-hero/theme-week-hero.stories';
import { QuestionBlock } from '../../stories/02-molecules/question-block/question-block.stories';
import { AnswerBlock } from '../../stories/02-molecules/answer-block/answer-block.stories';

class Form extends React.Component {

  constructor(props) {
    super(props);
    this.state = { show: false, title: '', question: '', categoryitem: this.props.pageContext.element[0].tid, btn: "Submit", error: false }
  }

  render() {
    return (
      <Layout
        pageTitle={
          <PageTitle PageTitle="Accessibility Theme" />
        }
        sidebar={
          <>
            <CTA />
          </>
        }
        content={
          <>
            <ThemeWeekLandingContent />
            <QuestionBlock />
            <AnswerBlock />
            <ThemeWeekHero />
          </>
        } >
      </Layout>
    )
  }
}

export default Form;
