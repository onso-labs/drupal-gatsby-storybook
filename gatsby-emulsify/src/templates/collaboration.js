/* Footer Collaboration Page */
import React, { Component } from "react";
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { linkList, blockTitle } from '../common/data'
import { SecondaryMenu } from '../../stories/03-organisms/secondary-menu/secondary-menu.stories';
import { CustomForm } from '../../stories/03-organisms/custom-form/custom-form.stories';
import config from '../config';
import axios from 'axios';
import { navigate } from 'gatsby';
export default class IndexPage extends React.Component {
  constructor() {
    super();
    this.state = {
      name: '',
      email: '',
      message: '',
      btn: "Submit",
      error: false,
      reCaptchaResponse: '',
      reCaptchaError: false
    }
  }
  /* Setting captcha value*/
  verifyCallback = (value) => {
    this.setState({
      reCaptchaResponse: value
    });
  }
  /* Form Submit */
  submit = (e) => {
    e.preventDefault()

    if (this.state.name.trim() == '' || this.state.email.trim() == '' || this.state.message.trim() == '') {
      this.setState({ error: true })
    }
    else if (this.state.reCaptchaResponse.trim() == '') {
      this.setState({ reCaptchaError: true })
    }
    else {
      this.setState({ btn: "Please Wait", error: false, reCaptchaError: false })
      let body = {
        "type": "collaboration",

        "field_name": [this.state.name],
        "field_email": [this.state.email],
        "body": {
          "value": this.state.message
        }
      }
      axios(`${config.REST_API_URL}node/?_format=json`, {
        method: 'POST',
        data: body,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      })
        .then((data) => {
          console.log('data returned:', data)
          if (data.status == 201) {
            this.setState({
              btn: "Submit",
              name: '',
              email: '',
              message: '',
              error: false,
              reCaptchaResponse: '', reCaptchaError: false
            })
            navigate('/', {
              state: { DESIRED_TEXT: "Thank you for submitting a collaboration request." },
            });
          }
        })
    }
  }

  /* Setting value of each field of form */
  handleChange = (e, type) => {
    this.setState({ [type]: e.target.value })

  }

  render() {
    const element = this.props.pageContext.element
    let data
    element.map((item, i) => {
      if (item.fieldFormtype == "collaboration") {
        data = item
      }
    })
    return (

      <Layout
        crumbLabel="Content Use & Collaboration"
        pageTitle={
          <PageTitle PageTitle="Content Use & Collaboration" />
        }
        sidebar={
          <>
            <CTA />
            <SecondaryMenu />
            <SecondaryMenu title={blockTitle} list={linkList} />
          </>
        }
        content={
          <>
            <div class="page-content">
              <div dangerouslySetInnerHTML={{ __html: data !== undefined ? data.body ? data.body.value : '' : '' }} />
            </div>
            <CustomForm name={this.state.name} email={this.state.email} message={this.state.message} verifyCallback={this.verifyCallback} handlechange={this.handleChange} handleSubmit={this.submit} type="collaboration" btnName={this.state.btn} error={this.state.error} reCaptchaError={this.state.reCaptchaError} />
          </>
        } >
      </Layout>)
  }

}


