/* Header health answer page of each category (alcohol & other drugs, emotional health, etc..)*/
import React, { Component } from "react";
import config from '../config';
import axios from 'axios';
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { CategoryListContent } from '../../stories/03-organisms/category-list-content/category-list-content.stories';
import { SecondaryMenu } from '../../stories/03-organisms/secondary-menu/secondary-menu.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { linkList, blockTitle } from '../common/data.js';
import TreeCollapse from '../Components/categoryTree';
import { SearchForm } from '../../stories/03-organisms/search-form/search-form.stories';
import { navigate } from 'gatsby';
import { Primarybtn } from '../../stories/01-atoms/buttons/button.stories';
import { NoResult } from '../../stories/02-molecules/no-result/no-result.stories';
var arrayToTree = require("array-to-tree");


export default class HealthAnswersTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category: this.props.pageContext.element,
      title: '',
      cat: '',
      data: [],
      defaultExpandedKeys: [],
      defaultSelectedKeys: [],
      defaultCheckedKeys: [],
      search: ''
    }
  }

  componentWillReceiveProps = async () => {
    const params = new URLSearchParams(window.location.search);
    let category = params.getAll('cat')
    let title = params.getAll('title')
    let search = params.get('search')
    this.setState({ search: search == null ? '' : search })
    title = title.map(key => key.replace("AND", "&"))
    this.setState({ title: title.join(', ') })
    console.log("cat", category)
    if (Array.isArray(category)) {
      if (category.length > 0) {
        await this.setState({
          defaultExpandedKeys: category, defaultSelectedKeys: category,
          defaultCheckedKeys: category,
          cat: category,
        })
      } else if (category.length == 0) {
        category = []
        // category = [1]
        await this.setState({
          defaultExpandedKeys: [],
          defaultSelectedKeys: [],
          defaultCheckedKeys: [],
          cat: category,
          title: ""
          // defaultSelectedKeys: category,
          // defaultCheckedKeys: category,
          // cat: category,
          // title: "Alcohol & Other Drugs"
        })
      }

    } else if (category == "" || category == undefined || category == null || category.length == 0) {
      // category = [1]
      category = []
      await this.setState({
        defaultExpandedKeys: [],
        defaultSelectedKeys: [],
        defaultCheckedKeys: [],
        cat: category,
        title: ""
        // defaultSelectedKeys: category,
        // defaultCheckedKeys: category,
        // cat: category,
        // title: "Alcohol & Other Drugs"
      })
    }
    else {
      category = [category]
      await this.setState({
        defaultExpandedKeys: category, defaultSelectedKeys: category,
        defaultCheckedKeys: category,
        cat: category,

      })
    }

    if (category.length == 0) {
      const params = new URLSearchParams(window.location.search);
      let search = params.get('search')
      await this.setState({ search })
    }
    await this.getData(category, search);
  }

  componentDidMount = async () => {

    /* Setting parent of each category */
    let categoryList = this.state.category
    await categoryList.map((item, i) => {
      item.id = item.tid
      item.key = item.tid
      item.title = item.name
      if (item.parent !== null && item.parent !== undefined && Array.isArray(item.parent) == true) {
        if (item.parent[0].entity !== null) {
          let parent = item.parent
          item.parent = parent[0].entity.tid
          item.parent_name = parent[0].entity.entityLabel
        } else {
          item.parent = null
          item.parent_name = null
        }
      } else if (item.parent == null || item.parent == undefined || Array.isArray(item.parent) == true) {
        item.parent = null
        item.parent_name = null
      }
    })

    this.setState({
      category: categoryList,
    })


    /* Get category and title from query string parameter */
    const params = new URLSearchParams(window.location.search);
    let category = params.getAll('cat')
    let title = params.getAll('title')
    let search = params.get('search')
    this.setState({ search: search == null ? '' : search })
    title = title.map(key => key.replace("AND", "&"))
    this.setState({ cat: category, title: title.join(', ') })
    console.log("cat", category)
    if (Array.isArray(category)) {
      if (category.length > 0) {
        this.setState({
          defaultExpandedKeys: category, defaultSelectedKeys: category,
          defaultCheckedKeys: category,
        })
      } else if (category.length == 0) {
        category = []
        this.setState({
          defaultExpandedKeys: [],
          defaultSelectedKeys: [],
          defaultCheckedKeys: [],
          title: ""
          // defaultSelectedKeys: category,
          // defaultCheckedKeys: category,
          // title: "Alcohol & Other Drugs"
        })
      }

    } else if (category == "" || category == undefined || category == null) {
      category = []
      this.setState({
        defaultExpandedKeys: [],
        defaultSelectedKeys: [],
        defaultCheckedKeys: [],
        title: ""
        // defaultSelectedKeys: category,
        // defaultCheckedKeys: category,
        // title: "Alcohol & Other Drugs"
      })
    }
    else {

      category = [category]
      this.setState({
        defaultExpandedKeys: category, defaultSelectedKeys: category,
        defaultCheckedKeys: category,
      })
    }


    if (category.length == 0) {
      const params = new URLSearchParams(window.location.search);
      let search = params.get('search')
      this.setState({ search })
    }
    await this.getData(category, search);

  }

  /* API call on search,any category selection/deselection */
  getData = (cat = '', search = this.state.serach) => {
    console.log("cat", cat)
    this.setState({ load: true, cat: cat })

    let id = ''
    let url = ''

    if (Array.isArray(cat) == true && cat.length > 0) {
      cat.map((item, i) => {
        if (i == 0) {
          id = `tag[]=${item}`
        } else {
          id = id.concat(`&tag[]=${item}`)
        }
      })
    } else if (this.state.defaultCheckedKeys.length > 0) {
      this.state.defaultCheckedKeys.map((item, i) => {
        if (i == 0) {
          id = `tag[]=${item}`
        } else {
          id = id.concat(`&tag[]=${item}`)
        }
      })
    } else {
      id = ''
      // id = 'tag[0]=1'
    }
    if (id != '') { id = "&".concat(id) }
    if (this.state.data.length > 0) {
      if (search !== '' && search !== undefined && search !== null) {
        url = `${config.REST_API_URL}api/search-all?_format=json${id}&search=${search}`
      } else {
        url = `${config.REST_API_URL}api/search-all?_format=json${id}`
      }

    }
    else {
      if (search !== '' && search !== undefined && search !== null) {
        let page = this.state.data.pager != undefined ? this.state.data.pager.current_page : 0;
        url = `${config.REST_API_URL}api/search-all?_format=json${id}&search=${search}&page=${page}`
      } else {
        let page = this.state.data.pager != undefined ? this.state.data.pager.current_page : 0;
        url = `${config.REST_API_URL}api/search-all?_format=json${id}&page=${page}`
      }
    }

    axios(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    })
      .then((res) => {
        console.log('data returned:', res)
        if (res) {
          let response = res.data
          this.setState({ data: response, load: false })
        }
        this.setState({ load: false })
      })
  }

  /* Call on expand/collapse of any category from the tree */
  onExpand = (array) => {
    this.setState({
      defaultExpandedKeys: array.length > 0 ? array : [],
    });
  };

  /* Set the value of search keyword */
  change = (value) => {
    this.setState({ search: value })
  }

  /* Call on hitting eneter key from search box */
  keypress = (e) => {
    if (e.keyCode == 13) {
      e.preventDefault();
      if (this.state.title == '') {
        navigate(`/health-answers/tag?search=${this.state.search}`, { state: this.state.search });
      } else {
        let url = new URL(window.location.href)
        if (window.location.href.indexOf('&search=') != -1) {
          url.searchParams.set('search', this.state.search);
          url = url.toString();
          window.history.replaceState({ url: url }, null, url);
        }
        console.log("url", url)
      }
      this.getData(this.state.defaultExpandedKeys, this.state.search)
    }
  }
  LoadMore = () => {
    if (this.state.data.rows.length != this.state.data.pager.total_items) {
      window.scrollTo(0, 0);
    }
    var rec = document.getElementsByClassName('weeklypoll__more')[0].getBoundingClientRect();
    const top = rec.top + window.scrollY;
    this.setState({ load: true })
    let id = ''
    let url = ''
    let cat = this.state.cat;
    let search = this.state.search;

    if (Array.isArray(cat) == true && cat.length > 0) {
      cat.map((item, i) => {
        if (i == 0) {
          id = `tag[]=${item}`
        } else {
          id = id.concat(`&tag[]=${item}`)
        }
      })
    } else if (this.state.defaultCheckedKeys.length > 0) {
      this.state.defaultCheckedKeys.map((item, i) => {
        if (i == 0) {
          id = `tag[]=${item}`
        } else {
          id = id.concat(`&tag[]=${item}`)
        }
      })
    } else {
      id = ''
      // id = 'tag[0]=1'
    }
    if (id != '') { id = "&".concat(id) }

    if (search !== '' && search !== undefined && search !== null) {
      let page = this.state.data.pager != undefined ? this.state.data.pager.current_page + 1 : 0;
      url = `${config.REST_API_URL}api/search-all?_format=json${id}&search=${search}&page=${page}`
    } else {
      let page = this.state.data.pager != undefined ? this.state.data.pager.current_page + 1 : 0;
      url = `${config.REST_API_URL}api/search-all?_format=json${id}&page=${page}`
    }
    axios(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    })
      .then((res) => {
        if (res) {
          const result = res;
          let response = res.data
          let olddata = this.state.data.rows;
          if (result.data['rows'].length > 0) {
            setTimeout(() => {
              window.scrollTo(0, top);
            }, 600)
          }
          response['rows'] = olddata.concat(response['rows']);
          this.setState({ data: response, load: false })
        }
        this.setState({ load: false })
      })
  }
  /* Call on clicking on search button */
  submit = async (id) => {
    let key = [id];
    if (key[key.length - 1].parent !== null) {

      this.state.category.forEach(element => {
        if (element.key == key[key.length - 1]) {
          let parent = element.parent
          key.push("" + parent + "");
        }
      });
    }
    await this.setState({ defaultExpandedKeys: key, defaultCheckedKeys: [id], defaultSelectedKeys: [id] })
    this.getData(id)
  }

  render() {
    let categoryTree = arrayToTree(this.state.category, {
      parentProperty: "parent",
      customID: "id",
    });
    const { defaultExpandedKeys,
      defaultSelectedKeys,
      defaultCheckedKeys, category } = this.state
    return (
      <Layout
        crumbLabel="Health Answers"
        pageTitle={
          <>
            <PageTitle PageTitle={this.state.title} />
          </>
        }
        sidebar={
          <>
            <CTA />
            <SecondaryMenu />
            <SecondaryMenu title={blockTitle} list={linkList} />
          </>
        }
        content={
          <>

            <React.Fragment>
              <SearchForm handleChange={this.change} keyPress={this.keypress} show="false" search={this.state.search} />
              <TreeCollapse
                categoryData={categoryTree}
                FlatArray={category}
                active={categoryTree[0]}
                renderData={this.renderData}
                form="category-page"
                id={this.state.id}
                defaultExpandedKeys={defaultExpandedKeys}
                defaultSelectedKeys={defaultSelectedKeys}
                defaultCheckedKeys={defaultCheckedKeys}
                onExpand={this.onExpand}
                tabIndex="0"
                checkable={true}
                search={this.state.search}
              />
              {this.state.load == true ?
                <div>Please wait...</div> :
                this.state.title == '' && this.state.search == '' ?
                  <NoResult />
                  : <React.Fragment>
                    <CategoryListContent categoryList={this.state.data} handleSubmit={this.submit} />
                    {this.state.data.pager != undefined && this.state.data.rows != undefined
                      ? <React.Fragment> <div className="weeklypoll__more">
                        <div className="subscribe-form__btn">
                          {this.state.data.rows.length > 0 ?
                            <Primarybtn buttonTextValue="Load More.." value={this.LoadMore} /> :
                            ''
                          }
                        </div>
                      </div>
                        {this.state.data.rows.length == 0 ?
                          <NoResult /> :
                          ''
                        }</React.Fragment> : <NoResult />}</React.Fragment>
              }
            </React.Fragment>

          </>
        } />
    );
  }
}

