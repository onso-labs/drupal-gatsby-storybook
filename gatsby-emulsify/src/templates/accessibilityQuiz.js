import React from 'react'
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { Layout } from '../../stories/04-templates/layout.stories';
// import { Title } from './ask-your-question-page';
import { Quizzes } from '../../stories/03-organisms/quizzes/quizzes.stories';
// import { ListContentBlock } from '../../stories/03-organisms/list-content-block/list-content-block.stories';
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { WeeklyPoll } from '../../stories/03-organisms/weekly-poll/weekly-poll.stories';

class Form extends React.Component {

  constructor(props) {
    super(props);
    this.state = { show: false, title: '', question: '', categoryitem: this.props.pageContext.element[0].tid, btn: "Submit", error: false }
  }

  render() {
    // const newqaData = this.props.pageContext.newqa
    // const themeData = this.props.pageContext.theme
    // const lastupdateData = this.props.pageContext.lastupdated
    // const quizzData = this.props.pageContext.quizz
    // const testimonial = this.props.pageContext.testimonial
    const poll = this.props.pageContext.poll
    return (

      <Layout
        pageTitle={
          <PageTitle PageTitle="Accessibility Quiz" />
        }
        sidebar={
          <>
            <CTA />
          </>
        }
        content={
          <>
            <Quizzes />
            {/* <ListContentBlock /> */}

            <WeeklyPoll weeklyPolls={poll} />
            {
              // weeklyDatas.map((item, i) => {
              //   return (
              //     <div>
              //       <WeeklyPollResult resultDatas={item.resultData} />
              //       {/* <SocialShare socialMenu={item.socialShareMenu} /> */}
              //     </div>
              //   )
              // })
            }

          </>
        } >
      </Layout>
    )
  }
}

export default Form;