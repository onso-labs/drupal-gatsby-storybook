/* Poll Detail page */
import React, { Component } from "react";
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { MorePolls } from '../../stories/03-organisms/more-polls/more-polls.stories';
import { WeeklyPoll } from '../../stories/03-organisms/weekly-poll/weekly-poll.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { SocialShare } from '../../stories/02-molecules/menus/socialshare/social-share-menu.stories';
import { SecondaryMenu } from '../../stories/03-organisms/secondary-menu/secondary-menu.stories';
import { linkList, blockTitle } from '../common/data.js';

export default class Faqs extends Component {
    constructor() {
        super();
        this.state = { list: [] }
    }
    componentDidMount = () => {
        const list = this.props.pageContext.list
        const weeklyPoll = this.props.pageContext.element
        list.map((item, i) => {
            if (item.entityId == weeklyPoll.entityId) {
                list.splice(i, 1);
            }
        })
        this.setState({ list: list })
    }
    render() {
        const weeklyPoll = this.props.pageContext.element
        const { list } = this.state
        return (
            <Layout
                crumbLabel="Polls"
                pageTitle={
                    <>
                        <PageTitle PageTitle={weeklyPoll.entityLabel} />
                    </>
                }

                sidebar={
                    <>
                        <CTA />
                        <SecondaryMenu title={blockTitle} list={linkList} />
                    </>
                }
                content={
                    <>

                        <React.Fragment>
                            <WeeklyPoll weeklyPolls={weeklyPoll} radiosData={weeklyPoll.choice} detail={true} />
                            {/* <SocialShare /> */}
                            
                        </React.Fragment>

                        <MorePolls quizzes={list} />
                    </>
                } />

        );
    }
}

