/* ASK your question page */
import React from 'react'
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { Layout } from "../Components/layout"
// import { Title } from './ask-your-question-page';
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { AskYourQuestionForm } from '../../stories/03-organisms/ask-your-question-form/ask-your-question-form.stories';
import { AskYourQuestionTop } from '../../stories/03-organisms/ask-your-question-top/ask-your-question-top.stories';
import config from '../config/index';
import axios from 'axios';
import { navigate } from 'gatsby';
class Form extends React.Component {

  constructor(props) {
    super(props);
    this.state = { show: false, title: '', question: '', reCaptchaResponse: '',reCaptchaError:false, categoryitem: this.props.pageContext.element[0].tid, btn: "Submit", error: false ,reCaptchaError:false}
  }

  /* called when ask your question button clicked */
  change = (value) => {
    this.setState({ show: true })
  }
  /* Setting value of each field of form */
  onchange = (e, type) => {
    if (type == "title") {
      this.setState({ title: e })
    } else if (type == "question") {
      this.setState({ question: e })
    } else {
      this.setState({ categoryitem: e })
    }
  }
  /* Setting captcha value*/
  verifyCallback = (value) => {
    this.setState({
      reCaptchaResponse: value
    });
  }

  /* submit Button */
  submit = (e) => {
    e.preventDefault()
    if (this.state.title.trim() == '') {
      this.setState({ error: true })
    }
    else if(this.state.reCaptchaResponse.trim()== '') {
      this.setState({ reCaptchaError: true })
    } 
    else {
      this.setState({ btn: "Please Wait", error: false , reCaptchaError: false})
      let body = {

        "type": "health_answer",
        "title": [this.state.title],
        "field_question": {
          "value": this.state.question
        },
        "field_category": [this.state.categoryitem]
      }
      axios(`${config.REST_API_URL}node/?_format=json`, {
        method: 'POST',
        data: body,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      })
        .then((data) => {
          console.log('data returned:', data)
          if (data.status == 201) {
            this.setState({
              error: false,
              btn: "Submit",
              title: '', question: '', reCaptchaError: false
            })
          }
          navigate('/', {
            state: { DESIRED_TEXT:"Thank you for submitting your question." },
          });
        })
    }
  }

  render() {
    return (
      <Layout
        crumbLabel="Ask your question"
        pageTitle={
          <PageTitle PageTitle="Ask your question" />
        }
        sidebar={
          <>
            <CTA />
          </>
        }
        content={
          <>
            {this.state.show == true ?
              <AskYourQuestionForm title={this.state.title} question={this.state.question} verifyCallback={this.verifyCallback} handleChange={this.onchange} submitQuestion={this.submit} category={this.props.pageContext.element} btnName={this.state.btn} error={this.state.error} reCaptchaError={this.state.reCaptchaError} /> :
              <AskYourQuestionTop showForm={this.change} />
            }
          </>
        } >
      </Layout>
    )
  }
}

export default Form;
