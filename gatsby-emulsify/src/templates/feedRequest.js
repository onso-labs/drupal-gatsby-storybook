/* Footer get alice in your box Page */
import React, { Component } from "react";
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { linkList, blockTitle } from '../common/data';
import { SecondaryMenu } from '../../stories/03-organisms/secondary-menu/secondary-menu.stories';
import { CustomForm } from '../../stories/03-organisms/custom-form/custom-form.stories'
import config from '../config';
import axios from 'axios';
import { navigate } from 'gatsby';
export default class IndexPage extends React.Component {
  constructor() {
    super();
    this.state = {
      name: '',
      email: '',
      organization: '',
      phone: '',
      message: '',
      btn: "Submit",
      site: '',
      error: false,
      reCaptchaResponse: '',
      reCaptchaError:false
    }
  }

  /* Form Submit */
  submit = (e) => {
    e.preventDefault()
    if (this.state.name.trim() == '' || this.state.organization.trim() == '' || this.state.email.trim() == '' || this.state.phone.trim()== '' || this.state.site.trim() == '') {
      this.setState({ error: true })
    } else if(this.state.reCaptchaResponse.trim()== '') {
      this.setState({ reCaptchaError: true })
    } else {
      this.setState({ btn: "Please Wait", error: false , reCaptchaError: false})
      let body = {
        "type": "feed_request",
        "field_name": [this.state.name],
        "field_email": [this.state.email],
        "body": {
          "value": this.state.message
        },
        "field_organization": [this.state.organization],
        "field_phone": [this.state.phone],
        "field_site_url": [this.state.site]
      }
      axios(`${config.REST_API_URL}node/?_format=json`, {
        method: 'POST',
        data: body,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      })
        .then((data) => {
          console.log('data returned:', data)
          if (data.status == 201) {
            this.setState({
              error: false,
              name: '',
              email: '',
              organization: '',
              phone: '',
              message: '',
              site: '',
              btn: "Submit",
              reCaptchaResponse: '',
              reCaptchaError:false
            })
            navigate('/', {
              state: { DESIRED_TEXT:"Thank you for submitting a Get Alice! on Your Website request." },
            });
          }
        })
    }
  }
  /* Setting value of each field of form */
  handleChange = (e, type) => {
    this.setState({ [type]: e.target.value })
  }
  /* Setting captcha value*/
  verifyCallback = (value) => {
    this.setState({
      reCaptchaResponse: value
    });
  }
  render() {
    let data
    const element = this.props.pageContext.element
    element.map((item, i) => {
      if (item.fieldFormtype == "feed_request") {
        data = item
      }
    })
    return (
      <Layout
        crumbLabel="Get Alice! on Your Website"
        pageTitle={
          <PageTitle PageTitle="Get Alice! on Your Website" />
        }
        sidebar={
          <>
            <CTA />
            <SecondaryMenu />
            <SecondaryMenu title={blockTitle} list={linkList} />
          </>
        }
        content={
          <>
            <div class="page-content">
              <div dangerouslySetInnerHTML={{ __html: data !== undefined ? data.body ? data.body.value : '' : '' }} />
            </div>
            <CustomForm type="feed" name={this.state.name} phone={this.state.phone} organization={this.state.organization} site={this.state.site} email={this.state.email} message={this.state.message} verifyCallback={this.verifyCallback} handlechange={this.handleChange} handleSubmit={this.submit} btnName={this.state.btn} error={this.state.error}reCaptchaError={this.state.reCaptchaError} />
          </>
        } >
      </Layout>)
  }

}


