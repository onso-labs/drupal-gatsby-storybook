/* Contact alice / ask your question page */
import React, { Component } from "react";
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { linkList, blockTitle } from '../common/data';
import { SecondaryMenu } from '../../stories/03-organisms/secondary-menu/secondary-menu.stories';
import { AffiliationForm } from '../../stories/03-organisms/affiliation-form/affiliation-form.stories';
import config from '../config';
import axios from 'axios';
import { navigate } from 'gatsby';

export default class IndexPage extends React.Component {
    constructor() {
        super();
        this.state = {
            email: '',
            affiliation: '',
            btn: "Submit",
            error: false
        }
    }

    /* Form Submit */
    submit = (e) => {
        e.preventDefault()
        if (this.state.email.trim() == '') {
            this.setState({ error: true })
        } else {
            this.setState({ btn: "Please Wait", error: false })
            let body = {
                "email_address": this.state.email,
                "status": "subscribed",
                "merge_fields": {
                    "EMAIL": this.state.email
                }
            }
            console.log("body", body)
            axios(`https://us7.api.mailchimp.com/3.0/lists/6c50b40974/members`, {
                method: 'POST',
                data: body,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
            })
                .then((data) => {
                    console.log('data returned:', data)
                    if (data.status == 201) {
                        this.setState({
                            error: false,
                            email: '',
                            affiliation: '',
                            btn: "Submit",
                        })

                        navigate('/', {
                            state: { DESIRED_TEXT: "You have been successfully subscribed." },
                        });
                    }
                })
        }
    }
    /* Setting value of each field of form */
    handleChange = (e, type) => {
        this.setState({ [type]: e.target.value })
    }

    render() {
        let data
        const element = this.props.pageContext.element
        element.map((item, i) => {
            if (item.fieldFormtype == "get_alice_in_your_inbox") {
                data = item
            }
        })
        return (
            <Layout
                crumbLabel={data.title}
                pageTitle={
                    <PageTitle PageTitle={data.title} />
                }
                sidebar={
                    <>
                        <CTA />
                        <SecondaryMenu />
                        <SecondaryMenu title={blockTitle} list={linkList} />
                    </>
                }
                content={
                    <>
                        <div className="page-content" dangerouslySetInnerHTML={{ __html: data !== undefined ? data.body ? data.body.value : '' : '' }} />
                        <AffiliationForm email={this.state.email} handleSubmit={this.submit} btnName={this.state.btn} error={this.state.error} radio={this.state.affiliation} change={this.handleChange} />
                    </>
                } >
            </Layout>
        )
    }

}


