/* Fact sheet detail page */
import React from 'react';
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { SecondaryMenu } from '../../stories/03-organisms/secondary-menu/secondary-menu.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { Card } from '../../stories/02-molecules/card/card.stories';
import { SocialShare } from '../../stories/02-molecules/menus/socialshare/social-share-menu.stories';
import { FactSheetResources } from '../../stories/03-organisms/fact-sheet-resources/fact-sheet-resources.stories';
import config from '../config';
import axios from 'axios';
import { SuccessMessage } from '../../stories/02-molecules/success-message/success-message.stories';
export default class Factsheets extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      resources: [],
      DESIRED_TEXT: props.location.state != null ? props.location.state.DESIRED_TEXT : ''

    }
  }
  componentDidMount = async () => {
    if (this.props.location.state != null) {
      window.history.replaceState(null, '')
    } await this.getData();
  }
  componentWillReceiveProps = () => {
    this.getData();
  }
  componentWillUnmount = () => {
    if (this.props.location.state != null) {
      window.history.replaceState(null, '')
    }
  }
  /* API call for resource listing */
  getData = () => {
    const data = this.props.pageContext.element
    let id = data.nid
    let url = ''
    url = `${config.REST_API_URL}api/fact-sheet-columbia-resource?_format=json&nid=${id}`
    axios(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    })
      .then((res) => {
        let response = res.data
        this.setState({ resources: response })
      })
  }
  render() {

    const data = this.props.pageContext.element
    /* Redirect Urls for social share */
    let mailurl = "/printmail/" + data.nid + ""
    let pdfurl = `${config.REST_API_URL}node/${data.nid}/printable/pdf`
    return (
      <Layout
        crumbLabel="Fact Sheets"
        link="/fact-sheets"
        title={data.title}
        pageTitle={
          <PageTitle PageTitle={data.title} />
        }
         newQA={
          <>
              <div className="success-wrap-nobackground">
                  {this.state.DESIRED_TEXT != "" && this.state.DESIRED_TEXT != null && (
                      <SuccessMessage message={this.state.DESIRED_TEXT} />
                  )}
              </div>
          </>
      }
        sidebar={
          <>
            <CTA />
            <SecondaryMenu />
          </>
        }
        content={
          <>
            <div class="page-content">
              <div dangerouslySetInnerHTML={{ __html: data.body.value }} />
            </div>
            {data.fieldResources.map((item, i) => (
              <Card card={item.entity} cardDetail={item.entity.body} key={i} />
            ))
            }
            {this.state.resources.map((res, i) => (
              <FactSheetResources data={res} />
            ))
            }
            <SocialShare url={`${config.REST_API_URL}node/${data.nid}/printable/print` + ""} mailUrl={mailurl} pdfUrl={pdfurl} />
          </>
        } >
      </Layout>
    );
  }
}
