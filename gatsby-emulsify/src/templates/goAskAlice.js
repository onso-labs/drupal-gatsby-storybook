/* Footer Link to go ask alice page*/
import React from "react";
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';

export default class IndexPage extends React.Component {

  render() {
    let data
    const element = this.props.pageContext.element
    element.map((item) => {
      if (item.fieldFormtype == "ask_alice") {
        data = item
      }
    })
    return (
      <Layout
        crumbLabel="About Alice!"
        pageTitle={
          <PageTitle PageTitle="Link to Go Ask Alice!" />
        }
        sidebar={
          <>
            <CTA />
          </>
        }
        content={
          <>
            <div class="page-content">
              <div dangerouslySetInnerHTML={{ __html: data !== undefined ? data.body ? data.body.value : '' : '' }} />
            </div>
          </>
        } >
      </Layout>)
  }

}


