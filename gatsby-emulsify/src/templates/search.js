/* Search page */
import React from 'react';
import config from '../config';
import axios from 'axios';
import { navigate } from 'gatsby';
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { SecondaryMenu } from '../../stories/03-organisms/secondary-menu/secondary-menu.stories';
import { ListContentBlock } from '../../stories/03-organisms/list-content-block/list-content-block.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { linkList, blockTitle } from '../common/data.js';
import TreeCollapse from '../Components/categoryTree';
import { SearchForm } from '../../stories/03-organisms/search-form/search-form.stories';
var arrayToTree = require("array-to-tree");

export default class Factsheets extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            search: '',
            load: true,
            category: this.props.pageContext.element,
            defaultExpandedKeys: [],
        }
    }

    componentWillReceiveProps = (next) => {
        const params = new URLSearchParams(window.location.search);
        let search = params.get('search')
        this.setState({ search })
        this.getData(search);
    }

    componentDidMount = async () => {
        const params = new URLSearchParams(window.location.search);
        let search = params.get('search')
        this.setState({ search })
        this.getData(search);
        let categoryList = this.state.category
        await categoryList.map((item, i) => {
            item.id = item.tid
            item.key = item.tid
            item.title = item.name
            if (item.parent !== null && item.parent !== undefined && Array.isArray(item.parent) == true) {
                if (item.parent[0].entity !== null) {
                    let parent = item.parent
                    item.parent = parent[0].entity.tid
                    item.parent_name = parent[0].entity.entityLabel
                } else {
                    item.parent = null
                    item.parent_name = null
                }
            } else if (item.parent == null || item.parent == undefined || Array.isArray(item.parent) == true) {
                item.parent = null
                item.parent_name = null
            }
        })


        await this.setState({ category: categoryList, })
    }

    /* API call for searched keyword */
    getData = (search) => {
        this.setState({ load: true })
        axios(`${config.REST_API_URL}api/search-all?_format=json&search=${search}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
        })
            .then((res) => {
                console.log('data returned:', res)
                this.setState({ data: res.data, load: false })
            })
    }

    /* setting search value */
    change = (value) => {
        this.setState({ search: value })
    }

    /* triggered when search button is clicked */
    handleSubmit = (e) => {
        e.preventDefault();
        console.log("here", this.state.search)
        navigate(`/search/node?search=${this.state.search}`, { state: this.state.search });
    }

    /* Triggered on expand/collapse any category from tree */
    onExpand = (array) => {
        console.log("array", array)
        this.setState({
            defaultExpandedKeys: array.length > 0 ? array : []
        });
    };

    /* navigate to the health answer page */
    renderData = () => {
        navigate('/health-answers/tag');
    }
    // keypress = () => {
    //     navigate(`/search/node?search=${this.state.search}`, { state: { search } });
    // }

    render() {
        const { defaultExpandedKeys,
            category } = this.state

        let categoryTree = arrayToTree(this.state.category, {
            parentProperty: "parent",
            customID: "id",
        });

        return (
            <div className="search-page">
                <Layout
                    crumbLabel="search"
                    link="/content"
                    pageTitle={
                        <PageTitle PageTitle="Search Result" />
                    }
                    sidebar={
                        <>
                            <CTA />
                            <SecondaryMenu />
                            <SecondaryMenu title={blockTitle} list={linkList} />
                        </>
                    }
                    content={
                        <>
                            <SearchForm btnName="Search" handleChange={this.change} keyPress={this.keypress} submit={this.handleSubmit} search={this.state.search} />
                            <TreeCollapse categoryData={categoryTree} onExpand={this.onExpand} defaultExpandedKeys={this.state.defaultExpandedKeys} renderData={this.renderData} form="details-page" selectable={true} checkable={false} />
                            {this.state.load == true ?
                                <div>Please wait...</div>
                                :
                                this.state.data.length == 0 ?
                                    <React.Fragment>
                                        <h2>Your search yielded no results</h2>
                                        <ul>
                                            <li>Check if your spelling is correct.</li>
                                            <li>Remove quotes around phrases to search for each word individually. <em>bike shed</em> will often show more results than <em>"bike shed".</em></li>
                                            <li>Consider loosening your query with <em>OR. bike OR shed</em> will often show more results than <em>bike shed.</em></li>
                                        </ul>
                                    </React.Fragment>
                                    :

                                    <React.Fragment>
                                        <h2>Search Results</h2>
                                        <div className="search-result-list">
                                            <ListContentBlock searchlist={this.state.data} />
                                        </div>
                                    </React.Fragment>
                            }

                        </>
                    } >
                </Layout >
            </div>
        );
    }
}
