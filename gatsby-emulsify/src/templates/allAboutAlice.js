/* All about Alice Page */
import React, { Component } from "react";
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { AboutAliceStats } from '../../stories/03-organisms/about-alice-stats/about-alice-stats.stories';
import { AllAboutAlice } from '../../stories/02-molecules/all-about-alice/all-about-alice.stories';
import { Testimonial } from '../../stories/03-organisms/testimonial/testimonial.stories';
import { SocialShare } from '../../stories/02-molecules/menus/socialshare/social-share-menu.stories';
import { AwardBlock } from '../../stories/03-organisms/award-block/award-block.stories';
import config from '../config';
import axios from 'axios';
import { SuccessMessage } from '../../stories/02-molecules/success-message/success-message.stories';
export default class IndexPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            field_testimonial: '',
            DESIRED_TEXT: props.location.state != null ? props.location.state.DESIRED_TEXT : ''
        }
        /* Testimonial data API call*/
        axios(`${config.REST_API_URL}testimonial_rotator?_format=json`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
        })
            .then((data) => {
                this.setState({ field_testimonial: data.data[0].field_testimonial })
            });
    }
    componentWillUnmount = () => {
        if (this.props.location.state != null) {
            window.history.replaceState(null, '')
        }
    }
    componentDidMount = async () => {
        if (this.props.location.state != null) {
            window.history.replaceState(null, '')
        }
    }
    render() {
        const element = this.props.pageContext.element
        const testimonial = this.props.pageContext.testimonial
        let data
        element.map((item, i) => {
            if (item.fieldFormtype == "all_about") {
                data = item
            }
        })
        /* Redirect Urls for social share */
        let url = data !== undefined && data.nid ? `${config.REST_API_URL}node/${data.nid}/printable/print` + "" : ''
        let mailurl = data !== undefined && data.nid ? "/printmail/" + data.nid + "" : ''
        let pdfurl = data !== undefined && data.nid ? `${config.REST_API_URL}node/${data.nid}/printable/pdf` : ''
        return (

            <div className="all-about-alice-page">
                <Layout
                    crumbLabel="About Alice!"
                    pageTitle={
                        <PageTitle PageTitle="All About Alice!" />
                    }
                    newQA={
                        <>
                            <div className="success-wrap-nobackground">
                                {this.state.DESIRED_TEXT != "" && this.state.DESIRED_TEXT != null && (
                                    <SuccessMessage message={this.state.DESIRED_TEXT} />
                                )}
                            </div>
                        </>
                    }
                    sidebar={
                        <>
                            <CTA />
                        </>
                    }
                    content={
                        <>
                            <div className="row">
                                <div className="col-lg-5 display-order">
                                    <AboutAliceStats />
                                </div>
                                <div className="col-lg-7">
                                    <AllAboutAlice />
                                </div>
                            </div>
                            <Testimonial userData={this.state.field_testimonial != null ? this.state.field_testimonial : ''} />
                            <div className="allabout-description">

                                <div className="award-block" dangerouslySetInnerHTML={{ __html: data !== undefined ? data.body ? data.body.value : '' : '' }} />
                            </div>
                            {/* <AwardBlock /> */}
                            <SocialShare url={url} mailUrl={mailurl} pdfUrl={pdfurl} />
                        </>
                    } >
                </Layout>
            </div>)
    }

}


