import React from 'react'
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { Layout } from '../../stories/04-templates/layout.stories';
// import { Title } from './ask-your-question-page';
import { SocialShare } from '../../stories/02-molecules/menus/socialshare/social-share-menu.stories';
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { Accordion } from '../../stories/02-molecules/accordion/accordion.stories';
import { AboutAliceStats } from '../../stories/03-organisms/about-alice-stats/about-alice-stats.stories';
import { AwardBlock } from '../../stories/03-organisms/award-block/award-block.stories';
import { NewHealthTopics } from '../../stories/03-organisms/new-health-topics/new-health-topics.stories';

class Form extends React.Component {

  constructor(props) {
    super(props);
    this.state = { show: false, title: '', question: '', categoryitem: this.props.pageContext.element[0].tid, btn: "Submit", error: false }
  }

  render() {
    return (
      <Layout
        pageTitle={
          <PageTitle PageTitle="Accessibility FAQ" />
        }
        sidebar={
          <>
            <CTA />
          </>
        }
        content={
          <>
            <Accordion />
            <SocialShare />
            <AboutAliceStats />
            <AwardBlock />
            <NewHealthTopics />
          </>
        } >
      </Layout>
    )
  }
}

export default Form;
