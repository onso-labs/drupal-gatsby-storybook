/* Factsheet page */
import React from 'react';
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { SecondaryMenu } from '../../stories/03-organisms/secondary-menu/secondary-menu.stories';
import { ListContentBlock } from '../../stories/03-organisms/list-content-block/list-content-block.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { linkList, blockTitle } from '../common/data.js'

export default class Factsheets extends React.Component {
  render() {

    const data = this.props.pageContext.element
    return (
      <div className="fact-sheet-page">
        <Layout
          crumbLabel="Fact Sheets"
          pageTitle={
            <>
              <PageTitle PageTitle="Fact sheets" />
            </>}

          sidebar={
            <>
              <CTA />
              <SecondaryMenu />
              <SecondaryMenu title={blockTitle} list={linkList} />
            </>
          }
          content={
            <>
              <ListContentBlock searchlist={data} />
            </>
          } />
      </div>
    );
  }
}
