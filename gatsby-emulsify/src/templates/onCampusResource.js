/* On campus page */
import React, { Component } from "react";
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { HealthResources } from '../../stories/03-organisms/health-resources/health-resources.stories';
import { ResourceTitle } from '../../stories/03-organisms/resource-title/resource-title.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { SocialShare } from '../../stories/02-molecules/menus/socialshare/social-share-menu.stories';
import config from '../config';
import { SuccessMessage } from '../../stories/02-molecules/success-message/success-message.stories';
export default class Faqs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      DESIRED_TEXT: props.location.state != null ? props.location.state.DESIRED_TEXT : ''
    }
  }
  componentDidUpdate() {
    if (this.props.location.state != null) {
      window.history.replaceState(null, '')
    }
    console.log("in did update")
    this.scroll();
  }
  componentDidMount = () => {
    if (this.props.location.state != null) {
      window.history.replaceState(null, '')
    }
    setTimeout(() => {
      this.scroll();
    }, 100)

  }

  componentWillMount = () => {
    if (this.props.location.state != null) {
      window.history.replaceState(null, '')
    }
    this.scroll();
  }
  /* Scrolling function when jump to any resource from health answer detail page */
  scroll = () => {
    if (typeof window !== `undefined`) {
      const queryString = window.location.search;
      if (queryString != "") {
        const urlParams = new URLSearchParams(queryString);
        let product = urlParams.get('name');
        product = product.trim();
        console.log(product);
        if (document.getElementById(product) != null) {
          window.scrollTo(0, 0);
          setTimeout(
            () => {
              var top = document.getElementById(product).getBoundingClientRect().top;
              console.log(top);
              window.scrollTo(0, top);
            },
            500
          );
        }
      }
    }
  }
  render() {
    const campusData = this.props.pageContext.element
    let url = campusData.length > 0 ? `${config.REST_API_URL}node/${campusData[0].nid}/printable/print` + "" : ''
    let mailurl = campusData.length > 0 ? "/printmail/" + campusData[0].nid + "" : ''
    let pdfurl = campusData.length > 0 ? `${config.REST_API_URL}node/${campusData[0].nid}/printable/pdf` : ''

    return (
      <Layout
        crumbLabel="On-campus resources"
        pageTitle={
          <>
            <PageTitle PageTitle="On-campus resources" />
          </>
        }

        newQA={
          <>
              <div className="success-wrap-nobackground">
                  {this.state.DESIRED_TEXT != "" && this.state.DESIRED_TEXT != null && (
                      <SuccessMessage message={this.state.DESIRED_TEXT} />
                  )}
              </div>
          </>
      }
        sidebar={
          <>
            <CTA />
          </>
        }
        content={
          <>
            {campusData.length > 0 && campusData.map((item, i) => (
              <React.Fragment>
                <ResourceTitle CampusTitle={item} />
                <HealthResources cardCampusResources={item.queryFieldResource.entities} />
              </React.Fragment>

            ))}

            <div class="social-menus">
              {/* <SocialShare url={url} mailUrl={mailurl} pdfUrl={pdfurl} /> */}
              <SocialShare url={null} mailUrl={null} pdfUrl={null} />
            </div>
          </>
        } />

    );
  }
}

