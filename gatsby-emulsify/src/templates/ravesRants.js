/* Raves and rents page */
import React, { Component } from "react";
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { Paragraph } from '../../stories/01-atoms/text/paragraph/paragraph.stories';
import { GAARaveRants } from '../../stories/03-organisms/gaa-raves-rants/gaa-raves-rants.stories';
import { ravesDetail } from '../common/data.js';
import { SocialShare } from '../../stories/02-molecules/menus/socialshare/social-share-menu.stories';
import config from '../config';
import axios from 'axios';
export default class IndexPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            response: []
        }
    }

    componentDidMount = async () => {
        await this.getResponse()
    }
    getResponse = async () => {
        await axios(`${config.REST_API_URL}raves_rants?_format=json`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
        }).then((res) => {
            console.log("res", res.data)
            this.setState({ response: res.data })
        })
    }

    render() {

        const ravesData = this.props.pageContext.element
        return (
            <Layout
                crumbLabel="About Alice!"
                link="/about-alice/all-about"
                title="Raves & Rants"
                pageTitle={
                    <PageTitle PageTitle="Raves & Rants" />
                }
                sidebar={
                    <>
                        <CTA />

                    </>
                }

                content={
                    <>
                        <Paragraph paragraphContent={ravesDetail.paragraph} />

                        <GAARaveRants gaaRavesRantsData={this.state.response} />
                        <div className="social-menus">
                            {/* <SocialShare pdfUrl={`${config.REST_API_URL}print/view/pdf/gq_raves_and_rants/page_1`} /> */}
                            <SocialShare url={null} mailUrl={null} pdfUrl={null} />
                        </div>
                    </>
                }

            >

            </Layout >
        )
    }

}


