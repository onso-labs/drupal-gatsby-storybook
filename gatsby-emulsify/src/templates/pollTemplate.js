/* Poll Page */
import React, { Component } from "react";
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { MorePolls } from '../../stories/03-organisms/more-polls/more-polls.stories';
import { WeeklyPoll } from '../../stories/03-organisms/weekly-poll/weekly-poll.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { SocialShare } from '../../stories/02-molecules/menus/socialshare/social-share-menu.stories';
import { SecondaryMenu } from '../../stories/03-organisms/secondary-menu/secondary-menu.stories';
import { linkList, blockTitle } from '../common/data.js';
import config from '../config';
import axios from 'axios';
import { WeeklyPollResult } from '../../stories/03-organisms/weekly-poll-result/weekly-poll-result.stories';
export default class Faqs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            weeklyPolls: [],
            cidData: {}
        }
        if (this.props.pageContext.element != undefined) {
            this.state.weeklyPolls = this.props.pageContext.element

        }
    }

    /* called when any option selected/deselected */
    onRadioClick = (pid, cid, e) => {
        // if (e != undefined) {
        //     e.preventDefault();
        // }
        var cidData = this.state.cidData;
        cidData[pid] = cid;
        var weeklyPolls = this.state.weeklyPolls;
        let index = weeklyPolls.findIndex(o => o.entityId == pid);
        if (index != -1) {
            weeklyPolls[index].cid = cid
        }
        this.setState({ weeklyPolls: weeklyPolls, cidData: cidData })
    }

    /* called when vote button clicked */
    onvoteclick = async (pid, e) => {
        if (e != undefined) {
            e.preventDefault();
        }
        var cidData = await this.state.cidData;
        var variables = {
            "chid": cidData[pid] != undefined ? cidData[pid] : 0,
            "pid": pid,
            "uid": "1"
        }
        await axios(`${config.REST_API_URL}rest-api/gaa_poll_vote_mutation?_format=json`, {
            method: 'POST',
            data: variables,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
        })
            // .then(r => r.json())
            .then((data) => {
                console.log('data returned:', data);
                let body = {
                    "id": data.data
                }
                axios(`${config.REST_API_URL}rest-api/gaa_poll_votes?_format=json`, {
                    method: 'POST',
                    data: body,
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                    },

                })
                    .then((data) => {
                        var weeklyPolls = this.state.weeklyPolls;
                        let index = weeklyPolls.findIndex(o => o.entityId == data.data.pid);
                        if (index != -1) {
                            let resultMessage = [];
                            if (weeklyPolls[index].weeklyData != undefined) {
                                resultMessage = weeklyPolls[index].weeklyData['resultMessage']
                            }
                            weeklyPolls[index].weeklyData = data.data;
                            if (resultMessage == undefined) {
                                resultMessage = [];
                            }
                            resultMessage.push("Your vote is being tallied. Meanwhile, here's how other readers voted.");
                            weeklyPolls[index].weeklyData.resultMessage = resultMessage.slice(0, 1);
                        }
                        this.setState({ weeklyPolls: weeklyPolls })
                    }).catch(err => {
                        console.log("err", err);
                    })

            }).catch(err => {
                console.log("err", err);
            })

    }

    render() {
        const weeklyPoll = this.props.pageContext.element
        const list = this.props.pageContext.list

        return (
            <Layout
                crumbLabel="Polls"
                pageTitle={
                    <>
                        <PageTitle PageTitle="Polls" />
                    </>
                }

                sidebar={
                    <>
                        <CTA />
                        <SecondaryMenu title={blockTitle} list={linkList} />
                    </>
                }
                content={
                    <>
                        {this.state.weeklyPolls.map((item) => (
                            <React.Fragment>
                                <WeeklyPoll weeklyPolls={item} checked={true} pid={item.entityId} radiosData={item.choice} show={false} onchange={this.onRadioClick} onVoteClick={this.onvoteclick} />
                                {item.weeklyData != null ?
                                    (Object.keys(item.weeklyData).length !== 0 ? <div>
                                        <WeeklyPollResult pollResult={item.weeklyData} resultDatas={item.weeklyData.field_poll_options} />
                                    </div> : '') : ''}
                                {/* <SocialShare url={`${config.REST_API_URL}poll/${item.entityId}/printable/print`} pdfUrl={`
              ${config.REST_API_URL}poll/${item.entityId}/printable/pdf`} /> */}
                                <SocialShare url={null} mailUrl={null} pdfUrl={null} />
                            </React.Fragment>
                        ))}
                        { list.length > 0 ? <MorePolls quizzes={list} /> : ''}
                    </>
                } />

        );
    }
}

