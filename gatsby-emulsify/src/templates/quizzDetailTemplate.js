/* QUizz detail page */
import React, { Component } from "react";
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { QuizzesBlock } from '../../stories/03-organisms/quizzes-block/quizzes-block.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { SocialShare } from '../../stories/02-molecules/menus/socialshare/social-share-menu.stories';
import { ListContentBlock } from '../../stories/03-organisms/list-content-block/list-content-block.stories';
import config from '../config';
import { SuccessMessage } from '../../stories/02-molecules/success-message/success-message.stories';
export default class IndexPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      questions: [],
      count: 1,
      list: [],
      DESIRED_TEXT: props.location.state != null ? props.location.state.DESIRED_TEXT : ''
    }
    this.createData();
    this.createList();
  }
  componentWillUnmount = () => {
    if (this.props.location.state != null) {
      window.history.replaceState(null, '')
    }
  }
  componentDidMount = async () => {
    if (this.props.location.state != null) {
      window.history.replaceState(null, '')
    }
  }
  /* called when one quizz answer submitted */
  loadMore = () => {
    this.setState({ count: this.state.count + 1 })
    let length = this.state.questions.length
    let count = this.state.count;
    if (count < this.state.data.length) {
      this.state.questions[length - 1].show = true
      this.state.questions.push(this.state.data[count])
    } else {
      this.state.questions[length - 1].show = true
    }
  }

  /* create list of options and answers */
  createList = () => {
    const quizzData = this.props.pageContext.element
    let listData = this.props.pageContext.quizz
    listData.map((item, i) => {
      if (item.nid == quizzData.nid) {
        listData.splice(i, 1)
      }
    })
    this.state.list = listData
  }

  /* set data in proper format */
  createData = () => {
    const quizzData = this.props.pageContext.element
    let list = []
    let temp = quizzData != null ? quizzData.queryFieldQuizDetails != null ? quizzData.queryFieldQuizDetails.entities : [] : []
    if (temp != null && temp != undefined) {
      temp.map((element) => {
        list.push({
          value: element.fieldQuizQuestion,
          question: element.fieldQuizCallToActionText,
          type: element.fieldQuizType,
          message: element.fieldQuizMessage,
          options: element.fieldAnswers
        })
      });
    }
    this.state.data = list
    if (list.length > 0) {
      this.state.questions.push(list[0])
    }
  }

  render() {

    const quizzData = this.props.pageContext.element
    let data = { body: quizzData.body }
    let pdfurl = `${config.REST_API_URL}node/${quizzData.nid}/printable/pdf`
    console.log(">>>", this.state)
    return (
      <div className="quizzes-detail-page">
        <Layout
          crumbLabel="Quizzes"
          link="/quizzes"
          title={quizzData.title}
          pageTitle={
            <PageTitle PageTitle={quizzData.title} />
          }
          newQA={
           <>
               <div className="success-wrap-nobackground">
                   {this.state.DESIRED_TEXT != "" && this.state.DESIRED_TEXT != null && (
                       <SuccessMessage message={this.state.DESIRED_TEXT} />
                   )}
               </div>
           </>
       }
          sidebar={
            <>
              <CTA />
            </>
          }
          content={
            <>
              <QuizzesBlock quizzesDatas={data} listData={this.state.questions} more={this.loadMore} />
              <SocialShare url={`${config.REST_API_URL}node/${quizzData.nid}/printable/print` + ""} mailUrl={"/printmail/" + quizzData.nid + ""} pdfUrl={pdfurl} />
              <h2>More Quizzes</h2>
              <ListContentBlock searchlist={this.state.list} />
            </>
          } >
        </Layout>
      </div>
    )
  }

}

