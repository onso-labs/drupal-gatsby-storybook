/* Emergency page */
import React from 'react';
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { HealthResources } from '../../stories/03-organisms/health-resources/health-resources.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { SocialShare } from '../../stories/02-molecules/menus/socialshare/social-share-menu.stories';
import config from '../config';
import { SuccessMessage } from '../../stories/02-molecules/success-message/success-message.stories';
export default class Emergency extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            DESIRED_TEXT: props.location.state != null ? props.location.state.DESIRED_TEXT : ''
        }
    } componentWillUnmount = () => {
        if (this.props.location.state != null) {
            window.history.replaceState(null, '')
        }
    }
    componentDidMount = async () => {
        if (this.props.location.state != null) {
            window.history.replaceState(null, '')
        }
    }
    render() {


        const element = this.props.pageContext.element
        /* Redirect Urls for social share */
        let url = element.length > 0 ? `${config.REST_API_URL}node/${element[0].nid}/printable/print` + "" : ''
        let mailurl = element.length > 0 ? "/printmail/" + element[0].nid + "" : ''
        let pdfurl = element.length > 0 ? `${config.REST_API_URL}node/${element[0].nid}/printable/pdf` : ''
        return (
            <Layout
                crumbLabel="In an Emergency"
                pageTitle={
                    <PageTitle PageTitle="In an Emergency" />
                }
                newQA={
                    <>
                        <div className="success-wrap-nobackground">
                            {this.state.DESIRED_TEXT != "" && this.state.DESIRED_TEXT != null && (
                                <SuccessMessage message={this.state.DESIRED_TEXT} />
                            )}
                        </div>
                    </>
                }
                sidebar={
                    <>
                        <CTA />
                    </>
                }
                content={
                    <>

                        <HealthResources cardCampusResources={element} />
                        <div className="social-menus">
                            {/* <SocialShare url={url} mailUrl={mailurl} pdfUrl={pdfurl} /> */}
                            <SocialShare url={null} mailUrl={null} pdfUrl={null} />
                        </div>
                    </>
                } >
            </Layout>
        );
    }
}
