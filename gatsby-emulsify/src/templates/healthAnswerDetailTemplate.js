/* Health answer detail page */
import React, { Component } from "react";
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { SecondaryMenu } from '../../stories/03-organisms/secondary-menu/secondary-menu.stories';
import { QuestionBlock } from '../../stories/02-molecules/question-block/question-block.stories';
import { AnswerBlock } from '../../stories/02-molecules/answer-block/answer-block.stories';
import { SocialShare } from '../../stories/02-molecules/menus/socialshare/social-share-menu.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { linkList, blockTitle } from '../common/data.js'
import { navigate } from "gatsby"
import TreeCollapse from '../Components/categoryTree';
import config from '../config';
import axios from 'axios';
import { CommentForm } from '../../stories/03-organisms/comment-form/comment-form.stories';
import { ResponseBlock } from "../../stories/02-molecules/response-block/response-block.stories";
import jQuery from "jquery";
import moment from 'moment';
import { SuccessMessage } from '../../stories/02-molecules/success-message/success-message.stories';
var arrayToTree = require("array-to-tree");

export default class HealthAnswersTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category: this.props.pageContext.category,
      defaultExpandedKeys: [],
      categoryTree: [], healthData: [],
      relatedquestionsData: [],
      tagsData: [],
      resource1: [],
      load: false,
      error: false,
      message: '',
      btn: "Submit",
      reCaptchaResponse: '',
      reCaptchaerror: false,
      response: [],
      lastUpdatedDate: '',
      originallyPublishedDate: '',
      healthcategory: [],
      DESIRED_TEXT: props.location.state != null ? props.location.state.DESIRED_TEXT : ''
    }
  }

  to_ul = (child, type) => {
    var ul = document.createElement(type);
    for (var i = 0, n = child.length; i < n; i++) {
      var branch = child[i];
      ul.setAttribute("id", branch.tid);
      var li = document.createElement("ul");
      li.setAttribute("id", branch.tid);
      var ext = branch.child ? document.createTextNode(">") : i != child?.length - 1 ? '' : '';

      console.log("i", i, "child", child?.length - 1, "ext", ext, "term_name", branch)
      // Create anchor element. 
      var a = document.createElement('a');
      // Create the text node for anchor element. 
      var link = document.createTextNode(branch.term_name);
      // Append the text node to anchor element. 
      a.appendChild(link);
      // Set the title. 
      a.title = branch.term_name
      // Set the href property. 
      a.href = "/health-answers/tag?cat=" + branch.tid + "&title=" + branch.term_name;
      a.setAttribute("class", "link");
      // Append the anchor element to the body. 
      li.appendChild(a);
      if (branch.child) {
        li.appendChild(this.to_ul(branch.child, "li"));
      }
      ul.appendChild(li);
    }
    return ul;
  }
  componentDidMount = async () => {
    if (this.props.location.state != null) {
      window.history.replaceState(null, '')
    }
    /*API call for Sidebar tags data */
    await this.setState({ healthData: this.props.pageContext })
    jQuery('.question__wrapper a').on('click', function (e) {
      //   return ( alert('you are being redirected to ' + this.href) );
      if (window !== undefined) {
        e.preventDefault();
        checkExternallinks(this.href);
      }
    });
    jQuery('.answer__block a').on('click', function (e) {
      if (window !== undefined) {
        e.preventDefault();
        checkExternallinks(this.href);

      }
    });
    await axios(`${config.REST_API_URL}rest-api/gaa_health_category?_format=json&nid=${this.props.pageContext.element.nid}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    }).then((data) => {
      var cat = [];
      cat = data.data;
      this.setState({ healthcategory: cat })
      var treeEl = document.getElementById("tree");
      if (treeEl != null) {
        treeEl.appendChild(this.to_ul(cat, "div"));
      }
    })
    await axios(`${config.REST_API_URL}api/healthtopiccategory?_format=json&nid=${this.props.pageContext.element.nid}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    }).then((data) => {
      let array = [];
      //  array.push(data.data[0])
      // console.log("healthtopiccategory",data.data)
      this.setState({ tagsData: data.data })
    })
    /* API call for side bar related question data */
    await axios(`${config.REST_API_URL}api/relatedquestions?_format=json&nid=${this.props.pageContext.element.nid}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    }).then((data) => {
      this.setState({ relatedquestionsData: data.data })
    })
    await axios(`${config.REST_API_URL}api/orignally-posted-last-updated?_format=json&nid=${this.props.pageContext.element.nid}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    }).then((data) => {
      this.setState({ lastUpdatedDate: data.data[0].field_published_at, originallyPublishedDate: data.data[0].field_last_reviewed_on })
    })
   
    await this.getResource();
    await this.getResponse();
    /* Setting parent of each category */
    let categoryList = this.state.category
    await categoryList.map((item) => {
      item.id = item.tid
      item.key = item.tid
      item.title = item.name
      if (item.parent !== null && item.parent !== undefined && Array.isArray(item.parent) == true) {
        if (item.parent[0].entity !== null) {
          let parent = item.parent
          item.parent = parent[0].entity.tid
          item.parent_name = parent[0].entity.entityLabel
        } else {
          item.parent = null
          item.parent_name = null
        }
      } else if (item.parent == null || item.parent == undefined || Array.isArray(item.parent) == true) {
        item.parent = null
        item.parent_name = null
      }
    })
    await this.setState({ category: categoryList, load: true })
  }

  /* API call for sidebar resource data */
  getResource = async () => {
    const healthData = this.props.pageContext;
    await axios(`${config.REST_API_URL}api/columbiaresourceslist?_format=json&nid=${healthData.element.nid}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    }).then(async (data) => {
      console.log("data", data)
      await this.setState({ resource1: data.data })
      let resources = await this.state.resource1
      await resources.map((item, i) => {
        console.log(">>>>>item", item)
        let obj = {
          'targetId': '',
          'entity': {
            'entityLabel': item.title,
            'entityUrl': {
              'path': ''
            }
          }
        }
        healthData.element.fieldResources.push(obj);
      })
      console.log(healthData.element.fieldResources)
      healthData.element.fieldResources = healthData.element.fieldResources.filter((v, i, a) => a.findIndex(t => (t.entity.entityLabel === v.entity.entityLabel)) === i)
      await this.setState({ healthData: healthData })
    })

  }
  componentWillUnmount = () => {
    if (this.props.location.state != null) {
      window.history.replaceState(null, '')
    }
  }

  /* Form Submit using mutation*/
  submit = (e) => {
    e.preventDefault()
    const healthData = this.props.pageContext;
    if (this.state.message == '') {
      this.setState({ error: true })
    }
    else if (this.state.reCaptchaResponse == '') {
      this.setState({ reCaptchaerror: true })
    }
    else {
      this.setState({ btn: "Please Wait", error: false, reCaptchaerror: false })
      //       var variables = {
      //         "input": {
      //           "title": "TEST",
      //           "body": { "value": this.state.message, "format": "basic_html", "summary": this.state.message },
      //           "fieldResponseDecision": "new_submission",
      //           "fieldHealthAnswer": { "targetId": healthData.element.nid }
      //         }
      //       }

      //       var query = `mutation ($input: NodeResponseCreateInput!) {
      //   createNodeResponse(input: $input) {
      //     entity {
      //       entityId
      //       }
      //     errors
      //     violations{
      //       message
      //     }
      //     }
      // }`;

      //       axios(config.REST_API_URL + 'graphql', {
      //         method: 'POST',
      //         data: JSON.stringify({
      //           query,
      //           variables
      //         }),
      //         headers: {
      //           'Content-Type': 'application/json',
      //           'Accept': 'application/json',
      //         },
      //       })
      //         // .then(r => r.json())
      //         .then(data => {
      //           console.log('data returned:', data)
      //           if (data) {
      //             this.setState({
      //               error: false,
      //               btn: "Submit",
      //               message: '',
      //               required: false,
      //               reCaptchaResponse: '',
      //               reCaptchaerror: false
      //             })
      //           }
      //         }
      //         );
      let body = {
        "type": "response",
        "field_health_answer": [healthData.element.nid],
        "body": {
          "value": this.state.message
        },
        "field_response_decision": ["new_submission"]
      }
      axios(`${config.REST_API_URL}node/?_format=json`, {
        method: 'POST',
        data: body,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      })
        .then((data) => {
          console.log('data returned:', data)
          if (data.status == 201) {
            this.setState({
              error: false,
              btn: "Submit",
              message: '',
              required: false,
              reCaptchaResponse: '',
              reCaptchaerror: false
            })
            this.getResponse();
          }
        })
    }
  }
  getResponse = async () => {
    const healthData = this.props.pageContext;
    await axios(`${config.REST_API_URL}api/response?_format=json&nid=${healthData.element.nid}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    }).then((res) => {
      console.log("res", res)
      this.setState({ response: res.data })
    })
  }
  /* Setting value of each field of form */
  handleChange = (e, type) => {
    this.setState({ [type]: e.target.value })
  }

  /* Setting captcha value*/
  verifyCallback = (value) => {
    this.setState({
      reCaptchaResponse: value
    });
  }

  /* Navigating to path on the select/deselect any category */
  renderData = () => {
    navigate('/health-answers/tag');
  }

  /* Called when any of the category is expand/collapsed */
  onExpand = (array) => {
    console.log("array", array)
    this.setState({
      defaultExpandedKeys: array.length > 0 ? array : []
    });
  };

  render() {
    const healthDataContext = this.props.pageContext;
    let categoryTree = arrayToTree(this.state.category, {
      parentProperty: "parent",
      customID: "id",
    });
    let mailurl = "/printmail/" + healthDataContext.element.nid + ""
    return (
      <>
        {/* {this.state.load == true && ( */}
        <Layout
          //  update={this.state.lastUpdatedDate}
          update={healthDataContext.element.fieldPublishedAt != null ? healthDataContext.element.fieldPublishedAt.date !== null ? moment(healthDataContext.element.fieldPublishedAt.date).isValid()==true? moment(healthDataContext.element.fieldPublishedAt.date).format("MM/DD/YYYY"):'' : '' : ''}
          crumbLabel="Health Answers"
          link="/answered-questions/recent"
          title={healthDataContext.element.title}
          newQA={
            <>
              <div className="success-wrap-nobackground">
                {this.state.DESIRED_TEXT != "" && this.state.DESIRED_TEXT != null && (
                  <SuccessMessage message={this.state.DESIRED_TEXT} />
                )}
              </div>
            </>
          }
          //originally={this.state.originallyPublishedDate}
          originally={healthDataContext.element.fieldLastReviewedOn != null ? healthDataContext.element.fieldLastReviewedOn.date !== null ? moment(healthDataContext.element.fieldLastReviewedOn.date).isValid()==true?moment(healthDataContext.element.fieldLastReviewedOn.date).format("MM/DD/YYYY"):'' : '' : ''}
          sidebar={
            <>
              <CTA />
              <SecondaryMenu title={blockTitle} list={linkList} />
              {this.state.load == true && (this.state.healthData.length !== 0 && this.state.healthData.element.fieldResources.length !== 0 ? <SecondaryMenu title="Resources" list={this.state.healthData.length !== 0 ? this.state.healthData.element.fieldResources : []} /> : '')}
              {/* {this.state.tagsData.length !== 0 ? <SecondaryMenu title="Health topics" list={this.state.tagsData.length !== 0 ? this.state.tagsData : []} /> : ''} */}
              {this.state.load == true && (this.state.relatedquestionsData.length !== 0 ? <SecondaryMenu title="Related questions" list={this.state.relatedquestionsData.length !== 0 ? this.state.relatedquestionsData : []} /> : '')}

            </>
          }
          content={
            <>
              {/* <TreeCollapse categoryData={categoryTree} onExpand={this.onExpand} defaultExpandedKeys={this.state.defaultExpandedKeys} renderData={this.renderData} form="details-page" selectable={true} /> */}
              <div id="tree" className="categorylist-wrap"></div>
              {/* <div className="category-wrap">
                {this.state.load == true && (this.state.tagsData.length !== 0 ? <SecondaryMenu title="Health topics" list={this.state.tagsData.length !== 0 ? this.state.tagsData : []} /> : '')}
              </div> */}
              <PageTitle PageTitle={healthDataContext.element.title} />
              {healthDataContext.element.fieldQuestion !== null ?
                <QuestionBlock themeQuestions={healthDataContext.element.fieldQuestion} checkable={false} /> : ''}
              {healthDataContext.element.fieldAnswer !== null ? <AnswerBlock themeAnswers={healthDataContext.element.fieldAnswer} /> : ''}
              <SocialShare url={`${config.REST_API_URL}node/${healthDataContext.element.nid}/printable/print`} mailUrl={mailurl} pdfUrl={`
              ${config.REST_API_URL}node/${healthDataContext.element.nid}/printable/pdf`} />
              <div className="response-form">
                <CommentForm message={this.state.message} verifyCallback={this.verifyCallback} handlechange={this.handleChange} handleSubmit={this.submit} btnName={this.state.btn} error={this.state.error} reCaptchaerror={this.state.reCaptchaerror} />
              </div>
              {this.state.load == true && (this.state.response.length > 0 && (
                <ResponseBlock Responses={this.state.response} />
              ))}

            </>
          } />
        {/* )} */}

      </>
    );
  }
}
const checkExternallinks = (path) => {
  const url = new URL(path);
  //console.log(url.hostname); 
  var urls = ["www.columbia.edu", "columbia.edu", "goaskalice.columbia.edu", "www.goaskalice.columbia.edu"];
  if (urls.includes(url.hostname.trim()) == false) {
    var result = window.confirm("This external link provides additional information outside of the Go Ask Alice! and the Columbia University domains.");
    if (result === true) {
      window.open(path, "_blank")
    }
  } else {
    window.open(path, "_blank")
  }
}