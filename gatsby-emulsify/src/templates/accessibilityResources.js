import React from 'react'
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { Layout } from '../../stories/04-templates/layout.stories';
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { HealthResources } from '../../stories/03-organisms/health-resources/health-resources.stories';

class Form extends React.Component {

  constructor(props) {
    super(props);
    this.state = { show: false, title: '', question: '', categoryitem: this.props.pageContext.element[0].tid, btn: "Submit", error: false }
  }

  render() {
    return (
      <Layout
        pageTitle={
          <PageTitle PageTitle="Accessibility Resources" />
        }
        sidebar={
          <>
            <CTA />
          </>
        }
        content={
          <>
            <HealthResources />
          </>
        } >
      </Layout>
    )
  }
}

export default Form;
