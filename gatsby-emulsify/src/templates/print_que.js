import React from "react";
import config from '../config';
import axios from 'axios';

export default class print_que extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            data: "",
            wait: true
        }
    }

    componentDidMount() {
        axios(`${config.REST_API_URL}rest-api/gaa_print`, {
            method: 'POST',
            data: {
                "id": this.props.pageContext.element.nid,
                "format": "print",
                "entity": "node"
            },
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
        }).then(res => {
            console.log("res", res)
            this.setState({ data: res.data, wait: false })
        }).catch(err => {
            console.log(err)
            this.setState({ wait: false })
        })
    }
    createMarkup = html => {
        return { __html: html };
    };
    render() {
        const data = this.props.pageContext.element;
        return (
            <section className="print-page que-ans-print">
                {this.state.wait === true ? <div>Please wait...</div> :
                    <div dangerouslySetInnerHTML={this.createMarkup(this.state.data
                        .replace("print-links", "print-link-list")
                        .replace("block__title", "Footer menu")
                        .replace("Footer menu", "Footer menu header-title")
                        .replace(/print-footnote/g, "print-note")
                        .replace("field-label", "related-title")
                        .replace("menu", "print-contact-list")
                    )} />
                }

            </section>
        )
    }

}


