/* quizz page */
import React, { Component } from "react";
import { CTA } from '../../stories/02-molecules/cta/cta.stories';
import { QuizzesBlock } from '../../stories/03-organisms/quizzes-block/quizzes-block.stories';
import { Layout } from "../Components/layout"
import { PageTitle } from '../../stories/02-molecules/page-title/page-title.stories';
import { SocialShare } from '../../stories/02-molecules/menus/socialshare/social-share-menu.stories';
import { ListContentBlock } from '../../stories/03-organisms/list-content-block/list-content-block.stories';
import config from '../config';
import { SuccessMessage } from '../../stories/02-molecules/success-message/success-message.stories';
export default class QuizzPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      questions: [],
      count: 1,
      DESIRED_TEXT: props.location.state != null ? props.location.state.DESIRED_TEXT : ''
    }
    this.createData();
  }
  componentWillUnmount = () => {
    if (this.props.location.state != null) {
      window.history.replaceState(null, '')
    }
  }
  componentDidMount = async () => {
    if (this.props.location.state != null) {
      window.history.replaceState(null, '')
    }
  }
  /* called when one quizz answer submitted */
  loadMore = () => {
    this.setState({ count: this.state.count + 1 })
    let length = this.state.questions.length
    let count = this.state.count;
    if (count < this.state.data.length) {
      this.state.questions[length - 1].show = true
      this.state.questions.push(this.state.data[count])
    } else {
      this.state.questions[length - 1].show = true
    }
  }

  /* set data in proper format */
  createData = () => {
    const quizzData = this.props.pageContext.element
    let list = []
    let temp = quizzData.length > 0 ? quizzData[0].queryFieldQuizDetails != null ? quizzData[0].queryFieldQuizDetails.entities : [] : []
    if (temp != null && temp != undefined) {
      temp.map((element) => {
        list.push({
          value: element.fieldQuizQuestion,
          question: element.fieldQuizCallToActionText,
          type: element.fieldQuizType,
          message: element.fieldQuizMessage,
          options: element.fieldAnswers
        })
      });
    }
    this.state.data = list
    if (list.length > 0) {
      this.state.questions.push(list[0])
    }

  }

  render() {

    const quizzData = this.props.pageContext.element
    const listData = this.props.pageContext.quizz
    let url = quizzData.length > 0 ? `${config.REST_API_URL}node/${quizzData[0].nid}/printable/print` + "" : ''
    let mailurl = quizzData.length > 0 ? "/printmail/" + quizzData[0].nid + "" : ''
    let pdfurl = quizzData.length > 0 ? `${config.REST_API_URL}node/${quizzData[0].nid}/printable/pdf` : ''

    return (
      <div className="quizzes-page">
        <Layout
          crumbLabel="Quizzes"
          pageTitle={
            <PageTitle PageTitle="Quizzes" />
          }
          newQA={
           <>
               <div className="success-wrap-nobackground">
                   {this.state.DESIRED_TEXT != "" && this.state.DESIRED_TEXT != null && (
                       <SuccessMessage message={this.state.DESIRED_TEXT} />
                   )}
               </div>
           </>
       }
          sidebar={
            <>
              <CTA />
            </>
          }
          content={
            <>
              <QuizzesBlock quizzesDatas={quizzData.length > 0 ? quizzData[0] : []} listData={this.state.questions} more={this.loadMore} />
              <SocialShare url={url} mailUrl={mailurl} pdfUrl={pdfurl} />
              <ListContentBlock searchlist={listData} />
            </>
          } >
        </Layout>
      </div>
    )
  }

}

