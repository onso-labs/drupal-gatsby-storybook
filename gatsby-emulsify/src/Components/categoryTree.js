/* Category tree component */
import React from 'react'
import { FaRegPlusSquare, FaRegMinusSquare } from "react-icons/fa";
import PropTypes from 'prop-types';
import { navigate } from "gatsby"
import Tree, { TreeNode } from 'rc-tree';
import '../../node_modules/rc-tree/assets/index.css';
// import './index.css';



const STYLE = `
.rc-tree-child-tree {
  display: block;
}

.node-motion {
  transition: all .3s;
  overflow-y: hidden;
}

.node-motion-enter,
.node-motion-leave-active {
  height: 0;
}
`;

const onEnterActive = (node) => {
    return { height: node.scrollHeight };
};

const motion = {
    motionName: 'node-motion',
    motionAppear: false,
    onEnterActive,
    onLeaveStart: (node) => ({ height: node.offsetHeight }),
};


export default class TreeCollapse extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            treeData: this.props.categoryData,
            isShowQuestion: false,
        }
    }

    componentWillReceiveProps = (next) => {
        this.setState({ treeData: next.categoryData })
    }

    onExpand = (...args) => {
        console.log("args", args);
        this.props.onExpand(...args)
    };

    onSelect = (selectedKeys, info) => {
        console.log("select", selectedKeys)
        this.props.renderData(selectedKeys);
    };

    onCheck = (checkedKeys, info) => {
        let keys = [];
        let titles = [];
        info.checkedNodes.forEach(element => {
            keys.push(element.key)
            titles.push(element.props.title)
        });
        console.log("titles", titles)
        // this.props.renderData(keys,titles);
        titles = titles.map(key => key.replace("&", "AND"));
        keys = keys.join('&cat=')
        titles = titles.join('&title=')
        if (keys.length > 0 && titles.length > 0) {
            if (this.props.search !== null && this.props.search !== '' && this.props.search !== undefined) {
                navigate('/health-answers/tag?cat=' + keys + "&title=" + titles + "&search=" + this.props.search);
            } else {
                navigate('/health-answers/tag?cat=' + keys + "&title=" + titles);
            }

        } else {
            if (this.props.search !== null && this.props.search !== '' && this.props.search !== undefined) {
                navigate('/health-answers/tag?search=' + this.props.search);
            } else {
                navigate('/health-answers/tag');
            }
        }
    };

    renderIcon = (node) => {
        if (node.expanded == true && node.hasOwnProperty('children') && node.children.length > 0) {
            return <div className="icon-design"><FaRegMinusSquare /> </div>
        } else if (node.hasOwnProperty('children') && node.children.length > 0) {
            return <div className="icon-design"><FaRegPlusSquare /></div>
        }
    }

    render() {
        const { treeData } = this.state;

        const { defaultSelectedKeys, defaultCheckedKeys, defaultExpandedKeys, selectable, checkable } = this.props;

        return (
            // <div className="alcohol-sidebar">
            //     <div className="category-menu-treeview">
            <div>
                <h2 className="block__title">HEALTH TOPICS</h2>
                <style dangerouslySetInnerHTML={{ __html: STYLE }} />
                <Tree
                    className="category-tree"
                    motion={motion}
                    showLine
                    checkable={checkable}
                    checkStrictly
                    showIcon={false}
                    showLine={false}
                    selectable={selectable ? true : false}
                    onExpand={this.onExpand}
                    selectedKeys={defaultSelectedKeys}
                    checkedKeys={defaultCheckedKeys}
                    expandedKeys={defaultExpandedKeys}
                    onSelect={this.onSelect}
                    onCheck={this.onCheck}
                    treeData={treeData}
                    switcherIcon={this.renderIcon}
                />
            </div>
            // </div>
        )
    }
}
