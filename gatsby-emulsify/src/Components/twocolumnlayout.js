/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react";

import { navigate } from 'gatsby';
import { Header } from "../../stories/03-organisms/site/site-header/site-header.stories"
import { SiteUpperHeader } from "../../stories/03-organisms/site/site-upper-header/site-upper-header.stories"
import { Footer } from "../../stories/03-organisms/site/site-footer/site-footer.stories"
import { MainMenu } from '../../stories/02-molecules/menus/main-menu/main-menu.stories'
import { Breadcrumbs } from "../../stories/02-molecules/menus/breadcrumbs/breadcrumbs.stories"
require("../../stories/02-molecules/menus/main-menu/menu.js");

// import "./layout.css"

export class TwoColumnLayout extends React.Component {
  constructor() {
    super();
  }

  submit = (search) => {
    if (search !== '') {
      navigate(`/search/node?search=${search}`, { state: { search } });
      // this.setState({ title: search, load: false, wait: true })

    } else {
      // this.setState({ title: '', search: [], wait: false })
    }
  }

  navigateQuestionpage = (e, url) => {
    e.preventDefault();
    // console.log("url>>", e, window)
    if (window !== undefined && window.confirm("This external link provides additional information outside of the Go Ask Alice! and the Columbia University domains.")) {
      window.open(url, "_blank");
    }
  };

  render() {
    const { content, sidebar, pageTitle, newQA, update, crumbLabel, title, link, leftsidebar } = this.props
    return (
      <>
        <SiteUpperHeader />
        <div className="container">
          <Header handleSubmit={this.submit} />
          <MainMenu />
          <Breadcrumbs crumblabel={crumbLabel} Title={title} Link={link} />
          <div className="main-layout">
            {newQA}
            <div className="page-wrapper">
              {/* <PageTitle PageTitle={pageTitle} /> */}
              {pageTitle}
              <div className="row">
                <div className="col-sm-12 col-md-3">
                  {leftsidebar}
                </div>
                <div className="col-sm-12 col-md-6">
                  {content}
                </div>
                <div className="col-sm-12 col-md-3">
                  {sidebar}
                </div>
              </div>
            </div>
          </div>
          <Footer update={update} navigateQuestionpage={this.navigateQuestionpage} />
        </div>
      </>
    )
  }
}


