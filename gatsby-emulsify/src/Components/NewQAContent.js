/* component for recent Q&A page */
import React from 'react';
import { Links } from "../../stories/01-atoms/link/link.stories";
import moment from 'moment';

export const NewQAContent = ({ data, title }) => (
    <div className="new-qa__list-wrapper recent-qa">

        {title === "newQa" && data !== null && data.length !== 0 && data !== undefined ? (
            <React.Fragment>
                {/* <div className="new-qa__date">
              <h1>New Q&As</h1>
            </div> */}
                {data.map((ele, i) => {
                    var parts = ele.entityUrl.path.replace('%E2%80%94', '-').split('/');
                    var answer = parts[parts.length - 1];
                    let j = i == 0 ? i : i - 1;
                    if (i == 0) {
                        return (

                            <React.Fragment>
                                <div className="new-qa__date">
                                    <h2>{ele.fieldLastReviewedOn !== null ? moment(ele.fieldLastReviewedOn.value).format("MMMM DD, YYYY") : ''}</h2>
                                </div>
                                <ul>
                                    <li><Links linkTextValue={ele.title} linkRedirectTo={ele.entityUrl.path} /></li>
                                </ul>
                            </React.Fragment>

                        )
                    } else {
                        if (data[i].fieldLastReviewedOn !== null && data[j].fieldLastReviewedOn !== null &&
                            data[i].fieldLastReviewedOn.value !==
                            data[j].fieldLastReviewedOn.value
                        ) {
                            return (
                                <React.Fragment>
                                    <div className="new-qa__date">
                                        <h2>{ele.fieldLastReviewedOn !== null ? moment(ele.fieldLastReviewedOn.value).format("MMMM DD, YYYY") : ''}</h2>
                                    </div>
                                    <ul>
                                        <li><Links linkTextValue={ele.title} linkRedirectTo={ele.entityUrl.path} /></li>
                                    </ul>
                                </React.Fragment>

                            )
                        } else {
                            return (
                                <ul key={i}>
                                    <li >
                                        <Links linkTextValue={ele.title} linkRedirectTo={ele.entityUrl.path} />
                                    </li>
                                </ul>
                            )

                        }
                    }
                })}
            </React.Fragment>



        ) : ''}

        {title === "updatedQa" && data !== null && data.length !== 0 && data !== undefined ? (
            <React.Fragment>
                {/* <div className="new-qa__date">
                    <h1></h1>
                </div> */}
                {data.map((ele, i) => {
                    var parts = ele.entityUrl.path.replace('%E2%80%94', '-').split('/');
                    var answer = parts[parts.length - 1];
                    let j = i == 0 ? i : i - 1;
                    if (i == 0) {
                        return (

                            <React.Fragment>
                                <div className="new-qa__date">
                                    <h2>{ele.fieldLastReviewedOn !== null ? moment(ele.fieldLastReviewedOn.value).format("MMMM DD, YYYY") : ''}</h2>
                                </div>
                                <ul>
                                    <li><Links linkTextValue={ele.title} linkRedirectTo={ele.entityUrl.path} /></li>
                                </ul>
                            </React.Fragment>

                        )
                    } else {
                        if (data[i].fieldLastReviewedOn !== null && data[j].fieldLastReviewedOn !== null &&
                            data[i].fieldLastReviewedOn.value !==
                            data[j].fieldLastReviewedOn.value
                        ) {
                            return (
                                <React.Fragment>
                                    <div className="new-qa__date">
                                        <h2>{ele.fieldLastReviewedOn !== null ? moment(ele.fieldLastReviewedOn.value).format("MMMM DD, YYYY") : ''}</h2>
                                    </div>
                                    <ul>
                                        <li><Links linkTextValue={ele.title} linkRedirectTo={ele.entityUrl.path} /></li>
                                    </ul>
                                </React.Fragment>

                            )
                        } else {
                            return (
                                <ul key={i}>
                                    <li >
                                        <Links linkTextValue={ele.title} linkRedirectTo={ele.entityUrl.path} />
                                    </li>
                                </ul>
                            )

                        }
                    }
                })}
            </React.Fragment>
        ) : ''}




    </div>
);
