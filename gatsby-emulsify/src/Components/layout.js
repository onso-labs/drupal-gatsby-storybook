/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */
/* header and footer of the site */
import React from "react";

import { navigate } from 'gatsby';
import { Header } from "../../stories/03-organisms/site/site-header/site-header.stories"
import { SiteUpperHeader } from "../../stories/03-organisms/site/site-upper-header/site-upper-header.stories"
import { Footer } from "../../stories/03-organisms/site/site-footer/site-footer.stories"
import { MainMenu } from '../../stories/02-molecules/menus/main-menu/main-menu.stories'
import { Breadcrumbs } from "../../stories/02-molecules/menus/breadcrumbs/breadcrumbs.stories"
import jQuery from "jquery";
require("../../stories/02-molecules/menus/main-menu/menu.js");

// import "./layout.css"

export class Layout extends React.Component {
  constructor() {
    super();
  }
  componentDidMount = async () => {
    jQuery('.externalLink').on('click', function (e) {
      if (window !== undefined) {
        e.preventDefault();
        checkExternallinks(this.href);
      }
    });
  }
  /* when hit search from the header */
  submit = (search) => {
    if (search.trim().length>0) {
      navigate(`/health-answers/tag?search=${search}`, { state: { search } });
      // navigate(`/search/node?search=${search}`, { state: { search } });
      // this.setState({ title: search, load: false, wait: true })

    } else {
      // this.setState({ title: '', search: [], wait: false })
    }
  }

  /* navigate to the page with searched content */
  navigateQuestionpage = (e, url) => {
    e.preventDefault();
    // console.log("url>>", e, window)
    if (window !== undefined && window.confirm("This external link provides additional information outside of the Go Ask Alice! and the Columbia University domains.")) {
      window.open(url, "_blank");
    }
  };

  render() {
    const { content, sidebar, pageTitle, newQA, update, crumbLabel, title, link,originally,} = this.props
    return (
      <>
        <SiteUpperHeader />
        <div className="container">
          <Header handleSubmit={this.submit} />
          <MainMenu />
          <Breadcrumbs crumblabel={crumbLabel} Title={title} Link={link} />
          <div className="main-layout">
            {newQA}
            <div className="page-wrapper">
              {/* <PageTitle PageTitle={pageTitle} /> */}
              {pageTitle}
              <div className="row">
                <div className="col-sm-12 col-md-8">
                  {content}
                </div>
                <div className="col-sm-12 col-md-4">
                  {sidebar}
                </div>
              </div>
            </div>
          </div>
          <Footer update={update} originally={originally} navigateQuestionpage={this.navigateQuestionpage} />
        </div>
      </>
    )
  }
}


const checkExternallinks = (path) => {
  const url = new URL(path);
  //console.log(url.hostname); 
  var urls = ["www.columbia.edu", "columbia.edu", "goaskalice.columbia.edu", "www.goaskalice.columbia.edu"];
  if (urls.includes(url.hostname.trim()) == false) {
    var result = window.confirm("This external link provides additional information outside of the Go Ask Alice! and the Columbia University domains.");
    if (result === true) {
      window.open(path, "_blank")
    }
  } else {
    window.open(path, "_blank")
  }
}