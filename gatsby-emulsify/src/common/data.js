const linkList = [
  {
    "entity": {
      "entityLabel": "Get Alice! In Your Box",
      "entityUrl": {
        "routed": true,
        "path": "/contact-alice/get-alice-in-your-inbox"
      }
    }
  }
]
const blockTitle = "Subscribe to Alice!"

const ravesDetail = {
  paragraph: "Dear Readers, Go Ask Alice! would not be what it is had it not been for all of you, its readers. Without you, no questions would be asked, no Q&As would be browsed and read, no feedback/comments would be submitted, no site would remain in publication. In recognition of Go Ask Alice!'s readership, which helps keep the site going and continue to serve as a health information resource, it's only appropriate to share some kudos and criticisms from readers. Thank you, Alice!"
}

export { linkList, blockTitle, ravesDetail }