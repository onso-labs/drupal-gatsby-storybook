/* Query for home page theme block data */
const themeBlock_query = `query {
    alice {
      gqThemeoftheWeekGraphqlView {
        results {
          entityType
          entityLabel
          body {
            value
            format
            processed
            summary
            summaryProcessed
          }
          entityBundle
          fieldWeek {
            value
            date
          }
          queryFieldHealthAnswer {
            entities {
              ... fragmentComponent
            }
          }
        }
      }
      }
    }
    fragment fragmentComponent on ALICE_NodeHealthAnswer {
        title
        entityUrl {
          routed
          path
        }
      }`

module.exports = { themeBlock_query };