/* Query for testimonial data */
const testimonial_query = `query {
    alice{
        testimonialGraphql1View{
    results{
      body {
        value
        format
        processed
        summary
        summaryProcessed
      }
    }
  }
    }
  }
  `

module.exports = { testimonial_query };