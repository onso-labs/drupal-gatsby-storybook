/* Query for home page health answer block data */
const homepagehealthanswer_query = `query{
    alice{
        gqhomepagehealthanswer{
        results {
            nid
            title
              entityUrl {
                routed
                path
              }
            fieldQuestion {
                value
                format
                processed
              }
              fieldAnswer {
                value
                format
                processed
                summary
                summaryProcessed
              }
              fieldCategory {
                targetId
                entity {
                  entityUrl {
                    routed
                    path
                  }
                  entityLabel
                }
              }
              fieldResources {
                targetId
                entity {
                  entityLabel
                  entityUrl {
                    routed
                    path
                  }
                }
              }
              fieldService {
                targetId
                entity{
                  tid
                  entityLabel
                  entityUrl {
                    routed
                    path
                  }
                }
              }
              fieldRelatedHealthAnswer {
                targetId
                entity{
                  entityLabel
                  nid
                  entityUrl {
                    routed
                    path
                  }
                }
              }
          }
    }
    }
  }
  `
module.exports = { homepagehealthanswer_query };