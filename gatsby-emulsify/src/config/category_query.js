/* Query for category data */
const category_query = `query{
    alice{
      gqCategoriesGraphql1View {
    results{
    	name
      tid
      entityUrl {
        routed
        path
      }
      parent {
        targetId
        entity {
          entityLabel
          tid
        }
      }
    }
  }
    }
  }
  `

module.exports = { category_query };