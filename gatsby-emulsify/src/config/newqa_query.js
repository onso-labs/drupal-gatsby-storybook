/* Query for 2new Q&A data for recent Q&A page */
const newqa_query = `query {
  alice {
    gqNewqaHealthAnswersGraphqlView {
      results {
        title
        nid
        entityUrl {
        routed
        path
        }
        status
        created
        fieldPublishedAt {
        date
        value
        }
        fieldLastReviewedOn {
          date
          value
        }
      }
    }

}
}


`

module.exports = { newqa_query };