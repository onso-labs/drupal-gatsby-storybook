/* Query for poll page's data */
const pollList_query = `query {
    alice{
     morepollListGraphqlView{
    results{
      entityType
      entityBundle
      entityLabel
      entityId
      entityCreated
      entityUrl {
          routed
          path
      }
      ...fragmentChoice
    }
  }
    }
  }

  fragment fragmentChoice on ALICE_Poll {
  choice {
    targetId
    entity{
      entityLabel
    }
  }
}
  `

module.exports = { pollList_query };