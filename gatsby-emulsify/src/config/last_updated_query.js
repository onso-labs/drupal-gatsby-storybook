/* Query for last updated data */
const last_updated_query = `query {
    alice{
        lastUpdatedFrontPageGraphqlView {
            results {
              changed
            }  
          }
    }
  }
  `

module.exports = { last_updated_query };