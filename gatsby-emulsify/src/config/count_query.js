/* Query for total count of health answers */
const count_query = `query{
    alice{
        gqHealthAnswersGraphqlViewCount{
            count
        }
    }
  }
  `

module.exports = { count_query };