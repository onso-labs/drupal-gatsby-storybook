/* Query for quizz page's data */
const quizzBlock_query = `query {
    alice {
        gqQuizzHomeGraphqlView {
    results {
      nid
      title
      entityUrl {
        path
      }
       queryFieldQuizDetails{
        entities {
          entityId
          entityLabel
          ... fragmentQuizBlock
        }
      }
    }
  }
      }
    }
    
    fragment fragmentQuizBlock on ALICE_BlockContentQuiz {
  body {
    value
    format
    processed
    summary
    summaryProcessed
  }
  fieldQuizQuestion
  fieldAnswers
  fieldQuizMessage
  fieldQuizType
  fieldQuizCallToActionText
}
    `

module.exports = { quizzBlock_query };