/* Query for recent Q&A page's updated QA data */
const updatedQA_query = `query {
    alice {
      gqUpdatedQaGraphqlView {
        results {
          title
          nid
          entityUrl {
            routed
            path
          }
          status
          created
          fieldLastReviewedOn {
            date
            value
          }
        }
      }
      }
    }`

module.exports = { updatedQA_query };