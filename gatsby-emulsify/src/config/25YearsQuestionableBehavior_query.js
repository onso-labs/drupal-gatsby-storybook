/* Query for 25 years of questionable behaviour page's data */
const yearsQuestionableBehavior_query = `query{
    alice{
      gq25YearsOfQuestionableBehaviorFormContentGraphqlView {
        results {
         title
        fieldFormtype
        body {
              summary
              value
            }
        nid
        }  
      }
    }
  }
  `
module.exports = { yearsQuestionableBehavior_query };