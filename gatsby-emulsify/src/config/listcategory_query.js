/* Query for ask your question form page's category dropdown data */
const listcategory_query = `query{
    alice{
        gqSelectListCategoriesGraphqlView {
            results{
                name
              tid
              entityUrl {
                routed
                path
              }
              parent {
                targetId
                entity {
                  entityLabel
                  tid
                }
              }
            }
          }
    }
  }
  `

module.exports = { listcategory_query };