/* Query for all health answers data */
const health_query = (page, pagesize) => {
  return `query{
    alice{
      gqHealthAnswersGraphqlViewResults(page:${page}, pageSize:${pagesize}){
        count
        results {
          nid
          title
            entityUrl {
              routed
              path
            }
          fieldQuestion {
              value
              format
              processed
            }
            fieldAnswer {
              value
              format
              processed
              summary
              summaryProcessed
            }
            fieldCategory {
              targetId
              entity {
                entityUrl {
                  routed
                  path
                }
                entityLabel
              }
            }
            fieldResources {
              targetId
              entity {
                entityLabel
                entityUrl {
                  routed
                  path
                }
              }
            }
            fieldService {
              targetId
              entity{
                tid
                entityLabel
                entityUrl {
                  routed
                  path
                }
              }
            }
            fieldRelatedHealthAnswer {
              targetId
              entity{
                entityLabel
                nid
                entityUrl {
                  routed
                  path
                }
              }
            }
         fieldPublishedAt{
            date
            value
          }
          fieldLastReviewedOn{
            date
            value
          }
        }
      }
    }
  }`
}




// `query{
//   alice{
//     gqHealthAnswersGraphqlView {
//       results {
//         title
//           entityUrl {
//             routed
//             path
//           }
//         fieldQuestion {
//             value
//             format
//             processed
//           }
//           fieldAnswer {
//             value
//             format
//             processed
//             summary
//             summaryProcessed
//           }
//           fieldCategory {
//             targetId
//             entity {
//               entityUrl {
//                 routed
//                 path
//               }
//               entityLabel
//             }
//           }
//           fieldResources {
//             targetId
//             entity {
//               entityLabel
//               entityUrl {
//                 routed
//                 path
//               }
//             }
//           }
//       }
//     }
// }
// }`

module.exports = { health_query };