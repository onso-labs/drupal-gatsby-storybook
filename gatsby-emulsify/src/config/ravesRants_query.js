/* Query for raves and rants page's data */
const ravesRants_query = `query {
    alice{
        gqRavesAndRantsGraphqlView {
            results {
            id
            fieldDescription {
                value
                format
                processed
                summary
                summaryProcessed
            }
            fieldLikeDislike
            }
        }
    }
  }
  `

module.exports = { ravesRants_query };