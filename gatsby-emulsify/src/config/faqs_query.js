/* Query for faq page's data */
const faqs_query = `query {
    alice {
      gqFaqsGraphqlView {
        results {
          nid
          title
          entityUrl {
            routed
            path
          }
          fieldFaq {
            question
            answer
            answerFormat
          }
        }
      }
    }
  }`

module.exports = { faqs_query };