/* Query for home page weekly poll data */
const pollHome_query = `query {
    alice{
      pollListGraphql1View(page:0, pageSize:1){
    results{
      entityType
      entityBundle
      entityLabel
      entityId
      entityCreated
      entityUrl {
          routed
          path
      }
      ...fragmentChoice
    }
  }
    }
  }

  fragment fragmentChoice on ALICE_Poll {
  choice {
    targetId
    entity{
      entityLabel
    }
  }
}
  `

module.exports = { pollHome_query };