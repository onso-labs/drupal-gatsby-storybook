/* Query for factsheet page's data */
const fact_query = `query {
  alice {
       gqFactsheetGraphqlView {
    results {
      nid
        title
        entityUrl {
          routed
          path
        }
        fieldResources {
          targetId
          entity {
            entityLabel
             ... fragmentResource
          }
        }
        fieldService {
          targetId
          entity {
            entityLabel
            entityUrl {
              routed
              path
            }
          }
        }
        body {
          summary
          value
        }
    }
  }
  }
}

fragment fragmentResource on ALICE_NodeResource {
  body {
    value
    format
    processed
    summary
    summaryProcessed
  }
  fieldLocation
  fieldPhone
  fieldWebsite {
    uri
    title
    options
  }
}
`

module.exports = { fact_query };