/* Query for footer page's data */
const footer_query = `query {
    alice {
        gqFormContentGraphql1View {
          results {
          nid
          title
          fieldFormtype
          body {
                summary
                value
              }
          }  
        }
    }
  }  `

module.exports = { footer_query };