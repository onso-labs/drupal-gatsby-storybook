/* Query for on campus page's data */
const oncampusresource_query = `query {
    alice{
      gqOncampusresourceGraphqlView {
        results {
          nid
          title
          entityId
          queryFieldResource{
            entities {
              entityId
              entityLabel
              ... fragmentResource
            }
          }
        }
      }
    }
  }

  fragment fragmentResource on ALICE_NodeResource {
    body {
      value
      format
      processed
      summary
      summaryProcessed
    }
    fieldLocation
    fieldPhone
    fieldWebsite {
      uri
      title
      options
    }
  }
  `

module.exports = { oncampusresource_query };