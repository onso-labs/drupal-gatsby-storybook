/* Query for go ask alice history page's data */
const history_query = `query {
    alice {
      gqHistoryGraphqlView {
        results {
              nid
                title
                body {
                  value
                  format
                  processed
                }
                entityUrl {
                  path
                }
                fieldLogo {
                  entity {
                        ... mediaFragment 
                    }
                }
        }
      }
      }
    }
  
    fragment mediaFragment on ALICE_MediaImage {
      fieldMediaImage {
        targetId
        alt
        title
        width
        height
        url
      }
    }`

module.exports = { history_query };