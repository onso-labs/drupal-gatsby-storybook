/* Query for emergency page's data */
const emergency_query = `query{
    alice{
         gqEmergencyColumbiaGraphqlView {
    results {
      title
      entityId
			queryFieldResource{
        entities {
          entityId
        	entityLabel
          ... fragmentResource
        }
      }
    }
  }
    }
  }

  fragment fragmentResource on ALICE_NodeResource {
  body {
    value
    format
    processed
    summary
    summaryProcessed
  }
  fieldLocation
  fieldPhone
  fieldWebsite {
    uri
    title
    options
  }
}
  `

module.exports = { emergency_query };