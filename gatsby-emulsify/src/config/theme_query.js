/* Query for theme page's data */
const theme_query = `query {
  alice {
    gqThemeoftheWeeklistingGraphqlView {
      results {
        entityType
        entityBundle
        entityLabel
        body {
          value
          format
          processed
          summary
          summaryProcessed
        }
        fieldWeek {
          value
          date
        }
        queryFieldHealthAnswer {
          entities {
            ... fragmentComponent
          }
        }
      }
    }
    }
  }
  fragment fragmentComponent on ALICE_NodeHealthAnswer {
  title
  entityUrl {
    routed
    path
  }
fieldQuestion {
  value
  format
  processed
}
    }`

module.exports = { theme_query };