/* Query for quizz page's data */
const morequizz_query = `query {
  alice {
      gqQuizzDetailGraphqlView {
        results {
          nid
          title
          body {
            value
            format
            processed
          }
          entityUrl {
            path
          }
           queryFieldQuizDetails{
            entities {
              entityId
              entityLabel
              ... fragmentQuizBlock
            }
          }
        }
        }
    }
  }
  fragment fragmentQuizBlock on ALICE_BlockContentQuiz {
    body {
      value
      format
      processed
      summary
      summaryProcessed
    }
    fieldQuizQuestion
    fieldAnswers
    fieldQuizMessage
    fieldQuizType
    fieldQuizCallToActionText
  }`

module.exports = { morequizz_query };