/* Query for poll page's data */
const morepoll_query = `query {
    alice {
        pollListGraphql1View{
    results{
      entityType
      entityBundle
      entityLabel
      entityId
      entityCreated
      entityUrl {
          routed
          path
      }
      ...fragmentChoice
    }
  }
      }
    }
    
    fragment fragmentChoice on ALICE_Poll {
  choice {
    targetId
    entity{
      entityLabel
    }
  }
}
    
    `

module.exports = { morepoll_query };