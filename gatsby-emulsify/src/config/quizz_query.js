/* Query for quizz page's data */
const quizz_query = `query {
  alice{
    gqMoreQuizzGraphqlView {
      results {
        nid
        title
        body {
          value
          format
          processed
        }
        entityUrl {
          path
        }
         queryFieldQuizDetails{
          entities {
            entityId
            entityLabel
            ... fragmentQuizBlock
          }
        }
      }
    }
  }
}
fragment fragmentQuizBlock on ALICE_BlockContentQuiz {
  body {
    value
    format
    processed
    summary
    summaryProcessed
  }
  fieldQuizQuestion
  fieldAnswers
  fieldQuizMessage
  fieldQuizType
  fieldQuizCallToActionText
}`

module.exports = { quizz_query };