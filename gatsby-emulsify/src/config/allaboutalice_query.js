/* Query for all about page's data */
const allaboutalice_query = `query{
    alice{
        gqhomePageAllAboutAliceGraphqlView {
            results {
              title
              body {
                value
                format
                processed
              }
              entityUrl {
                routed
                path
              }
            }
          }
    }
  }
  `

module.exports = { allaboutalice_query };