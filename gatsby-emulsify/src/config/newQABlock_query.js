/* Query for home page's new qa block data */
const newQABlock_query = `query {
    alice {
        gqNewqaGraphqlView {
            results {
              entityType
              entityBundle
              ... fragmentComponent
            }
          }
      }
    }
    fragment fragmentComponent on ALICE_BlockContent {
        ... on ALICE_BlockContentNewHealthAnswers {
          
          fieldWeek {
            value
            date
          }
          fieldTitle
          fieldHealthAnswer {
            targetId
            entity {
              title
              entityUrl {
                path
                routed
              }
            }
          }
          
        }
      
      }
  
   `

module.exports = { newQABlock_query };