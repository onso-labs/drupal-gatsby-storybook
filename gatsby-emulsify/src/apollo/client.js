import fetch from 'isomorphic-fetch';

import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { RestLink } from 'apollo-link-rest';


const restLink = new RestLink({
  // uri: 'http://192.168.0.60/html/bask/',
  uri: 'https://dev-54ta5gq-whlhb3bjsmuos.us-2.platformsh.site/'
});

export const client = new ApolloClient({
  link: restLink,
  cache: new InMemoryCache(),
});
