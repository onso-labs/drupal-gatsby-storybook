/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

// You can delete this file if you're not using it
import React from 'react';
import { ApolloProvider } from 'react-apollo';
import { client } from './src/apollo/client';
import './css/main.scss';

// import 'bootstrap/dist/css/bootstrap.css';

// if (typeof window !== `undefined`) {

//   window.Bootstrap = require('bootstrap');
// }
// else{
//   console.log("here")
// }

// import './src/assets/styles/application.scss';

window.$ = window.jQuery = require('jquery'); // not sure if you need this at all
// import './src/global.css';

export const wrapRootElement = ({ element, props }) => (
  <ApolloProvider client={client}>{element}</ApolloProvider>
);
