const path = require('path')

/* Imported all the templates and graphql queries */
const factsheet_template = path.resolve('./src/templates/factTemplate.js');
const factsheets = path.resolve('./src/templates/factsheets.js');
const question = path.resolve('./src/templates/question.js');
const healthanswersDeatil_template = path.resolve('./src/templates/healthAnswerDetailTemplate.js');
const health_template = path.resolve('./src/templates/healthAnswersTemplate.js');
const themeweekDetailTemplate = path.resolve('./src/templates/themeweekDetailTemplate.js');
const fact_query = require('./src/config/fact_query.js');
const health_query = require('./src/config/health_query.js');
const category_query = require('./src/config/category_query.js');
const listcategory_query = require('./src/config/listcategory_query.js');
const oncampusresource_query = require('./src/config/oncampusresource_query.js');
const faqs_query = require('./src/config/faqs_query.js');
const history_query = require('./src/config/histrory_query.js');
const last_updated_query = require('./src/config/last_updated_query.js');
const quizzBlock_query = require('./src/config/quizzBlock_query');
const quizz_query = require('./src/config/quizz_query');
const morequizz_query = require('./src/config/morequizz_query');
const newqa_query = require('./src/config/newqa_query.js');
const newQABlock_query = require('./src/config/newQABlock_query');
const themeBlock_query = require('./src/config/themeBlock_query');
const testimonial_query = require('./src/config/testimonial_query');
const theme_query = require('./src/config/theme_query');
const updatedQA_query = require('./src/config/updatedQA_query.js');
const count_query = require('./src/config/count_query.js');
const footer_query = require('./src/config/footer_query.js');
const ravesRants_query = require('./src/config/ravesRants_query.js');
const emergency_query = require('./src/config/emergency_query.js');
const pollHome_query = require('./src/config/pollHome_query.js');
const morepoll_query = require('./src/config/morepoll_query.js');
const pollList_query = require('./src/config/pollList_query.js');
const homepagehealthanswer_query = require('./src/config/homepagehealthanswer_query.js');
const yearsQuestionableBehavior_query = require('./src/config/25YearsQuestionableBehavior_query');
const oncampusresource = path.resolve('./src/templates/onCampusResource.js');
const faqs = path.resolve('./src/templates/faqs.js');
const history = path.resolve('./src/templates/history.js');
const theme = path.resolve('./src/templates/themeweekTemplate.js');
const quizzDetailTemplate = path.resolve('./src/templates/quizzDetailTemplate.js');
const homepage = path.resolve('./src/templates/index.js');
const newQA = path.resolve('./src/templates/newQATemplate.js');
const quizzTemplate = path.resolve('./src/templates/quizzTemplate.js');
const collaboration = path.resolve('./src/templates/collaboration.js')
const comments = path.resolve('./src/templates/commentsCorrections.js')
const feed = path.resolve('./src/templates/feedRequest.js')
const licensing = path.resolve('./src/templates/licensing.js')
const press = path.resolve('./src/templates/pressRequest.js')
const syndication = path.resolve('./src/templates/syndication.js')
const alice = path.resolve('./src/templates/goAskAlice.js')
const ravesPage = path.resolve('./src/templates/ravesRants.js')
const getAliceInBoxPage = path.resolve('./src/templates/getAliceInBox.js')
const emergencyPage = path.resolve('./src/templates/emergency.js')
const allAboutAlicePage = path.resolve('./src/templates/allAboutAlice.js')
const searchPage = path.resolve('./src/templates/search.js')
const printquePage = path.resolve('./src/templates/print_que.js')
const printmailPage = path.resolve('./src/templates/printmail.js')
const pollTemplate = path.resolve('./src/templates/pollTemplate.js')
const pollDetailTemplate = path.resolve('./src/templates/pollDetailTemplate.js');
const allaboutalice_query = require('./src/config/allaboutalice_query.js');
const yearsQuestionableBehaviorpage = path.resolve('./src/templates/25YearsQuestionableBehavior.js')

/* Create pages using template */
exports.createPages = async ({ graphql, actions }) => {

  /* Called graphql query */
  const factResult = await graphql(fact_query.fact_query)
  

  const categoryResult = await graphql(category_query.category_query)

  const listcategoryResult = await graphql(listcategory_query.listcategory_query)

  const oncampusresourceResult = await graphql(oncampusresource_query.oncampusresource_query);

  const faqsResult = await graphql(faqs_query.faqs_query);

  const historyResult = await graphql(history_query.history_query);

  const quizzResult = await graphql(quizz_query.quizz_query);

  const newqaResult = await graphql(newqa_query.newqa_query);

  const newQABlockResult = await graphql(newQABlock_query.newQABlock_query);

  const themeBlockResult = await graphql(themeBlock_query.themeBlock_query)

  const themeResult = await graphql(theme_query.theme_query);

  const updatedQAResult = await graphql(updatedQA_query.updatedQA_query);

  const lastUpdatedResult = await graphql(last_updated_query.last_updated_query);

  const quizzBlockResult = await graphql(quizzBlock_query.quizzBlock_query);

  const countResult = await graphql(count_query.count_query);

  const morequizzResult = await graphql(morequizz_query.morequizz_query);

  const footerResult = await graphql(footer_query.footer_query)

  const ravesRantsResult = await graphql(ravesRants_query.ravesRants_query)

  const emergencyResult = await graphql(emergency_query.emergency_query)

  const testimonialResult = await graphql(testimonial_query.testimonial_query)

  const pollHomeResult = await graphql(pollHome_query.pollHome_query)

  const morepollResult = await graphql(morepoll_query.morepoll_query)

  const pollListResult = await graphql(pollList_query.pollList_query)

  const homepagehealthanswerResult = await graphql(homepagehealthanswer_query.homepagehealthanswer_query)

  const allaboutaliceResult = await graphql(allaboutalice_query.allaboutalice_query)

  const yearsQuestionableBehaviorResult = await graphql(yearsQuestionableBehavior_query.yearsQuestionableBehavior_query)

  const { createPage } = actions;

  let factdata = factResult.data.alice.gqFactsheetGraphqlView.results
  let categorydata = categoryResult.data.alice.gqCategoriesGraphql1View.results
  let listcategorydata = listcategoryResult.data.alice.gqSelectListCategoriesGraphqlView.results
  let oncampusresourcedata = oncampusresourceResult.data.alice.gqOncampusresourceGraphqlView.results
  let faqsdata = faqsResult.data.alice.gqFaqsGraphqlView.results;
  let historydata = historyResult.data.alice.gqHistoryGraphqlView.results;
  let quizzdata = quizzResult.data.alice.gqMoreQuizzGraphqlView.results;
  let newqadata = newqaResult.data.alice.gqNewqaHealthAnswersGraphqlView.results;
  let updatedQAdata = updatedQAResult.data.alice.gqUpdatedQaGraphqlView.results;
  let newQABlockdata = newQABlockResult.data.alice.gqNewqaGraphqlView.results;
  let themeBlockdata = themeBlockResult.data.alice.gqThemeoftheWeekGraphqlView.results;
  let themedata = themeResult.data.alice.gqThemeoftheWeeklistingGraphqlView.results;
  let lastupdateddata = lastUpdatedResult.data.alice.lastUpdatedFrontPageGraphqlView.results;
  let quizzBlockData = quizzBlockResult.data.alice.gqQuizzHomeGraphqlView.results;
  let countData = countResult.data.alice.gqHealthAnswersGraphqlViewCount.count;
  let morequizzData = morequizzResult.data.alice.gqQuizzDetailGraphqlView.results;
  let footerData = footerResult.data.alice.gqFormContentGraphql1View.results;
  let ravesRantsData = ravesRantsResult.data.alice.gqRavesAndRantsGraphqlView.results;
  let emergencyData = emergencyResult.data.alice.gqEmergencyColumbiaGraphqlView.results;
  let testimonialData = testimonialResult.data.alice.testimonialGraphql1View.results;
  let pollHomeData = pollHomeResult.data.alice.pollListGraphql1View.results;
  let morepollData = morepollResult.data.alice.pollListGraphql1View.results;
  let pollListData = pollListResult.data.alice.morepollListGraphqlView.results;
  let homepagehealthanswerData = homepagehealthanswerResult.data.alice.gqhomepagehealthanswer.results;
  let allaboutaliceData = allaboutaliceResult.data.alice.gqhomePageAllAboutAliceGraphqlView.results;
  let yearsQuestionableBehaviorData = yearsQuestionableBehaviorResult.data.alice.gq25YearsOfQuestionableBehaviorFormContentGraphqlView;
  //  let page = countData % 200;
  console.log("countData", countData)
  let total_page = Math.ceil(countData / 200);
  // let total_page = ((countData - page) / 200) + page
  console.log("total", total_page)

  /* Creating health answer detail page */
  
  for (var i = 0; i <= total_page; i++) {
    
    console.log("page", i);
    const healthResult = await graphql(health_query.health_query(i, 200))
    if (healthResult.data.alice.gqHealthAnswersGraphqlViewResults != null) {
      let healthdata = healthResult.data.alice.gqHealthAnswersGraphqlViewResults.results;
      healthdata.map((health, i) => {
        let healthPath = health.entityUrl.path
        // let printPath = `/print/${health.nid}`
        let printmailPath = `/printmail/${health.nid}`
        createPage({
          path: healthPath,
          component: healthanswersDeatil_template,
          // id: health.nid,
          context: {
            element: health,
            category: categorydata
          },
        });
        // createPage({
        //   path: printPath,
        //   component: printquePage,
        //   // id: health.nid,
        //   context: {
        //     element: health,
        //   }
        // })

        /* Create email page for each health answer detail */
        createPage({
          path: printmailPath,
          component: printmailPage,
          // id: health.nid,
          context: {
            element: health,
            title: healthdata[i].title,
            paths: healthPath
          }
        })

      })
    }
  }

  quizzdata.map((quizz, i) => {
    let quizzPath = quizz.entityUrl.path
    // let printPath = '/print/' + quizz.nid + ''
    let printmailPath = `/printmail/${quizz.nid}`
    /* create  quizz detail page */
    createPage({
      path: quizzPath,
      component: quizzDetailTemplate,
      id: quizz.nid,
      context: {
        element: quizz,
        quizz: quizzdata
      }
    });
  });
  morequizzData.map((quizz, i) => {
    let quizzPath = quizz.entityUrl.path
    // let printPath = '/print/' + quizz.nid + ''
    let printmailPath = `/printmail/${quizz.nid}`
    /* create  quizz detail page */
    createPage({
      path: quizzPath,
      component: quizzDetailTemplate,
      id: quizz.nid,
      context: {
        element: quizz,
        quizz: []
      }
    });
    // createPage({
    //   path: printPath,
    //   component: printquePage,
    //   id: `1`,
    //   context: {
    //     element: quizz,
    //   }
    // });
    /* create email page for each quizz detail */
    createPage({
      path: printmailPath,
      component: printmailPage,
      id: `1`,
      context: {
        element: quizz,
        title: quizz.title,
        paths: quizzPath
      }
    });
  })

  // if (faqsdata.length > 0) {
  //   let printpath = '/print/' + faqsdata[0].nid + ''
  //   createPage({
  //     path: printpath,
  //     component: printquePage,
  //     // id: health.nid,
  //     context: {
  //       element: faqsdata.length > 0 ? faqsdata[0] : [],
  //       title: 'FAQs',
  //       paths: '/about-alice/faqs'
  //     }
  //   })
  // }

  /*Create Polls page */
  createPage({
    path: '/polls',
    component: pollTemplate,
    id: `1`,
    context: {
      element: morepollData,
      list: pollListData
    }
  })

  /* Create poll detail page */
  pollListData.map((item, i) => {
    createPage({
      path: item.entityUrl.path,
      component: pollDetailTemplate,
      context: {
        element: item,
        list: pollListData
      }
    })
  })

  /* create search page */
  createPage({
    path: '/search/node',
    component: searchPage,
    id: `1`,
    context: {
      element: categorydata,
    }
  })

  /* Create raves and rants page */
  createPage({
    path: '/about-alice/raves-rants',
    component: ravesPage,
    id: `1`,
    context: {
      element: ravesRantsData,
    }
  })

  /* create link to go ask alice page */
  createPage({
    path: '/about-alice/link-go-ask-alice',
    component: alice,
    id: `1`,
    context: {
      element: footerData,
    }
  })

  /* create collaboration page */
  createPage({
    path: '/contact-alice/collaboration',
    component: collaboration,
    id: `1`,
    context: {
      element: footerData,
    }
  });

  /* create Media inquiries page */
  createPage({
    path: '/contact-alice/press-request',
    component: press,
    id: `1`,
    context: {
      element: footerData,
    }
  });

  /* create comments and correction page */
  createPage({
    path: '/contact-alice/comments-corrections',
    component: comments,
    id: `1`,
    context: {
      element: footerData,
    }
  });

  /* create licensing page */
  createPage({
    path: '/contact-alice/licensing',
    component: licensing,
    id: `1`,
    context: {
      element: footerData,
    }
  });

  /* create feed request page */
  createPage({
    path: '/contact-alice/feed-request',
    component: feed,
    id: `1`,
    context: {
      element: footerData,
    }
  });

  /* create syndication page */
  createPage({
    path: '/contact-alice/syndication',
    component: syndication,
    id: `1`,
    context: {
      element: footerData,
    }
  });

  /* create go ask alice history page */
  createPage({
    path: '/about-alice/go-ask-alice-history',
    component: history,
    id: `1`,
    context: {
      element: historydata,
    }
  });

  /* create Contact alice page */
  createPage({
    path: '/contact-alice/get-alice-in-your-inbox',
    component: getAliceInBoxPage,
    id: `1`,
    context: {
      element: footerData,
    }
  })

  /* create email page for footer pages */
  footerData.map((item, i) => {
    if (item.fieldFormtype == "all_about") {
      data = item
      // let printPath = data !== undefined && data.nid ? "/print/" + data.nid + "" : ''
      let printmailPath = data !== undefined && data.nid ? "/printmail/" + data.nid + "" : ''
      // createPage({
      //   path: printPath,
      //   component: printquePage,
      //   id: `1`,
      //   context: {
      //     element: data,
      //   }
      // });

      createPage({
        path: printmailPath,
        component: printmailPage,
        id: `1`,
        context: {
          element: data,
          title: "All About Alice!",
          paths: "/about-alice/all-about"
        }
      });

    }
  })

  /* create all about alice page */
  createPage({
    path: '/about-alice/all-about',
    component: allAboutAlicePage,
    id: `1`,
    context: {
      element: footerData,
      testimonial: testimonialData
    }
  })

  /* create email page for emergency */
  if (emergencyData.length > 0) {
    // let printpath = '/print/' + emergencyData[0].nid + ''
    let printmailPath = '/printmail/' + emergencyData[0].nid + ''

    // createPage({
    //   path: printpath,
    //   component: printquePage,
    //   // id: health.nid,
    //   context: {
    //     element: emergencyData.length > 0 ? emergencyData[0] : [],
    //   }
    // })
    createPage({
      path: printmailPath,
      component: printmailPage,
      id: `1`,
      context: {
        element: emergencyData.length > 0 ? emergencyData[0] : [],
        title: "In an Emergency",
        paths: "/find-help/emergency"
      }
    });
  }

  /* create emergency page */
  createPage({
    path: '/find-help/emergency',
    component: emergencyPage,
    id: `1`,
    context: {
      element: emergencyData,
    }
  })

  /* create page Ask your question */
  createPage({
    path: '/node/add/question',
    component: question,
    id: `1`,
    context: {
      element: listcategorydata,
    }
  });

  /* create faqs page */
  createPage({
    path: '/about-alice/faqs',
    component: faqs,
    id: `1`,
    context: {
      element: faqsdata,
    }
  });

  /* create email page for on campus */
  if (oncampusresourcedata.length > 0) {
    // let printpath = '/print/' + oncampusresourcedata[0].nid + ''
    let printmailPath = '/printmail/' + oncampusresourcedata[0].nid + ''

    // createPage({
    //   path: printpath,
    //   component: printquePage,
    //   // id: health.nid,
    //   context: {
    //     element: oncampusresourcedata.length > 0 ? oncampusresourcedata[0] : [],
    //   }
    // })
    createPage({
      path: printmailPath,
      component: printmailPage,
      id: `1`,
      context: {
        element: oncampusresourcedata.length > 0 ? oncampusresourcedata[0] : [],
        title: 'On-campus Resources',
        paths: '/find-help/campus-resources'
      }
    });
  }

  /* create on campus page */
  createPage({
    path: '/find-help/campus-resources',
    component: oncampusresource,
    id: `1`,
    context: {
      element: oncampusresourcedata
    }
  })

  /* create home page */
  createPage({
    path: '/',
    component: homepage,
    id: `1`,
    context: {
      newqa: newQABlockdata,
      theme: themeBlockdata,
      lastupdated: lastupdateddata,
      quizz: quizzBlockData,
      testimonial: testimonialData,
      poll: pollHomeData,
      allaboutalice: allaboutaliceData,
      homepagehealthanswer: homepagehealthanswerData
    }
  })

  /* create theme page */
  createPage({
    path: '/themes',
    component: theme,
    id: `1`,
    context: {
      element: themedata
    }
  })

  // themedata.map((theme,i)=>{
  //   theme.queryFieldHealthAnswer.entities.map((entity,j)=>{
  //     let ids =i+j
  //     let themePath = entity.entityUrl.path
  //     createPage({
  //       path: themePath,
  //       component:themeweekDetailTemplate,
  //       id:ids,
  //       context: {
  //         element: entity,
  //       }
  //     });
  //   })
  // })

  /* create recent Q&A page */
  createPage({
    path: '/answered-questions/recent/',
    component: newQA,
    id: `1`,
    context: {
      newQA: newqadata,
      updatedQA: updatedQAdata
    }
  })

  /* create fact sheet page */
  createPage({
    path: '/fact-sheets',
    component: factsheets,
    id: `1`,
    context: {
      element: factdata
    }
  })

  /* create 25 years of questional behaviour page */
  createPage({
    path: '/basic-page/25-years-questionable-behavior',
    component: yearsQuestionableBehaviorpage,
    id: `1`,
    context: {
      element: yearsQuestionableBehaviorData
    }
  })

  /* create email page for fact sheet */
  factdata.map((fact, i) => {
    let factPath = fact.entityUrl.path
    // let printPath = `/print/${fact.nid}`
    let printmailPath = `/printmail/${fact.nid}`
    createPage({
      path: factPath,
      component: factsheet_template,
      id: fact.nid,
      context: {
        element: fact,
      }
    });

    // createPage({
    //   path: printPath,
    //   component: printquePage,
    //   id: `1`,
    //   context: {
    //     element: fact,
    //   }
    // });

    createPage({
      path: printmailPath,
      component: printmailPage,
      id: `1`,
      context: {
        element: fact,
        title: fact.title,
        paths: factPath
      }
    });

  })

  /* Create mail page for quizz */
  if (morequizzData.length > 0) {
    // let printPath = `/print/${morequizzData[0].nid}`
    let printmailPath = `/printmail/${morequizzData[0].nid}`
    // createPage({
    //   path: printPath,
    //   component: printquePage,
    //   id: `1`,
    //   context: {
    //     element: morequizzData[0],

    //   }
    // });

    createPage({
      path: printmailPath,
      component: printmailPage,
      id: `1`,
      context: {
        element: morequizzData[0],
        title: morequizzData[0].title,
        paths: '/quizzes'
      }
    });
  }

  /* create quizz page */
  createPage({
    path: '/quizzes',
    component: quizzTemplate,
    id: `1`,
    context: {
      element: morequizzData,
      quizz: quizzdata
    }
  });

  /* create health answer page */
  createPage({
    path: '/health-answers/tag',
    component: health_template,
    id: `1`,
    context: {
      element: categorydata,
    }
  });

  // quizzdata.map((quizz,i)=>{
  //   let quizzPath = quizz.entityUrl.path
  //   createPage({
  //     path: quizzPath,
  //     component:quizz_template,
  //     id: quizz.nid,
  //     context: {
  //       element: quizz,
  //       quizz:quizzdata
  //     }
  //   });
  // })
  // createPage({
  //   path:'/quizzes',
  //   component:quizz,
  //   id: `1`,
  //   context: {
  //     element: quizzdata,
  //   }
  // });



}




