const on_campus_resources = [
  {
    entityLabel: "red University general health Resource",
      phone_title: "Phone",
      fieldPhone: "(555) 555-2222",
      website_title: "Website",
      fieldWebsite:{
        title:"General service Resource new",
        uri:"#"
      },
      body:{
        value:"Replace this text with a description of this service."
      }
  },
  {
    entityLabel: "Blue University general health Resource",
      phone_title: "Phone",
      fieldPhone: "(444) 444-3333",
      website_title: "Website",
      fieldWebsite:{
        title:"General service Resource new",
        uri:"#"
      },
      body:{
        value:"Replace this text with a description of this service."
      }
  },
  {
    entityLabel: "yellow University general health Resource",
      phone_title: "Phone",
      fieldPhone: "(444) 444-3333",
      website_title: "Website",
      fieldWebsite:{
        title:"General service Resource new",
        uri:"#"
      },
      body:{
        value:"Replace this text with a description of this service."
      }
  }
]

export { on_campus_resources }
