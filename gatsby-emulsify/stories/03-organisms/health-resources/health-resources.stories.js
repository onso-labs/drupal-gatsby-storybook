import React from 'react';
import { on_campus_resources } from './health-resources';
import { Card } from "../../02-molecules/card/card.stories";

export const HealthResources = ({ cardCampusResources = on_campus_resources }) => (
  <div>
    {cardCampusResources.map((item) => {
      if (item !== null) {
        return (
          <Card card={item.queryFieldResource ? item.queryFieldResource.entities[0] : item} cardDetail={item.queryFieldResource ? item.queryFieldResource.entities[0].body : item.body} />
        )
      }


    })}
  </div>
);

export default {
  title: 'Organisms/HealthResources',
  component: HealthResources,
};


