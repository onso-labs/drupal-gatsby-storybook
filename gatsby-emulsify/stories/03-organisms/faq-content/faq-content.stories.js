import { Accordion } from '../../02-molecules/accordion/accordion.stories';
import React from 'react';
import { faqContent } from './faq-content.js';

export const FAQsContent = ({ faqs = faqContent, activeKey = activeKey, onKeyChange }) => (
  <div >
    {faqs.map((item, i) => {
    
      return (
        <Accordion  accordions={item} key={i} panelkey={i + 1} activeKey={activeKey} onKeyChange={onKeyChange} />
      )
    })}
  </div>
);


export default {
  title: 'Organisms/FAQsContent',
  component: FAQsContent,
};

