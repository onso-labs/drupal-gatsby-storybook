import React from 'react';
import { formData } from './custom-form';
import { Successbtn } from '../../01-atoms/buttons/button.stories';
import config from '../../../src/config';
import ReCAPTCHA from "react-google-recaptcha";

// import { Links } from '../../01-atoms/link/link.stories';
export const CustomForm = ({ form = formData, name = '', email = '', message = '', verifyCallback, handlechange, handleSubmit, type = '', organization = '', phone = '', site = '', students = '', license = '', btnName, error = false ,reCaptchaError=false}) => (
  <form class="custom-form">
    {type !== "comment" && (
      <div class="form-item">
        <label for="edit-name" class="form-item__label form-item__label--required form-item__label--textfield">Name <span class="form-required">* <span class="form-required-text">{form.requireMsg}</span></span></label>
        <input autocorrect="none" autocapitalize="none" spellcheck="false" aria-describedby="edit-name--description" type="text" name="name" value={name} size="60" maxlength="60" class="form-item__textfield" className={error ? "invalidClass" : ""} aria-required="true" onChange={(e) => handlechange(e, "name")} />
      </div>
    )}
    {(type == "licensing" || type == "feed" || type == "syndication") && (
      <div class="form-item">
        <label for="organization" class="form-item__label form-item__label--textfield">Organization/Institution <span class="form-required">* <span class="form-required-text" >{form.requireMsg}</span></span></label>
        <input id="organization" value={organization} type="text" class="form-item__textfield" onChange={(e) => handlechange(e, "organization")} className={error ? "invalidClass" : ""} />
      </div>
    )}
    <div class="form-item">
      <label for="emailaddress" class="form-item__label form-item__label--textfield">Email Address <span class="form-required">* <span class="form-required-text" >{form.requireMsg}</span></span></label>
      <input id="emailaddress" value={email} type="email" class="form-item__textfield" onChange={(e) => handlechange(e, "email")} className={error ? "invalidClass" : ""} />
    </div>

    {(type !== "collaboration" && type !== "comment") && (
      <div class="form-item">
        <label for="phone" class="form-item__label form-item__label--textfield">Phone Number <span class="form-required">* <span class="form-required-text">{form.requireMsg}</span></span></label>
        <input id="phone" value={phone} type="text" class="form-item__textfield" onChange={(e) => handlechange(e, "phone")} className={error ? "invalidClass" : ""} />
      </div>
    )}

    {type == "feed" && (
      <div class="form-item">
        <label for="site" class="form-item__label form-item__label--textfield">Site on which Go Ask Alice! box would appear <span class="form-required">* <span class="form-required-text" >{form.requireMsg}</span></span></label>
        <input id="site" value={site} type="text" class="form-item__textfield" onChange={(e) => handlechange(e, "site")} className={error ? "invalidClass" : ""} />
      </div>
    )}

    {type == "syndication" && (
      <div class="form-item">
        <label for="students" class="form-item__label form-item__label--textfield">Total student enrollment <span class="form-required">* <span class="form-required-text" >{form.requireMsg}</span></span></label>
        <input id="students" value={students} type="text" class="form-item__textfield" onChange={(e) => handlechange(e, "students")} className={error ? "invalidClass" : ""} />
      </div>
    )}

    <div class="form-item">
      <label for="textarea" class="form-item__label form-item__label--textfield">Message <span class="form-required">
        {(type !== "licensing") && (type !== "feed") && (type !== "syndication") && (<span class="form-required-text">*{form.requireMsg}</span>)}</span></label>
      <textarea id="textarea" class="form-item__textfield form-item__textarea" rows="8" cols="48" value={message} onChange={(e) => handlechange(e, "message")} className={(error && type !== "licensing" && type !== "feed" && type !== "syndication") ? "invalidClass" : ""}></textarea>
    </div>
    {type == "licensing" && (
      <div class="form-item">
        <label for="license" class="form-item__label form-item__label--textfield">Content you wish to license? <span class="form-required">* <span class="form-required-text">{form.requireMsg}</span></span></label>
        <textarea id="license" class="form-item__textfield form-item__textarea" rows="8" cols="48" value={license} onChange={(e) => handlechange(e, "license")} className={error ? "invalidClass" : ""}></textarea>
      </div>
    )}

    <div class="form-captcha-content">
      <fieldset className={`form-fieldset ${reCaptchaError ? 'invalidClass' : ''}`}>
        <legend>CAPTCHA</legend>
        {form.paragraph_content}
        <ReCAPTCHA
          sitekey={config.ReCAPTCHA_Key}
          onChange={verifyCallback}
        />
      </fieldset>
    </div>
    <div class="custom-form__btn">
      <Successbtn buttonTextValue={btnName} value={handleSubmit} />
    </div>
  </form>
);

export default {
  title: 'Organisms/CustomForm',
  component: CustomForm,
};


