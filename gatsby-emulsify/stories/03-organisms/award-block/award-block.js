import placeholder from '../../../images/icons/badge-placeholder.png';
import healthline from '../../../images/icons/Healthline std_BADGE 2017.png';
import winner from '../../../images/icons/wha_winner_09_bronze.png';
import medallion from '../../../images/icons/NASPA_medallion.jpg';
import exawards from '../../../images/icons/NASPA_exawards_logo2__medium.jpg';

const awardBlock = [
    {
        awardimgsrc: healthline,
        awardimgalt: 'Healthline std_BADGE 2017',
        awarddetail: 'Best of the Web/STD Blogs, Healthline, 2012, 2013, 2014, 2017'
    },
    {
        awardimgsrc: winner,
        awardimgalt: 'Web Health Award, Bronze Level, 2009',
        awarddetail: 'Web Health Award, Bronze Level, 2009'
    },
    {

        awardimgsrc: placeholder,
        awardimgalt: 'Best Practices in College Health Award, American College Health Association, 2007',
        awarddetail: 'Best Practices in College Health Award, American College Health Association, 2007'
    },
    {

        awardimgsrc: medallion,
        awardimgalt: 'Gold Excellence Award for Health, Wellness, & Counseling, NASPA, 2007',
        awarddetail: 'Gold Excellence Award for Health, Wellness, & Counseling, NASPA, 2007'
    },
    {
        awardimgsrc: exawards,
        awardimgalt: 'Grand Gold Award, NASPA, 2007',
        awarddetail: 'Grand Gold Award, NASPA, 2007'
    },
    {
        awardimgsrc: placeholder,
        awardimgalt: 'HEDIR Technology Award, 1998',
        awarddetail: 'HEDIR Technology Award, 1998'
    }

]
const paragraphContent = {
    paragraph: "Go Ask Alice! accepts these awards with great pride and appreciation. We thank our colleagues for the recognition and look forward to continuing to provide excellent health information and education online.",
    title: 'Awards'
}

export { awardBlock, paragraphContent }