import React from 'react';
import { Paragraph } from "../../01-atoms/text/paragraph/paragraph.stories";
import { awardBlock, paragraphContent } from "./award-block";
import { AwardItem } from '../../02-molecules/award-item/award-item.stories';
export const AwardBlock = ({ awardBlockData = awardBlock, paragraphData = paragraphContent }) => (
    <div class="award-block">
        <div class="award-block__header">
            <h3 class="award-block__title">
                {paragraphData.title}
            </h3>
            <Paragraph paragraphContent={paragraphData.paragraph} />
        </div>
        <ul class="award-block__content">
            {awardBlockData.map((item, i) => {
                return (
                    <AwardItem awardItemData={item} />
                )
            })}
        </ul>
    </div>
);

export default {
    title: 'Organisms/AwardBlock',
    component: AwardBlock,
};