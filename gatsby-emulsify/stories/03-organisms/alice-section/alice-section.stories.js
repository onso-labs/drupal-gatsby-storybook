import React from 'react';
import { statsTitle, statsBody } from './alice-section';
export const AboutAlice = ({ statsTitle = statsTitle, statsBody = statsBody }) => (
    <section class="alice-section">
        <div class="alice-header">
            <a class="link-orange link-h2" href="/about-alice/all-about">
                {statsTitle}</a></div>
        <div class="about-alice" dangerouslySetInnerHTML={{ __html: statsBody !== undefined ?statsBody  : '' }} />
        <div class="more-details"><a class="link-orange" href="/about-alice/all-about">Read More</a></div>
    </section>
);

export default {
    title: 'Organisms/AboutAlice',
    component: AboutAlice,
};


