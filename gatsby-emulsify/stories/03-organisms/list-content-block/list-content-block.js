const fields = [
    {

        title: "Testing for sexually transmitted infections (STIs) (Question)",
        body: { summary: "Dear Alice, Is there any test for STD? Dear Reader, The very short answer ... or STIs). There are multiple ways health care providers test for STIs and most are quick and painless. Some STIs can be accurately ... Why is that a wise step? Providers may not automatically test for STIs during routine screenings and they might not test for every ..." },
        entityUrl: { path: '/' }
    },
    {
        title: "How effective is at-home HIV testing kit? (Question)",
        body: { summary: "... I'm starting to panic. So, I thought about doing an at-home test because I'm too scared to go to the clinic. I found an at home test that says that you can get at home results in 15 minutes. It's called ..." },
        entityUrl: { path: '/' }
    },

]

const title = {
    section_title: "Fact sheets",
}

export { fields }
export { title }
