import React from 'react';
import { fields } from './list-content-block.js';
import { Paragraph } from '../../01-atoms/text/paragraph/paragraph.stories.js';
import { Links } from '../../01-atoms/link/link.stories.js';

export const ListContentBlock = ({ searchlist = fields }) => (

  <ul className="content-list">
    {searchlist.map((item, i) => {
      let body
      if (item.queryFieldQuizDetails && item.queryFieldQuizDetails.entities[0].body !== null) {
        body = item.queryFieldQuizDetails.entities[0].body.value
      } else {
        body = item.field_question
      }

      return (
        <li className="content-list-item">
          <h3><Links linkTextValue={item.title} linkRedirectTo={item.entityUrl ? item.entityUrl.path : item.field_search_result_link_target} /></h3>
          <Paragraph paragraphContent={body} />
        </li>
      )
    })}
  </ul>
);

export default {
  title: 'Organisms/ListContentBlock',
  component: ListContentBlock,
};



