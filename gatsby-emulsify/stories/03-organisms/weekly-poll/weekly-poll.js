
const weeklyPoll = {
    weeklypollReadmore: 'More polls',
    weeklypollReadmoreUrl: '#',
    weeklypollHeaderTitle: 'Its almost time for finals! What is your essential study tool?',
    weeklypollHeadertitleUrl: '#',
    weeklypollTitle: 'Weekly Poll',
    buttonContent: 'vote'
}
const radios = [
    {
        name: "radio",
        value: "Oak",
        label: "Oak",
        checked: false
    },
    {
        name: "radio",
        value: "Maple",
        label: "Maple",
        checked: false
    },
    {
        name: "radio",
        value: "Pine",
        label: "Pine",
        checked: false
    },
    {
        name: "radio",
        value: "Apple",
        label: "Apple",
        checked: false
    },
    {
        name: "radio",
        value: "Other",
        label: "Other",
        checked: false
    },
]

export { weeklyPoll, radios }

