import React from 'react';
import { weeklyPoll, radios } from "./weekly-poll";
import { Primarybtn } from '../../01-atoms/buttons/button.stories';
import { Links } from '../../01-atoms/link/link.stories';
import { Radio } from '../../01-atoms/forms/radio/radio.stories';

export const WeeklyPoll = ({ weeklyPolls = weeklyPoll, radiosData = radios, checked, show = false, detail = false, onchange, pid = 0, onVoteClick }) => (

	<div className="weeklypoll">
		{/* {(weeklyPolls.weeklypollTitle !== "" && weeklyPolls.weeklypollTitle !== null) && ( */}
		{show == true && (
			<div className="block-title">
				<h2>
					Weekly Poll
				</h2>
			</div>
		)}

		{/* )} */}
		<div className="weeklypoll__content">
			{detail == false && (
				<div className="weeklypoll__header">
					<Links linkTextValue={weeklyPolls.entityLabel ? weeklyPolls.entityLabel !== '' ? weeklyPolls.entityLabel !== null ? weeklyPolls.entityLabel : '' : '' : ''} linkRedirectTo={weeklyPolls.weeklypollHeadertitleUrl} />
				</div>
			)}

			<div className="weeklypoll__body">
				<form className="weeklypoll__form-groups">
					<ul className="form-item--radios">
						{radiosData.map((item, i) => {
							return (
								<li className="form-item--radio__item">
									<Radio type="radio" checked={weeklyPolls.cid == item.targetId} id={item.targetId} name={item.entity ? item.entity.entityLabel : ''} label={item.entity ? item.entity.entityLabel : ''} value={item.targetId}
										onchange={(e) => { onchange(pid, item.targetId, e) }}
										data={true} />
								</li>
							)
						})}
					</ul>
					<div className="weeklypoll__btn-wrapper">
						<Primarybtn buttonTextValue="VOTE" value={(e) => { onVoteClick(pid, e) }} />
					</div>
				</form>
			</div>
		</div>
		{/* {(weeklyPolls.weeklypollReadmore !== "" && weeklyPolls.weeklypollReadmore !== null) && ( */}

		{/* )} */}

	</div>
);

export default {
	title: 'Organisms/WeeklyPoll',
	component: WeeklyPoll,
}
