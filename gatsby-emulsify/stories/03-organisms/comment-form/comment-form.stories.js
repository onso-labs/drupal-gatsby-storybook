import React from 'react';
import { formData } from './comment-form';
import { Successbtn } from '../../01-atoms/buttons/button.stories';
import config from '../../../src/config';
import ReCAPTCHA from "react-google-recaptcha";

// import { Links } from '../../01-atoms/link/link.stories';
export const CommentForm = ({ form = formData, message = '', verifyCallback, handlechange, handleSubmit, btnName, error = false, reCaptchaerror = false }) => (
  <form class="custom-form">
    <h2 class="block__title">Submit a new response</h2>

    <div class="form-item">
      <label for="textarea" class="form-item__label form-item__label--textfield">Comments <span class="form-required">
        <span class="form-required-text">*{form.requireMsg}</span></span></label>
      <textarea id="textarea" class="form-item__textfield form-item__textarea" rows="5" cols="60" value={message} onChange={(e) => handlechange(e, "message")} className={(error) ? "invalidClass" : ""}></textarea>
    </div>
    <div class="form-item">
      <div class="form-captcha-content">
      <fieldset className={`form-fieldset ${reCaptchaerror ? 'invalidClass' : ''}`} >
          <legend>CAPTCHA</legend>
          {form.paragraph_content}
          <ReCAPTCHA
            sitekey={config.ReCAPTCHA_Key}
            onChange={verifyCallback}
          />
        </fieldset>
      </div>
      <div class="messages messages--error " tabindex="0" className={(reCaptchaerror) ? "" : "visually-hidden"}>
        <div class="messages__icon invalidClass">The answer you entered for the CAPTCHA was not correct.</div></div>
    </div>
    <div class="custom-form__btn">
      <Successbtn buttonTextValue={btnName} value={handleSubmit} />
    </div>
  </form>
);

export default {
  title: 'Organisms/CommentForm',
  component: CommentForm,
};


