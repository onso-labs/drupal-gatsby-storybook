import React from 'react';
import { Links } from "../../01-atoms/link/link.stories";
import { quizzList, pollDetail } from "./more-polls";
import { Successbtn } from '../../01-atoms/buttons/button.stories';

export const MorePolls = ({ quizzes = quizzList, polls = pollDetail }) => (
    <div class="quizz-list__wrapper">
        <h2 class="h2">{polls.header_polls}</h2>
        {quizzes.map((item) => {
            return (
                <div class="poll-list">
                    <Links linkTextValue={item.entityLabel ? item.entityLabel : ''} linkRedirectTo={item.entityUrl && item.entityUrl !== null && item.entityUrl.path ? item.entityUrl.path : ''} />
                </div>
            )
        })}
        {/* <div class="load-more-btn">
            <Successbtn buttonTextValue={polls.button_content} />
        </div> */}
    </div>
);

export default {
    title: 'Organisms/MorePolls',
    component: MorePolls,
};
