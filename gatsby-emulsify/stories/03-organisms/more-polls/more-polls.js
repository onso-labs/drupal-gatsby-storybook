
const pollDetail = {
    header_polls: 'More Polls',
    button_content: 'Load More'

}
const quizzList = [
    {
        link_content: 'COVID-19 is affecting people all over the world. Where have you been getting your COVID-19 info?',
        link_url: "#"
    },
    {
        link_content: "What's your go-to type of snack?",
        link_url: "#"
    },
    {
        link_content: 'What is your favorite winter sport?',
        link_url: "#"
    }
]

export { quizzList, pollDetail }

