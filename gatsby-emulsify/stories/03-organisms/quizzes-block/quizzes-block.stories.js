import React from 'react';
import { quizzesData, list } from "./quizzes-block";
import { Links } from '../../01-atoms/link/link.stories';
import { Paragraph } from '../../01-atoms/text/paragraph/paragraph.stories';
import { QuestionBlock } from '../../02-molecules/question-block/question-block.stories';
import { QuizzesAnswerBlock } from '../../02-molecules/quizzes-answer-block/quizzes-answer-block.stories';
import { SlideDown } from 'react-slidedown';
import 'react-slidedown/lib/slidedown.css';
// import { ListContentBlock } from '../../03-organisms/list-content-block/list-content-block.stories';
export const QuizzesBlock = ({ quizzesDatas = quizzesData, listData = list, more }) => (

	<div class="quizzes-collection">
		<div className="quick-quizzes__list">

			<div className="quizzes-link">
				{(quizzesDatas.title !== undefined && quizzesDatas.title !== "" && quizzesDatas.title !== null) && (
					<h3><Links linkTextValue={quizzesDatas.title} linkRedirectTo={quizzesDatas.entityUrl.path} /></h3>)}
				<Paragraph paragraphContent={quizzesDatas?.body?.value} />
			</div>

		</div>
		{listData.map((list, i) => (
			<div className="quiz-wrapper">
				<QuestionBlock themeQuestions={list} />
				<QuizzesAnswerBlock quizzesAnswers={list} loadMore={more} />
				{(list.show !== undefined && list.show == true) && (
					<div className="quiz-expert-answer">
						<SlideDown className={'my-dropdown-slidedown'}>
							<QuestionBlock themeQuestions={list} type="message" />
						</SlideDown>
					</div>
				)}

			</div>
		))}
	</div>
);

export default {
	title: 'Organisms/QuizzesBlock',
	component: QuizzesBlock,
};