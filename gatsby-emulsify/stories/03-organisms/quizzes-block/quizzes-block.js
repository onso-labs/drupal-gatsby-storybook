
const quizzesData = {
    title: "Know all about the benefits of exercise? Flex your knowledge!",
    entityUrl:{
        path: "#"
    },
    body:{
    value: "<p>Put your reps, sets, and laps knowledge to the test with this quiz.</p>"}
    // "nid": 1080818,
    // "title": "Know all about the benefits of exercise? Flex your knowledge!",
    // "body": {
    //     "value": "<p>Put your reps, sets, and laps knowledge to the test with this quiz.</p>\r\n",
    //     "format": "basic_html",
    //     "processed": "<p>Put your reps, sets, and laps knowledge to the test with this quiz.</p>\n"
    // },
    // "entityUrl": {
    //     "path": "/quizzes/know-all-about-benefits-exercise-flex-your-knowledge"
    // },

}

const list=[
    {
        message:" Unfortunately, any of these can hinder your ability to eat a healthy and nutritious diet.",
        options:["Lack of healthy options at home, school, or work.","Hefty price tags on healthy foods.","Not enough time to prepare healthy meals.","Eating out for most meals where there aren't a lot of healthy options.","All of the above."],
        question:"Do any of these look familiar?",
        type:"MS",
        value:"Okay, first course: What are some common obstacles to healthy eating?"
    },
    {
        message:" Unfortunately, any of these can hinder your ability to eat a healthy and nutritious diet.",
        options:["Lack of healthy options at home, school, or work.","Hefty price tags on healthy foods.","Not enough time to prepare healthy meals.","Eating out for most meals where there aren't a lot of healthy options.","All of the above."],
        question:"Do any of these look familiar?",
        type:"MS",
        value:"Okay, first course: What are some common obstacles to healthy eating?"
    },
    {
        message:" Unfortunately, any of these can hinder your ability to eat a healthy and nutritious diet.",
        options:["Lack of healthy options at home, school, or work.","Hefty price tags on healthy foods.","Not enough time to prepare healthy meals.","Eating out for most meals where there aren't a lot of healthy options.","All of the above."],
        question:"Do any of these look familiar?",
        type:"MS",
        value:"Okay, first course: What are some common obstacles to healthy eating?"
    }

]

export { quizzesData,list }

