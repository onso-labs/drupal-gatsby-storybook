
const affiliation = {
  buttonContent: 'Submit',
  requireMsg: ' * This field is required.'
}
const radios = [
  {
    name: "radio",
    value: "Columbia University",
    label: "Columbia University",
    checked: false
  },
  {
    name: "radio",
    value: "Another College/University",
    label: "Another College/University",
    checked: false
  },
  {
    name: "radio",
    value: "Other",
    label: "Other",
    checked: false
  },

]

export { affiliation, radios }

