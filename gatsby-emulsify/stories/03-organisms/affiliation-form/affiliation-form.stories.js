import React from 'react';
import { radios, affiliation } from './affiliation-form';
import { Primarybtn } from '../../01-atoms/buttons/button.stories';
import { Radio } from '../../01-atoms/forms/radio/radio.stories';

export const AffiliationForm = ({ radiosData = radios, checked, affiliationData = affiliation, email, radio, handleSubmit, btnName, error, change }) => (
  <form className="subscribe-form">
    <div className="form-item subscribe-form-input">
      <label for="emailaddress" className="form-item__label form-item__label--textfield">Email Address<span className="form-required">{affiliationData.requireMsg}</span></label>
      <input id="emailaddress" type="email" className="form-item__textfield" value={email} onChange={(e) => change(e, "email")} />
    </div>

    <ul className="form-item form-item--radio">
      <label for="emailaddress" className="form-item__label form-item__label--textfield">Affiliation</label>
      {radiosData.map((item, i) => {
        return (
          <li className="form-item--radio__item form-item">
            <Radio className="form-radio" type="radio" checked={checked} id={item.label} name={item.name} label={item.label} value={item.label} onchange={(e) => { change(e, "affiliation") }} data={true} />
          </li>
        )
      })}
    </ul>
    <div className="subscribe-form__btn">
      <Primarybtn buttonTextValue={btnName} value={handleSubmit} />
    </div>
  </form>
);

export default {
  title: 'Organisms/AffiliationForm',
  component: AffiliationForm,
};


