import React from 'react';
import { theme_week_content, activeKey } from './theme-week-landing-content';
import { ThemeWeekCollection } from "../../02-molecules/theme-week-collection/theme-week-collection.stories";

export const ThemeWeekLandingContent = ({ themeWeekContent = theme_week_content, activeKey = activeKey, onKeyChange }) => (
  <div>
    {themeWeekContent.map((item, index) => {
      return (
        <ThemeWeekCollection themeWeeks={item} themeAnswersList={item.queryFieldHealthAnswer ?.entities}
          panelkey={index + 1} activeKey={activeKey} onKeyChange={onKeyChange} />
      )
    })}
  </div>
);

export default {
  title: 'Organisms/ThemeWeekLandingContent',
  component: ThemeWeekLandingContent,
};


