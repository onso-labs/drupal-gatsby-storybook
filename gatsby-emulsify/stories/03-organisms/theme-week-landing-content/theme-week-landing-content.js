const theme_week_content = [
  {
    "entityType": "block_content",
    "entityBundle": "theme_of_the_week",
    "entityLabel": "Bringing Sex (Ed) Back",
    "fieldWeek": {
        "value": "2020-06-08",
        "date": "2020-06-08 12:00:00 UTC"
    },
    "queryFieldHealthAnswer": {
        "entities": [
            {
                "title": "Will sex-ercise shed those extra pounds?",
                "entityUrl": {
                    "routed": true,
                    "path": "/health-answers/will-sex-ercise-shed-those-extra-pounds"
                },
                "fieldQuestion": {
                    "value": "<p>Dear Alice,</p>\r\n\r\n<p>You know how you often see that, for example, a 30-minute jog burns such and such number of calories. Well, I was wondering if there are any figures for sex. Thanks.</p>\r\n\r\n<p>—Curious</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Dear Alice,</p>\n\n<p>You know how you often see that, for example, a 30-minute jog burns such and such number of calories. Well, I was wondering if there are any figures for sex. Thanks.</p>\n\n<p>—Curious</p>"
                }
            },
            {
                "title": "Lost all control in the bedroom",
                "entityUrl": {
                    "routed": true,
                    "path": "/health-answers/lost-all-control-bedroom"
                },
                "fieldQuestion": {
                    "value": "<p>Dear Alice,</p>\r\n\r\n<p>I have been with my long term girlfriend for five years. I am seriously thinking about getting engaged.</p>\r\n\r\n<p>Over the last three years she has taken control of our sex life. I have definitely become submissive in the bedroom. We don’t get into BDSM, but she always decides when, where, and how. She loves this control. While I have come to accept my submissive role, and I do certainly enjoy making love to her and pleasing her, I have asked her on many occasions for some variety. I would like to enter her sometimes and she has not performed oral sex on me for years. Granted, I am a bit under average in the endowment area and she always has a hard time reaching orgasm with straight intercourse. I have tried to talk to her, but she always shuts down the convo by making me admit that I love pleasuring her. Of course when we start fooling around I am so excited that I just follow her lead.</p>\r\n\r\n<p>Honestly, I am very happy in every other aspect and I don't want to add any friction to the relationship. She is a very private person and doesn’t like to talk sex. Visiting a therapist is not an option as I have already subtly suggested it and been rebuffed. Should I be overly concerned? Should I continue to press the issue (although it doesn’t seem to be getting me anywhere) or just accept my role? Again, I do enjoy pleasing her and I usually get off. I guess I could be happy continuing this way and I don't want to lose her. I would love your thoughts.</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Dear Alice,</p>\n\n<p>I have been with my long term girlfriend for five years. I am seriously thinking about getting engaged.</p>\n\n<p>Over the last three years she has taken control of our sex life. I have definitely become submissive in the bedroom. We don’t get into BDSM, but she always decides when, where, and how. She loves this control. While I have come to accept my submissive role, and I do certainly enjoy making love to her and pleasing her, I have asked her on many occasions for some variety. I would like to enter her sometimes and she has not performed oral sex on me for years. Granted, I am a bit under average in the endowment area and she always has a hard time reaching orgasm with straight intercourse. I have tried to talk to her, but she always shuts down the convo by making me admit that I love pleasuring her. Of course when we start fooling around I am so excited that I just follow her lead.</p>\n\n<p>Honestly, I am very happy in every other aspect and I don't want to add any friction to the relationship. She is a very private person and doesn’t like to talk sex. Visiting a therapist is not an option as I have already subtly suggested it and been rebuffed. Should I be overly concerned? Should I continue to press the issue (although it doesn’t seem to be getting me anywhere) or just accept my role? Again, I do enjoy pleasing her and I usually get off. I guess I could be happy continuing this way and I don't want to lose her. I would love your thoughts.</p>"
                }
            },
            {
                "title": "What's fisting?",
                "entityUrl": {
                    "routed": true,
                    "path": "/health-answers/whats-fisting"
                },
                "fieldQuestion": {
                    "value": "<p>Dear Alice,</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>I have a question. My friends often make jokes about something called \"fisting.\" I feel really left out when they joke about it, so could you please tell me what fisting is?! Thanks a lot.</p>\r\n\r\n<p>Just wondering</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Dear Alice,</p>\n\n<p> </p>\n\n<p>I have a question. My friends often make jokes about something called \"fisting.\" I feel really left out when they joke about it, so could you please tell me what fisting is?! Thanks a lot.</p>\n\n<p>Just wondering</p>"
                }
            },
            {
                "title": "Semen goes where?",
                "entityUrl": {
                    "routed": true,
                    "path": "/health-answers/semen-goes-where"
                },
                "fieldQuestion": {
                    "value": "<p>Dear Alice,</p>\r\n\r\n<p>I was just wondering what happens to semen after it has been ejaculated into a woman's vagina. Does it just stay there until it dies, or does it seep out? If it stays in, what happens to it once it dies?</p>\r\n\r\n<p>Thanks,<br />\r\nPrude and Wondering</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Dear Alice,</p>\n\n<p>I was just wondering what happens to semen after it has been ejaculated into a woman's vagina. Does it just stay there until it dies, or does it seep out? If it stays in, what happens to it once it dies?</p>\n\n<p>Thanks,\nPrude and Wondering</p>"
                }
            },
            {
                "title": "C'mon, tell me about chlamydia",
                "entityUrl": {
                    "routed": true,
                    "path": "/health-answers/cmon-tell-me-about-chlamydia"
                },
                "fieldQuestion": {
                    "value": "<p>(1) Dear Alice,</p>\r\n\r\n<p>What exactly is chlamydia? How is it transmitted? Can you get it through oral sex (man on woman)? How is it treated (especially in the case of males)?</p>\r\n\r\n<p>Help!</p>\r\n\r\n<p>— Need to know asap!!!</p>\r\n\r\n<p>(2) Dear Alice,</p>\r\n\r\n<p>Could you tell me about the symptoms of chlamydia and if one test is enough to detect that disease? Thank you.</p>\r\n\r\n<p>— Curious</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>(1) Dear Alice,</p>\n\n<p>What exactly is chlamydia? How is it transmitted? Can you get it through oral sex (man on woman)? How is it treated (especially in the case of males)?</p>\n\n<p>Help!</p>\n\n<p>— Need to know asap!!!</p>\n\n<p>(2) Dear Alice,</p>\n\n<p>Could you tell me about the symptoms of chlamydia and if one test is enough to detect that disease? Thank you.</p>\n\n<p>— Curious</p>"
                }
            }
        ]
    }
},
{
    "entityType": "block_content",
    "entityBundle": "theme_of_the_week",
    "entityLabel": "Love on the Brain",
    "fieldWeek": {
        "value": "2020-05-20",
        "date": "2020-05-20 12:00:00 UTC"
    },
    "queryFieldHealthAnswer": {
        "entities": [
            {
                "title": "Does diabetes affect sex?",
                "entityUrl": {
                    "routed": true,
                    "path": "/health-answers/does-diabetes-affect-sex"
                },
                "fieldQuestion": {
                    "value": "<p>Dear Alice,</p>\r\n\r\n<p>I have been diabetic for 19 years now (I'm 20 years old) and I was wondering: does having this disease for so long have long-term effects on my sexual health? I am a man and so far I have no issues.</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Dear Alice,</p>\n\n<p>I have been diabetic for 19 years now (I'm 20 years old) and I was wondering: does having this disease for so long have long-term effects on my sexual health? I am a man and so far I have no issues.</p>"
                }
            },
            {
                "title": "Do birth control pills cause brain aneurysms?",
                "entityUrl": {
                    "routed": true,
                    "path": "/health-answers/do-birth-control-pills-cause-brain-aneurysms"
                },
                "fieldQuestion": {
                    "value": "<p>Alice,</p>\r\n\r\n<p>Sadly, today I found out that a friend of my family has just passed away. The cause of her sudden death was a brain aneurysm. I have been researching brain aneurysms and have found out that oral contraceptives are thought to sometimes cause them. I am 19 and I am taking birth control, and now I am very worried. Can you give me some statistics about brain aneurysms linked to oral contraceptives? Thank you.</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Alice,</p>\n\n<p>Sadly, today I found out that a friend of my family has just passed away. The cause of her sudden death was a brain aneurysm. I have been researching brain aneurysms and have found out that oral contraceptives are thought to sometimes cause them. I am 19 and I am taking birth control, and now I am very worried. Can you give me some statistics about brain aneurysms linked to oral contraceptives? Thank you.</p>"
                }
            },
            {
                "title": "Marijuana: Does it cause cancer?",
                "entityUrl": {
                    "routed": true,
                    "path": "/health-answers/marijuana-does-it-cause-cancer"
                },
                "fieldQuestion": {
                    "value": "<p>Dear Alice,</p>\r\n\r\n<p>I have been reading some of your info about marijuana. My question is: If it is so much stronger than smoking cigarettes and has some of the same ingredients, why don't we hear reports of people who have some form of cancer?</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Dear Alice,</p>\n\n<p>I have been reading some of your info about marijuana. My question is: If it is so much stronger than smoking cigarettes and has some of the same ingredients, why don't we hear reports of people who have some form of cancer?</p>"
                }
            },
            {
                "title": "Natural highs",
                "entityUrl": {
                    "routed": true,
                    "path": "/health-answers/natural-highs"
                },
                "fieldQuestion": {
                    "value": "<p>Dear Alice,</p>\r\n\r\n<p>Is there a natural way to get high?</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Dear Alice,</p>\n\n<p>Is there a natural way to get high?</p>"
                }
            },
            {
                "title": "Loving confusion — Is it okay to love same sex friends... and say so?",
                "entityUrl": {
                    "routed": true,
                    "path": "/health-answers/loving-confusion-it-okay-love-same-sex-friends-and-say-so"
                },
                "fieldQuestion": {
                    "value": "<p>Alice,</p>\r\n\r\n<p>What is the difference between \"I like you\" and \"I love you\"?</p>\r\n\r\n<p>I mean, when I say, \"I like you,\" to my male friends, and when I said, \"I love you,\" to my girlfriend and my family. In my opinion, I'm not supposed to say, \"I love you,\" to my best male friend because of homosexuality. Is it true?</p>\r\n\r\n<p>Mr. Doubtful</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Alice,</p>\n\n<p>What is the difference between \"I like you\" and \"I love you\"?</p>\n\n<p>I mean, when I say, \"I like you,\" to my male friends, and when I said, \"I love you,\" to my girlfriend and my family. In my opinion, I'm not supposed to say, \"I love you,\" to my best male friend because of homosexuality. Is it true?</p>\n\n<p>Mr. Doubtful</p>"
                }
            }
        ]
    }
}
]
const activeKey=1

export { theme_week_content,activeKey }
