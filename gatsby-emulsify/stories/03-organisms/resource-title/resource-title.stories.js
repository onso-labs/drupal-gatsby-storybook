import React from 'react';
import { resourceTitle } from './resource-title';

export const ResourceTitle = ({ CampusTitle = resourceTitle }) => (

  <div className="resource-title">
    <h3 class="h3">{CampusTitle.title}</h3>
  </div>
);

export default {
  title: 'Organisms/ResourceTitle',
  component: ResourceTitle,
};



