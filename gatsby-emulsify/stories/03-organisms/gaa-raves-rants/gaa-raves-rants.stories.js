import React from 'react';
import { gaaRavesRants } from "./gaa-raves-rants";
import { GAARave } from '../../02-molecules/gaa-rave/gaa-rave.stories';
export const GAARaveRants = ({ gaaRavesRantsData = gaaRavesRants }) => (
    <ul className="gaa-raves-rants">
        {gaaRavesRantsData.map((item, i) => {
            return (
                <GAARave gaaRavesData={item} />
            )
        })}

    </ul>
);

export default {
    title: 'Organisms/GAARaveRants',
    component: GAARaveRants,
};