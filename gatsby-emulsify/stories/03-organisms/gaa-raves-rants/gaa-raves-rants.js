const gaaRavesRants = [
    {
        gaaRaveModifierClass: 'rant',
        paragraphContent: " Alice, whoever you are, for answering questions and addressing subjects that some people can't even talk about. I also deeply appreciate your warm responses to those who have very delicate issues and intense problems. You are something that I hope to aspire to be — someone who can help others, who can touch others, and who can help create a better individual, community, and world. Alice, whoever you are, for answering questions and addressing subjects that some people can't even talk about. I also deeply appreciate your warm responses to those who have very delicate issues and intense problems. You are something that I hope to aspire to be — someone who can help others, who can touch others, and who can help create a better individual, community, and world."
    },
    {
        gaaRaveModifierClass: 'rave',
        paragraphContent: " Alice, whoever you are, for answering questions and addressing subjects that some people can't even talk about. I also deeply appreciate your warm responses to those who have very delicate issues and intense problems. You are something that I hope to aspire to be — someone who can help others, who can touch others, and who can help create a better individual, community, and world. Alice, whoever you are, for answering questions and addressing subjects that some people can't even talk about. I also deeply appreciate your warm responses to those who have very delicate issues and intense problems. You are something that I hope to aspire to be — someone who can help others, who can touch others, and who can help create a better individual, community, and world."
    },
    {
        gaaRaveModifierClass: 'rant',
        paragraphContent: " Alice, whoever you are, for answering questions and addressing subjects that some people can't even talk about. I also deeply appreciate your warm responses to those who have very delicate issues and intense problems. You are something that I hope to aspire to be — someone who can help others, who can touch others, and who can help create a better individual, community, and world. Alice, whoever you are, for answering questions and addressing subjects that some people can't even talk about. I also deeply appreciate your warm responses to those who have very delicate issues and intense problems. You are something that I hope to aspire to be — someone who can help others, who can touch others, and who can help create a better individual, community, and world."
    }
]
export { gaaRavesRants }
