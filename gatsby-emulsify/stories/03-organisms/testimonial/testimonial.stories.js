import React from 'react';
import { Paragraph } from "../../01-atoms/text/paragraph/paragraph.stories";
import { user_testimonial, alice_reader } from "./testimonial.js";

export const Testimonial = ({ userData = user_testimonial, readerData = alice_reader }) => (
	<div className="testimonial">
		<div className="testimonial__user">
			<Paragraph paragraphContent={userData} />
		</div>
		<div className="testimonial__alice-reader">
			<Paragraph paragraphContent={readerData.paragraphText} />
		</div>
	</div>

);

export default {
	title: 'Organisms/Testimonial',
	component: Testimonial,
};
