//  testimonial_modifier_class: 
//  	- green-text

const user_testimonial = {
    paragraphText: "Great site Alice! I've learned more in the last twenty minutes than I did in the last five years."
}

const alice_reader = {
    paragraphText: " —Go Ask Alice! Fan"
}

export { user_testimonial, alice_reader }