const newQa = {
  link_content: 'Ask Your Question',
  ctaContent: "<b>Couldn't find an answer</b> to your health issue in the Q&A Library? We're always happy to hear from you, so please send us your question.",
  link_url:"/node/add/question",
  newqaTitle: 'Recent Q&As',
  
}

const moreQa = {
  link_content: 'More Q&As',
  link_url: '/answered-questions/recent'
}

const newqaList = [
  {
    "targetId": 11461,
    "entity": {
        "title": "Taking care of the sick without getting sick",
        "entityUrl": {
            "path": "#",
            "routed": true
        }
    }
},
{
  "targetId": 11461,
  "entity": {
      "title": "Prickly feeling feet from standing all day",
      "entityUrl": {
          "path": "/health-answers/taking-care-sick-without-getting-sick",
          "routed": true
      }
  }
},
{
  "targetId": 11461,
  "entity": {
      "title": "Taking care of the sick without getting sick",
      "entityUrl": {
          "path": "/health-answers/taking-care-sick-without-getting-sick",
          "routed": true
      }
  }
}, {
  "targetId": 11461,
  "entity": {
      "title": "Taking care of the sick without getting sick",
      "entityUrl": {
          "path": "/health-answers/taking-care-sick-without-getting-sick",
          "routed": true
      }
  }
},
{
  "targetId": 11461,
  "entity": {
      "title": "Taking care of the sick without getting sick",
      "entityUrl": {
          "path": "/health-answers/taking-care-sick-without-getting-sick",
          "routed": true
      }
  }
},
]


export { newQa, moreQa, newqaList }
