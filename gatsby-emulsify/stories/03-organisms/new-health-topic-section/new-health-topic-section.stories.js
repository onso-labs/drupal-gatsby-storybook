import React from 'react';
import { newQa, moreQa, newqaList } from './new-health-topic-section';
import { Links } from '../../01-atoms/link/link.stories';
export const NewHealthTopicSection = ({ newqa = newQa, moreqa = moreQa, newqaLists = newqaList }) => (
  <div className="new-qa__wrapper">
    <div className="row display-order">
      <div className="col-lg-8 col-md-8 col-sm-12">
        <div className="new-qa__collection">
          <h1 className="h1">{newqa.newqaTitle}</h1>
          <div className="item-list">
            <ul>
              {newqaLists.map((item) => {
                return (
                  <li><Links linkTextValue={item.entity.title} linkRedirectTo={item.entity.entityUrl.path} /></li>
                )
              })}
            </ul>
          </div>
        </div>
        <div className="view-footer">
          <Links linkTextValue={moreqa.link_content} linkRedirectTo={moreqa.link_url} />
        </div>
      </div>
      <div className="col-lg-4 col-md-4 col-sm-12">
        <div className="new-qa__cta-wrapper">
          <div className="cta-content" dangerouslySetInnerHTML={{ __html: newqa.ctaContent }} />
          {/* <div className="cta-content">{newqa.ctaContent}</div> */}
          <div className="cta-button">
            <Links linkTextValue={newqa.link_content} linkRedirectTo={newqa.link_url} />
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default {
  title: 'Organisms/NewHealthTopicSection',
  component: NewHealthTopicSection,
};


