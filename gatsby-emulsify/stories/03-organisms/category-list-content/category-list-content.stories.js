import React from 'react';
import { category_list } from "./category-list-content.js";
import { CategoryList } from "../../02-molecules/category-list/category-list.stories";

export const CategoryListContent = ({ categoryList = category_list, handleSubmit }) => (
    <div className="categoryList__wrapper">
        {categoryList!=undefined?categoryList.rows!=undefined?categoryList.rows.map((item, i) => {
            return (
                <CategoryList categories={item} key={i} submit={handleSubmit} />
            )
        }):'':''}
    </div>
);

export default {
    title: 'Organisms/CategoryListContent',
    component: CategoryListContent,
};