const category_list = [
    {
        "title": 'Marijuana: How long does it hang out in the body?',
        "entityUrl": {
          "routed": true,
          "path": "/node/1081356"
        },
        "fieldQuestion": {
          "value": "<p>Question Pub. Cont-2</p>\r\n",
          "format": "basic_html",
          "processed": "<p>Alice, How long does marijuana stay in the body?</p>\n"
        },
        "fieldAnswer": {
          "value": "<p>Answer Pub. Cont-2</p>\r\n",
          "format": "basic_html",
          "processed": "<p>Answer Pub. Cont-2</p>\n",
          "summary": "",
          "summaryProcessed": ""
        },
        "fieldCategory": [
            {
                "targetId": 4,
                "entity": {
                  "entityUrl": {
                    "routed": true,
                    "path": "#"
                  },
                  "entityLabel": "Alcohol & Other Drugs,"
                }
              },
              {
                "targetId": 5,
                "entity": {
                  "entityUrl": {
                    "routed": true,
                    "path": "#"
                  },
                  "entityLabel": "Marijuana, Hash, & Other Cannabis"
                }
              },
        ],
        "fieldResources": [
          {
            "targetId": 1081357,
            "entity": {
              "entityLabel": "Reference Content 1",
              "entityUrl": {
                "routed": true,
                "path": "/node/1081357"
              }
            }
          },
          {
            "targetId": 1081359,
            "entity": {
              "entityLabel": "Reference Content 3",
              "entityUrl": {
                "routed": true,
                "path": "/node/1081359"
              }
            }
          },
          {
            "targetId": 1081360,
            "entity": {
              "entityLabel": "Reference Content 4",
              "entityUrl": {
                "routed": true,
                "path": "/node/1081360"
              }
            }
          }
        ]
    },
    {
    "title": 'Alcohol enemas?',
    "entityUrl": {
        "routed": true,
        "path": "#"
    },
    "fieldQuestion": {
        "value": "<p>Question Pub. Cont-2</p>\r\n",
        "format": "basic_html",
        "processed": "<p>Alice, I have been living an 'alternative' lifestyle for many years now which leads to a lot of well...alternative situations. I have</p>\n"
    },
    "fieldAnswer": {
        "value": "<p>Answer Pub. Cont-2</p>\r\n",
        "format": "basic_html",
        "processed": "<p>Answer Pub. Cont-2</p>\n",
        "summary": "",
        "summaryProcessed": ""
    },
    "fieldCategory": [
        {
            "targetId": 4,
            "entity": {
                "entityUrl": {
                "routed": true,
                "path": "#"
                },
                "entityLabel": "Alcohol & Other Drugs,"
            }
            },
            {
            "targetId": 5,
            "entity": {
                "entityUrl": {
                "routed": true,
                "path": "#"
                },
                "entityLabel": "Alcohol"
            }
            },
    ],
    "fieldResources": [
        {
        "targetId": 1081357,
        "entity": {
            "entityLabel": "Reference Content 1",
            "entityUrl": {
            "routed": true,
            "path": "/node/1081357"
            }
        }
        },
        {
        "targetId": 1081359,
        "entity": {
            "entityLabel": "Reference Content 3",
            "entityUrl": {
            "routed": true,
            "path": "/node/1081359"
            }
        }
        },
        {
        "targetId": 1081360,
        "entity": {
            "entityLabel": "Reference Content 4",
            "entityUrl": {
            "routed": true,
            "path": "/node/1081360"
            }
        }
        }
    ]
    },
]

export { category_list }