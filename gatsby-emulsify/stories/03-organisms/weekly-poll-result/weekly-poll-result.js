
const weeklyPollResult = {
    resultMessage: "Your vote is being tallied. Meanwhile, here's how other readers voted.",
    paragraphContent: "17"
}
const resultData = [
    {
        paragraphContent: "Highlighters",
        votePercentage: '30',
        voteNumbers: '21',
    },
    {
        paragraphContent: "Notecards",
        votePercentage: '20',
        voteNumbers: '11',
    },
    {
        paragraphContent: "Sticky notes",
        votePercentage: '60',
        voteNumbers: '17',
    },
    {
        paragraphContent: "Class outlines",
        votePercentage: '70',
        voteNumbers: '32',
    },
    {
        paragraphContent: "Other",
        votePercentage: '3',
        voteNumbers: '14',
    }
]

export { weeklyPollResult, resultData }

