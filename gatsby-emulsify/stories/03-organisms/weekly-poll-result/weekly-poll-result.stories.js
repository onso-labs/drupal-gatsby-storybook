import React from 'react';
import { weeklyPollResult, resultData } from "./weekly-poll-result";
import { Paragraph } from '../../01-atoms/text/paragraph/paragraph.stories';
export const WeeklyPollResult = ({ pollResult = weeklyPollResult, resultDatas = resultData }) => (
	<div class="weekllypollresult">
		{pollResult.resultMessage != undefined && (
			pollResult.resultMessage.map((item) => (
				<div class="results-message">{item}</div>
			)
			))}

		{resultDatas.map((item) => (
			<div class="weekllypollresult__process-bar">
				<div class="weekllypollresult__text">
					<Paragraph paragraphContent={item.paragraphContent != undefined ? item.paragraphContent : item.chtext != null ? item.chtext : ''} />
				</div>
				<div class="weekllypollresult__progress">
					<div class="weekllypollresult__progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style={{ width: item.votePercentage != undefined ? item.votePercentage : item.vote_perc != null ? item.vote_perc : '' + '%' }}  ></div>
				</div>
				<div class="weekllypollresult__percent">
					<span>{item.votePercentage != undefined ? item.votePercentage : item.vote_perc != null ? item.vote_perc : ''}%</span>
					<span> ({item.voteNumbers != undefined ? item.voteNumbers : item.chvotes != null ? item.chvotes : ''} votes)</span>
				</div>
			</div>
		))}
		<div class="weekllypollresult__total-votes">
			<span>Total votes:</span>
			<Paragraph paragraphContent={pollResult.paragraphContent != undefined ? pollResult.paragraphContent : pollResult.field_total_votes ? pollResult.field_total_votes : ''} />
		</div>
	</div >
);

export default {
	title: 'Organisms/WeeklyPollResult',
	component: WeeklyPollResult,
};
