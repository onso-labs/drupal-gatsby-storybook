import React from 'react';
import { linkgroups, paraLastupdate, paragraphOne, paragraphCopyright } from './site-footer';
import { Links } from '../../../01-atoms/link/link.stories';
import { Paragraph } from '../../../01-atoms/text/paragraph/paragraph.stories';
import { SiteUpperFooter } from '../site-upper-footer/site-upper-footer.stories';
// import { Link } from 'gatsby';
export const Footer = ({ footerData = linkgroups, paraLastupdates = paraLastupdate, dataCopyright = paragraphCopyright, paragraphOneData = paragraphOne, update, navigateQuestionpage,originally }) => (
	<div>
		<div className="footer">
			<SiteUpperFooter navigate={navigateQuestionpage} />
			<div className="footer-bottom">
				<div className="footer-bottom__inner">
					<div className="footer-bottom__mainlinks">
						<ul className="footer-bottom__link-group">
							<li className="footer-bottom__main-title">
								<Links href="#" linkTextValue={footerData[0].link_main_title} />
							</li>
							{footerData[0].linklists.map((item) => {
								return (
									<li className="footer-bottom__links"><Links linkTextValue={item.linkContent} linkRedirectTo={item.linkUrl} />
									</li>
								)
							})}
						</ul>
						<ul className="footer-bottom__link-group">
							<li className="footer-bottom__main-title">
								<Links href="#" linkTextValue={footerData[1].link_main_title} />
							</li>
							{footerData[1].linklists.map((item) => {
								return (
									<li className="footer-bottom__links"><Links linkTextValue={item.linkContent} linkRedirectTo={item.linkUrl} />
									</li>
								)
							})}
						</ul>
					</div>

					<div className="footer-bottom__lastupdate">
						<div className="footer-bottom__para-lastupdate">
							<Paragraph paragraphContent={paraLastupdates.paragraphContent} />
							<div className="footer-bottom__emergency-text">
								<p>{paragraphOneData.paragraph}
									<Links linkTextValue={paragraphOneData.linkContent} linkRedirectTo={paragraphOneData.linkUrl} />
									{paragraphOneData.paragraphText}</p>
							</div>
						</div>
						{originally !== undefined && (
							<div class="gaa-last-updated-reviewed">
								<div class="gaa-last-updated-reviewed-label">Last Updated / Reviewed:</div>
								<div class="last-updated-date">{originally}</div>
							</div>
							
						)}
{update !== undefined && (
							<div class="gaa-last-updated-reviewed published-date">
								<div class="gaa-last-updated-reviewed-label">Originally Published:</div>
								<div class="last-updated-date">{update}</div>
							</div>
							
						)}
					</div>
				</div>
			</div>
		</div >
		<div className="footer-bottom__copyright">
			{dataCopyright.map((item) => {
				return (
					<Paragraph paragraphContent={item.paragraphContent} />
				)
			})}
		</div>
	</div>
);

export default {
	title: 'Organisms/Site/Footer',
	component: Footer,
};

