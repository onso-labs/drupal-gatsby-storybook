const linkgroups = [
	{
		link_main_title: 'Contact Alice!',
		linklists: [
			{
				linkContent: 'Content Use',
				linkUrl: '/contact-alice/collaboration'
			},
			{
				linkContent: 'Media Inquiries',
				linkUrl: '/contact-alice/press-request'
			},
			{
				linkContent: 'Comments & Corrections',
				linkUrl: '/contact-alice/comments-corrections'
			}
		]
	},
	{
		link_main_title: 'Syndication & Licensing',
		linklists: [
			{
				linkContent: 'Licensing Q&As',
				linkUrl: '/contact-alice/licensing',
			},
			{
				linkContent: 'Get Alice! on Your Website',
				linkUrl: '/contact-alice/feed-request'
			},
			{
				linkContent: 'Full Site Syndication',
				linkUrl: '/contact-alice/syndication'
			},
			{
				linkContent: 'Link to Go Ask Alice!',
				linkUrl: '/about-alice/link-go-ask-alice'
			}
		]
	}
]

const paraLastupdate = {
	paragraphContent: "Go Ask Alice! is not an emergency or instant response service."
}

const paragraphCopyright = [
	{
		paragraphContent: "All materials on this website are copyrighted. Copyright by The Trustees of Columbia University in the City of New York. All rights reserved."
	},
	{
		paragraphContent: "© 2005 – 2019"
	}
]

const paragraphOne = {
	paragraph: 'If you are in an urgent situation, please',
	linkContent: ' visit our Emergency page',
	linkUrl: '/find-help/emergency',
	paragraphText: 'to view a list of 24 hour support services and hotlines.'

}

export { linkgroups, paraLastupdate, paragraphOne, paragraphCopyright }
