import React from 'react';
import { Image } from "../../../01-atoms/images/images.stories"
import { Links } from '../../../01-atoms/link/link.stories.js';

import logoImg from '../../../../images/icons/upperlogo.png';

export const SiteUpperHeader = ({ url}) => (

	<div className="upper-header">
		<div className="container">
			<div className="upper-header__logo">
				<Links linkclassName="logo-link" linkTextValue={<Image imgSrc={logoImg} alt='header logo' />} linkRedirectTo="https://www.columbia.edu" />
			</div>
		</div>
	</div>

);

export default {
	title: 'Organisms/site/SiteUpperHeader',
	component: SiteUpperHeader,
};