import React from 'react';
import { FormGroup, Input, Collapse, Navbar, NavbarToggler } from 'reactstrap';
import { IoIosMenu, IoIosCloseCircleOutline } from "react-icons/io";
import { FaSistrix, FaAngleDown } from 'react-icons/fa';
import { Image } from "../../../01-atoms/images/images.stories"
import { Links } from '../../../01-atoms/link/link.stories.js';
import logoImg from '../../../../images/logo-main.svg';
import { mainMenu } from "./site-header";
import { navigate } from 'gatsby';
import ReactDOM from 'react-dom';
class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.wrapperRef = React.createRef();
        this.handleClickOutside = this.handleClickOutside.bind(this);
        this.state = {
            isOpenMenu: false,
            isOpenSearch: false,
            search: "",
            activeId: null
        }
    }

    handleClick = (event, id) => {
        if (this.state.activeId == id) {
            this.setState({ activeId: null })
            setTimeout(
                () => {
                    var arialabel = document.querySelector('[aria-label="system-breadcrumb"]');
                    if (arialabel != null) {
                        arialabel.style.marginTop = 0 + "px";
                    } else {
                        document.getElementsByClassName("main-layout")[0].style.marginTop = 0 + "px";
                    }
                }, 100
            );
        } else {
            this.setState({ activeId: id })
            setTimeout(
                () => {
                    var arialabel = document.querySelector('[aria-label="system-breadcrumb"]');
                    if (arialabel != null) {
                        arialabel.style.marginTop = document.getElementsByClassName("active-dropdown")[0].getElementsByTagName("ul")[0].clientHeight + "px";
                    } else {
                        document.getElementsByClassName("main-layout")[0].style.marginTop = document.getElementsByClassName("active-dropdown")[0].getElementsByTagName("ul")[0].clientHeight + "px";
                    }
                }, 100
            );
        }
    }
    handleChange = (e) => {
        this.setState({ search: e.target.value })
    }

    searchsubmit = () => {
        this.props.submit(this.state.search);
        let search = this.state.search;
        // SearchText.searchText = this.state.search;
        // navigate('/search/node', { state: { search } });
        this.setState({ search: "" })
    }

    keyPress = (e) => {
        // e.preventDefault();
        if (e.keyCode == 13) {
            this.props.submit(this.state.search);
            let search = this.state.search;
            // SearchText.searchText = this.state.search;
            // navigate('/search/node', { state: { search } });
            this.setState({ search: "" })
            // e.preventDefault();
            // let str = window.location.href;
            // var keyword = str.substring(str.lastIndexOf("=") + 1, str.length);
            // console.log("keyword", keyword);
            // this.setState({ SearchText: keyword});
        }
    }

    toggle = (type) => {
        // console.log("abcd")

        if (type == 'menu') {
            // console.log("abc")
            this.setState({
                isOpenMenu: !this.state.isOpenMenu,
                isOpenSearch: false
            });
        } else if (type == 'search') {
            this.setState({
                isOpenSearch: !this.state.isOpenSearch,
                isOpenMenu: false
            });
        }

    }
    categoryNavigate = (key, title) => {
        // console.log("key>>",key)
        let array = [];
        array.push(key)
        // SearchText.array = array
        // SearchText.searchText = title
        title = title.replace("&", "AND");
        // navigate('/health-answers/tag', { state: { array, title: title } });
        navigate('/health-answers/tag?cat=' + array + "&title=" + title);
    }
    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }
    /**
     * Alert if clicked on outside of element
     */
    handleClickOutside(event) {
        const domNode = ReactDOM.findDOMNode(this);
        if (!domNode || !domNode.contains(event.target)) {
            var element = document.getElementsByClassName("active-dropdown");
            if (element[0] != undefined) {

                element[0].classList.toggle("active-dropdown");
                this.setState({ activeId: null });
            }
            var arialabel = document.querySelector('[aria-label="system-breadcrumb"]');
            if (arialabel != null) {
                arialabel.style.marginTop = 0 + "px";
            } else {
                var element2 = document.getElementsByClassName("main-layout");
                if (element2[0] != undefined) {
                    element2[0].style.marginTop = 0 + "px";
                }
            }
        }
    }

    render() {
        return (
            <div>
                <header className="header" >
                    <div className="row align-items-end">
                        <div className="col-lg-8 col-md-8 col-sm-4 col-5 right-padding">
                            <div className="header__branding">
                                <Links linkclassName="logo-link" linkTextValue={<Image imgSrc={logoImg} alt='header logo' />} linkRedirectTo='/' />
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-4 col-sm-8 col-7 left-padding">
                            <div className="search-box desktop-show">
                                <div className="search-form desktop-show">
                                    <FormGroup>
                                        <Input type="form-submit" name="search" className="search-input" title="Enter the terms you wish to search for." value={this.state.search} onChange={this.handleChange} onKeyDown={this.keyPress} />
                                        <Input type="submit" name="search" value={this.state.search} className="form-submit search-icon" onClick={this.searchsubmit} />
                                    </FormGroup>
                                </div>
                            </div>
                            <div class="toggle-icon-right">
                                <div className="nav-menu-toggler form-action">
                                    <NavbarToggler onClick={() => this.toggle('menu')} className={` ${this.state.isOpenMenu ? 'nav-active' : ''}`}>
                                        <span className="menu-toggler toggle-expand__text"> {this.state.isOpenMenu ? <IoIosCloseCircleOutline /> : <IoIosMenu />}  </span>
                                        Menu
                                    </NavbarToggler>
                                </div>
                                <div className={`nav-menu-toggler search-toggle ${this.state.isOpenSearch ? 'active-search' : ''}`}>
                                    <a className="search-act search-expand" onClick={() => this.toggle('search')} >
                                        <span className="menu-toggler search__text">{this.state.isOpenSearch ? <IoIosCloseCircleOutline /> : <FaSistrix />}</span>
                                        Search
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <Collapse isOpen={this.state.isOpenSearch}>
                    <FormGroup className="search-form mobile-show">
                        <Input type="form-submit" name="search" className="search-input" value={this.state.search} onChange={this.handleChange} onKeyDown={this.keyPress} />
                        <Input type="submit" name="search" value={this.state.search} className="form-submit search-icon" onClick={this.searchsubmit} />
                    </FormGroup>
                </Collapse>
                <Navbar className="navbar navbar-expand-lg bg-black header-nav-menu" id="navbar" expand="md">
                    <Collapse isOpen={this.state.isOpenMenu} navbar className="main-nav">
                        <ul className="navbar-nav main-menu">
                            {mainMenu.map((item, i) => {
                                return (
                                    <li key={i}
                                        className={this.state.activeId === i ? 'main-menu__item main-menu__item--with-sub active-dropdown' : 'main-menu__item main-menu__item--with-sub'}
                                        onClick={(e) => { this.handleClick(e, i) }}
                                        to="">
                                        <a className="link" href={item.url}> {item.title} <FaAngleDown /></a>
                                        <ul className="main-menu main-menu--sub main-menu--sub-1">
                                            {item.below.map((ele) => {
                                                return (
                                                    <li className="main-menu__item main-menu__item--sub main-menu__item--sub-1" onClick={() => ele.id !== undefined ? this.categoryNavigate(ele.id, ele.subTitle) : ''}>
                                                        <Links linkTextValue={ele.subTitle} linkRedirectTo={ele.url} />
                                                    </li>
                                                )
                                            })}
                                        </ul>
                                    </li>
                                )
                            })}
                        </ul>
                    </Collapse >
                </Navbar >
            </div >
        );
    }

};
export const Header = ({ menus = mainMenu, handleSubmit }) => (
    <Menu submit={handleSubmit} />
)
export default {
    title: 'Organisms/site/Header',
    component: Header,
};