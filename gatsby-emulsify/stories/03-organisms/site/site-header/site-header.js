
const mainMenu = [
    {
        title: 'Health Topics',
        // url: '#',
        below: [
            {

                subTitle: 'Recent Q&As',
                url: '/answered-questions/recent',
            },
            {
                subTitle: 'Alcohol & Other Drugs',
                url: '/health-answers/tag',
                id: '1'
            },
            {
                subTitle: 'Emotional Health',
                url: '/health-answers/tag',
                id: '6'
            },
            {
                subTitle: 'General Health',
                url: '/health-answers/tag',
                id: '11'
            },
            {
                subTitle: 'Nutrition & Physical Activity',
                url: '/health-answers/tag',
                id: '16'
            },
            {
                subTitle: 'Relationships',
                url: '/health-answers/tag',
                id: '21'
            },
            {
                subTitle: 'Sexual & Reproductive Health',
                url: '/health-answers/tag',
                id: '26'
            },
            {
                subTitle: 'Themes',
                url: '/themes'
            },
            {
                subTitle: 'Fact Sheets',
                url: '/fact-sheets'
            }

        ]
    },
    {
        title: 'Quizzes',
        url: '#',
        below: [
            {
                subTitle: 'Quizzes',
                url: '/quizzes'
            },
            {
                subTitle: 'Polls',
                url: '/polls'
            }
        ]
    },
    {
        title: 'Find Help',
        url: '#',
        below: [
            {
                subTitle: 'In an Emergency',
                url: '/find-help/emergency'
            },
            {
                subTitle: 'On-campus Resources',
                url: '/find-help/campus-resources'
            }
        ]
    },
    {
        title: 'About Alice!',
        url: '#',
        below: [
            {
                subTitle: 'All About Alice!',
                url: '/about-alice/all-about'
            },
            {
                subTitle: 'Go Ask Alice! History',
                url: '/about-alice/go-ask-alice-history'
            },
            {
                subTitle: 'Raves & Rants',
                url: '/about-alice/raves-rants'
            },
            {
                subTitle: '25 Years of Questionable Behavior',
                url: '/basic-page/25-years-questionable-behavior'
            },
            {
                subTitle: 'FAQs',
                url: '/about-alice/faqs'
            }

        ]
    },
    {
        title: 'Subscribe',
        url: '#',
        below: [
            {
                subTitle: 'Get Alice! In Your Box',
                url: '/contact-alice/get-alice-in-your-inbox'
            }
        ]
    },
    // {
    //     title: 'Accessibility',
    //     url: '#',
    //     below: [
    //         {
    //             subTitle: 'Quiz',
    //             url: '/accessibility/quizzes'
    //         },

    //         {
    //             subTitle: 'FAQ',
    //             url: '/accessibility/faqs'
    //         },
    //         {
    //             subTitle: 'Theme',
    //             url: '/accessibility/theme'
    //         },
    //         {
    //             subTitle: 'Form',
    //             url: '/accessibility/form'
    //         },
    //         {
    //             subTitle: 'Raves & Rants',
    //             url: '/accessibility/ravs-rants'
    //         },
    //         {
    //             subTitle: 'Health Resources',
    //             url: '/accessibility/resources'
    //         }
    //     ]
    // }
]

export { mainMenu };

