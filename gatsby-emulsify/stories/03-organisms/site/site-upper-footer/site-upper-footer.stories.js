import React from 'react';
import { SocialMenu } from '../../../02-molecules/menus/social/social-menu.stories';
import footerLogoImg from '../../../../images/icons/footer-logo.svg';
import { Image } from "../../../01-atoms/images/images.stories"
import { Links } from '../../../01-atoms/link/link.stories.js';

export const SiteUpperFooter = ({ navigate }) => (
  <div className="upper-footer">
    <div className="upper-footer__logo">
      <Links linkclassName="logo-link" linkTextValue={<Image imgSrc={footerLogoImg} alt="footer-logo" className="image" />} linkRedirecTo='#' />

    </div>
    <div className="upper-footer__social">
      <SocialMenu navigateQuestionpage={navigate} />
    </div>
  </div>
);

export default {
  title: 'Organisms/Site/SiteUpperFooter',
  component: SiteUpperFooter,
};

