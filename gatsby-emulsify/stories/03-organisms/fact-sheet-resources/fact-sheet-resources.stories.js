import React from 'react';
import { Paragraph } from "../../01-atoms/text/paragraph/paragraph.stories";
import { Links } from "../../01-atoms/link/link.stories";
export const FactSheetResources = ({ data}) => (
  
 <div className="card__wrapper">
   <div className="card__heading">
     <h2 className="h2">{data.title ? data.title : ''}</h2>
   </div>
   <div className="card__body">
     <div className="row">
       <div className="col-lg-6">
         <div className="campus-phone">
           <div className="phone-wrap">
             <h3>Phone</h3>
             <div>{data.field_phone}</div>
           </div>
           <h3 className="h3">Website</h3>
         {/* {data.field_website} */}
         <Links linkTextValue={data.field_website_1 !== null ? data.field_website_1 : ''} 
         linkRedirectTo={data.field_website !== null ? data.field_website : ''} />
         </div>
       </div>
       <div className="col-lg-6">
         {/* {cardDetail.map((item) => {
         return ( */}
         <Paragraph paragraphContent={data.body !== null ? data.body : ''} />
         {/* )
       })} */}
       {/* <div className="read-more-text">
       <Links linkTextValue="Read more" linkRedirectTo='' />
       </div> */}
       </div>
     </div>
   </div>
 </div>

);

export default {
    title: 'Organisms/FactSheetResources',
    component: FactSheetResources,
};


