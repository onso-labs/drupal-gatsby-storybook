const linkList = [
  {
    "entity": {
      "entityLabel": "Content Use",
      "entityUrl": {
        "routed": true,
        "path": "/contact-alice/collaboration"
      }
    }
  },
  {
    "entity": {
      "entityLabel": "Media Inquiries",
      "entityUrl": {
        "routed": true,
        "path": "/contact-alice/press-request"
      }
    }
  },
  {
    "entity": {
      "entityLabel": "Comments & Corrections",
      "entityUrl": {
        "routed": true,
        "path": "/contact-alice/comments-corrections"
      }
    }
  }
]
const blockTitle = "Contact Alice!"

export { linkList, blockTitle }