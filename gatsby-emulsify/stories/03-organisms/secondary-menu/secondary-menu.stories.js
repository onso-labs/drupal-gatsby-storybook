import { isTerminating } from 'apollo-link/lib/linkUtils';
import React from 'react';
import { Links } from "../../01-atoms/link/link.stories";
import { linkList, blockTitle } from "./secondary-menu";

export const SecondaryMenu = ({ list = linkList, title = blockTitle }) => (
    <div className={title != "Health topics" ? "link-list-wrapper" : ''}>
        <div className={title != "Health topics" ? "block-title" : ''}>
            <h2 className="h2">{title != "Health topics" ? title : ''} </h2>
        </div>
        <ul>
            {list.length > 0 && list.map((item) => {
                var url = "";
                if (title == "Resources") {
                    url = (`/find-help/campus-resources?name=${item.entity !== null && item.entity !== undefined ? item.entity.entityLabel : ''}`)
                }
                else if (title == "Health topics") {

                    url = (`/health-answers/tag?cat=${item.tid}&title=${item.field_category !== null && item.field_category !== undefined ? item.field_category : ''}`)

                }
                //<div dangerouslySetInnerHTML={{ __html: pageContent.contentData }} />

                return (
                    title == "Resources" ? <li><Links
                        linkTextValue={<div dangerouslySetInnerHTML={{ __html: item.entity !== null && item.entity !== undefined ? item.entity.entityLabel : item.title !== null ? item.title : item.field_category != null ? item.field_category : '' }} />}

                        linkRedirectTo={url}
                    /></li> :
                        title == "Health topics" ?
                            <>
                                <li><Links
                                    linkTextValue=
                                    {<div dangerouslySetInnerHTML={{ __html: item.field_category !== null && item.field_category !== undefined ? item.field_category : '' }} />}

                                    linkRedirectTo={url}
                                /></li>
                                {/* <div dangerouslySetInnerHTML={{ __html: item.field_category_export !== null && item.field_category_export !== undefined ? item.field_category_export[0] : '' }} /> */}
                                </> :
                            <li><Links
                                linkTextValue=
                                {<div dangerouslySetInnerHTML={{ __html: item.entity !== null && item.entity !== undefined ? item.entity.entityLabel : item.title !== null ? item.title : '' }} />}

                                linkRedirectTo={item.entity !== null && item.entity !== undefined ? item.entity.entityUrl.path : item.view_node != null ? item.view_node : item.view_taxonomy_term != null ? item.view_taxonomy_term : ''}
                            /></li>
                )
            })}
        </ul>
    </div>
);

export default {
    title: 'Organisms/SecondaryMenu',
    component: SecondaryMenu,
};
