const instructionData = {
  btnValue: 'Ok, show the question form',
  instruction: '<p><strong>Before you submit…</strong></p>'
}

const instructionList = [
  {
    value: 'Did you search the Go Ask Alice! Archives to see if your question has been answered already?'
  },
  {
    value: 'Please limit your question to these categories: Alcohol &amp; Other Drugs, Emotional Health, Nutrition Physical Activity, General Health, Relationships, or Sexual Reproductive Health.'
  },
  {
    value: 'This form is for submitting health related questions only. To contact Alice! about other matters, please use the appropriate Contact Alice! resource.'
  }
]


export { instructionData, instructionList }



