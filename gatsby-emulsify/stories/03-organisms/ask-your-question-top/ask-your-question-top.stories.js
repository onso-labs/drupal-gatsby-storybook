import React from 'react';
import { instructionData, instructionList } from './ask-your-question-top';
import { Primarybtn } from '../../01-atoms/buttons/button.stories';
import { Paragraph } from '../../01-atoms/text/paragraph/paragraph.stories';
// import { Links } from '../../01-atoms/link/link.stories';
export const AskYourQuestionTop = ({ instruction = instructionData, instructionlist = instructionList, showForm }) => (
  <div class="ask-your-question-top">
    <Paragraph paragraphContent={instruction.instruction} />
    <ol>
      {instructionlist.map((item) => {
        return (
          <li>{item.value}</li>
        )
      })
      }
    </ol>
    <div class="ask-question-btn">
      <Primarybtn buttonTextValue={instruction.btnValue} value={showForm} />
    </div>
  </div>
);

export default {
  title: 'Organisms/AskYourQuestionTop',
  component: AskYourQuestionTop,
};
