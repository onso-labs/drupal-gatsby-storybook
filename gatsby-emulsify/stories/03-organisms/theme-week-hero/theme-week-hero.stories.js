import React from 'react';
import { Links } from "../../01-atoms/link/link.stories";
import { theme, theme_list } from "./theme-week-hero";
import moment from 'moment';

export const ThemeWeekHero = ({ themes = theme, themeLinks = theme_list }) => (
	<div className="theme-of-week__wrapper">
		<div className="theme-of-week__header">
			<h2 className="h2">Theme of the Week</h2>
		</div>
		<div className="theme-of-week__content-wrapper">
			<div className="theme-title">
				<h2 className="h2"><Links linkTextValue={<div dangerouslySetInnerHTML={{ __html: themes?.entityLabel }} />}
				linkRedirectTo="/themes" /></h2>
				<div className="theme-date">{themes.fieldWeek ==null?'':moment(themes.fieldWeek ? themes.fieldWeek.value : '').format("MMMM DD, YYYY")}</div>
			</div>
			<div className="theme-content">
				<div className="theme-list">
					<ul>
						{themeLinks.map((item) => {
							if (item !== null) {
								return (
									<li>
										<Links linkTextValue={<div dangerouslySetInnerHTML={{ __html:item.title && item.title !== null ? item.title : ''}} />} linkRedirectTo={item.entityUrl.path} />
									</li>
								)
							}
						})}
					</ul>
				</div>
				<div className="theme-footer">
					<Links linkTextValue="More Themes of the Week" linkRedirectTo="/themes" />
				</div>
			</div>
		</div>
	</div>
);

export default {
	title: 'Organisms/ThemeWeekHero',
	component: ThemeWeekHero,
};
