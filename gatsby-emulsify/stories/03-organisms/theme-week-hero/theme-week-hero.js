const theme ={
    body:{
        value: 'A Little Stress Conversation',
    
    },
    fieldWeek:{
        "value":'May 01, 2020'
    }
}


const theme_list = [
    {
        "title": "Stress ball",
        "entityUrl": {
            "path": "#"
        }
    },
    {
        "title": 'Stress balls',
        "entityUrl":{
            "path":"#"
        }
    },
    {
        "title": 'Can stress from school cause nausea and stomachaches? ',
        "entityUrl":{
            "path":"#"
        }
    },
    {
        "title": 'Good stress?',
        "entityUrl":{
            "path":"#"
        }
    },
    {
        "title": 'How to reduce stress at work',
        "entityUrl":{
            "path":"#"
        }
    },
    {
        "title": 'Can stress kill?',
        "entityUrl":{
            "path":"#"
        }
    }
]
export { theme, theme_list }