import React from 'react';
import { NewHealthTopic } from "../../02-molecules/new-health-topic/new-health-topic.stories";
import { newQA } from "./new-health-topics";

export const NewHealthTopics = ({ newQAs = newQA }) => (
    <div>
        {newQAs.map((item) => {
            return (
                <NewHealthTopic list={item.linkList} title={item.blockTitle} />
            )
        })
        }
    </div >
);

export default {
    title: 'Organisms/NewHealthTopics',
    component: NewHealthTopics,
};

