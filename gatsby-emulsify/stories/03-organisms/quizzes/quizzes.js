const quizTitle = {
    quizTitle: "Quick Quizzes",
    quizReadMore: "More Quizzes"
}
const quizzes = [
    {
        "nid": 1080818,
        "title": "Know all about the benefits of exercise? Flex your knowledge!",
        "body": {
            "value": "<p>Put your reps, sets, and laps knowledge to the test with this quiz.</p>\r\n",
            "format": "basic_html",
            "processed": "<p>Put your reps, sets, and laps knowledge to the test with this quiz.</p>\n"
        },
        "entityUrl": {
            "path": "/quizzes/know-all-about-benefits-exercise-flex-your-knowledge"
        },
        // "fieldQuestionsAnswers": {
        //     "question": "question"
        // }

    },
    {
        "nid": 1080817,
        "title": "Eat Well, Feel Well?: A Nutritiously Delicious Quiz",
        "body": {
            "value": "<p>Test your knowledge to see if you know what it takes to maintain a healthy diet.</p>\r\n",
            "format": "basic_html",
            "processed": "<p>Test your knowledge to see if you know what it takes to maintain a healthy diet.</p>\n"
        },
        "entityUrl": {
            "path": "/quizzes/eat-well-feel-well-nutritiously-delicious-quiz"
        }
    }
]

export { quizzes, quizTitle }

