import React from 'react';
import { quizzes, quizTitle } from "./quizzes";
import { Links } from '../../01-atoms/link/link.stories';
import { ListContentBlock } from '../../03-organisms/list-content-block/list-content-block.stories';
export const Quizzes = ({ quiz = quizzes, quizzesTitle = quizTitle }) => (
	<div class="quick-quizzes">
		{(quizzesTitle.quizTitle !== "" && quizzesTitle.quizTitle !== null) && (
			<div className="block-title">
				<h2>
					{quizzesTitle.quizTitle}
				</h2>
			</div>
		)}
		<div classname="quizzes-content">
			<div className="quick-quizzes__list">
				<ListContentBlock searchlist={quiz} />
			</div>
		</div>

		{(quizzesTitle.quizReadMore !== "" && quizzesTitle.quizReadMore !== null) && (
			<div className="read__more">
				<Links linkTextValue="More quizzes" linkRedirectTo="/quizzes" />
			</div>
		)}
	</div>
);

export default {
	title: 'Organisms/Quizzes',
	component: Quizzes,
};
