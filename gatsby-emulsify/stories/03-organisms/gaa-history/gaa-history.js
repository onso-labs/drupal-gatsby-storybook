import GAAoriginal from '../../../images/icons/GAA_Original.jpg';
import Gaa1998 from '../../../images/icons/GAA_1998.gif';
import GAAPills from "../../../images/icons/Get_Alice_Pill.jpg";
const years = [
  {
    yearContent: '1993',
    yearUrl: '#1993'
  },
  {
    yearContent: '1994',
    yearUrl: '#1994'
  },
  {
    yearContent: '1996',
    yearUrl: '#1996'
  },
  {
    yearContent: '1997',
    yearUrl: '#1997'
  },
  {
    yearContent: '1998',
    yearUrl: '#1998'
  },
  {
    yearContent: '1999',
    yearUrl: '#1999'
  },
  {
    yearContent: '2000',
    yearUrl: '#2000'
  },
  {
    yearContent: '2002',
    yearUrl: '#2022'
  },
  {
    yearContent: '2003',
    yearUrl: '#2003'
  },
  {
    yearContent: '2004',
    yearUrl: '#2004'
  },
  {
    yearContent: '2006',
    yearUrl: '#2006'
  },
  {
    yearContent: '2007',
    yearUrl: '#2007'
  },
  {
    yearContent: '2009',
    yearUrl: '#2009'
  },
  {
    yearContent: '2010',
    yearUrl: '#2010'
  },
  {
    yearContent: '2011',
    yearUrl: '#2011'
  },
  {
    yearContent: '2012',
    yearUrl: '#2012'
  },
  {
    yearContent: '2013',
    yearUrl: '#2013'
  },
  {
    yearContent: '2014',
    yearUrl: '#2014'
  },
  {
    yearContent: '2015',
    yearUrl: '#2015'
  },
  {
    yearContent: '2016',
    yearUrl: '#2016'
  },
  {
    yearContent: '2017',
    yearUrl: '#2017'
  }
]

const historyitems = [
  {
    gaaHistoryYear: '1993',
    heroimagesrc: GAAoriginal,
    heroimagealt: 'GAA_Original',
    historyDetails: '<i> Go Ask Alice! </i> begins. Initially only available to Columbia students.'
  },
  {
    gaaHistoryYear: '1994',
    heroimagesrc: '',
    historyDetails: '<i> Go Ask Alice! </i> goes live on the internet, likely making it the oldest major health Q&A site on the web.'
  },
  {
    gaaHistoryYear: '1996',
    heroimagesrc: '',
    historyDetails: '<i> Go Ask Alice! </i> publishes the 1000th Q&A.'
  },
  {
    gaaHistoryYear: '1997',
    heroimagesrc: '',
    historyDetails: '<p>Syndication of <i> Go Ask Alice! </i> Q&As in campus newspapers and other print sources around the country begins.</p><p> <i> Go Ask Alice! </i> cited in U.S.Supreme Court case on free speech on the internet.</p>'
  },
  {
    gaaHistoryYear: '1998',
    heroimagesrc: Gaa1998,
    heroimagealt: 'GAA_1998',
    historyDetails: '<p>The <i> Go Ask Alice! </i> Book of Answers is published and the site gets an overhaul and updated look.</p><p> <i> Go Ask Alice! </i> is recognized with its first major award, the HEDIR Technology Award presented in conjunction with the American Public Health Association.</p>'
  },
  {
    gaaHistoryYear: '1999',
    heroimagesrc: '',
    historyDetails: 'The American Library Association recommends < i > Go Ask Alice! </i > as a web resource on health information for young adults.'
  },
  {
    gaaHistoryYear: '2000',
    heroimagesrc: GAAPills,
    heroimagealt: 'Healthline std_BADGE 2017',
    historyDetails: 'You no longer have rush to your web browser to check out the new and updated Q&As. Get Alice! in Your Box (our weekly e  mail) starts.'
  },
  {
    gaaHistoryYear: '2002',
    heroimagesrc: '',
    historyDetails: '<i> Go Ask Alice! </i> teams up with MTV in their Fight For Your Rights sexual health campaign.'
  },
  {
    gaaHistoryYear: '2003',
    heroimagesrc: '',
    historyDetails: '<i> Go Ask Alice! </i> celebrates 10 wonderful years and Get Alice! In Your Box has subscribers from 190 countries.'
  },
  {
    gaaHistoryYear: '2004',
    heroimagesrc: '../images/icons/gaa2004.jpg',
    heroimagealt: 'Healthline std_BADGE 2017',
    historyDetails: 'The <i> Go Ask Alice! </i> website gets a new look.'
  },
  {
    gaaHistoryYear: '2006',
    heroimagesrc: '',
    historyDetails: 'The new update and review quality assurance process is launched.'
  },
  {
    gaaHistoryYear: '2007',
    heroimagesrc: '',
    historyDetails: '<i> Go Ask Alice! </i> wins the Best Practices in College Health Award from the American College Health Association and the Grand Gold Award for outstanding work in health and wellness from NASPA: Student Affairs Administrators in Higher Education.'
  },
  {
    gaaHistoryYear: '2009',
    heroimagesrc: '../images/icons/wha_winner_09_bronze.jpg',
    heroimagealt: 'wha_winner_09_bronze.jpg',
    historyDetails: '<i> Go Ask Alice! </i> wins a Web Health Award and also joins Facebook. Are you a fan?'
  },
  {
    gaaHistoryYear: '2010',
    heroimagesrc: '',
    historyDetails: 'An independent study from Stanford University lists <i> Go Ask Alice! </i> first among websites for accurate reproductive health information on the web. The study was published in Knowledge Quest: Journal of the American Association of School Librarians.'
  },
  {
    gaaHistoryYear: '2011',
    heroimagesrc: '../images/icons/small_GAA_COLOR.jpg',
    heroimagealt: 'small_GAA_COLOR.jpg',
    historyDetails: '<p><i> Go Ask Alice! </i> turns 18 and the site gets a brand new look. Included are new features, more interaction, better integration with social media, and a widget to have <i> Go Ask Alice! </i> new Q&As automatically display on other websites.</p><p> More than 1.7 million people visit the site each month.</p>'
  },
  {
    gaaHistoryYear: '2012',
    heroimagesrc: '../images/icons/small_Best_of_the_web.png',
    heroimagealt: 'small_Best_of_the_web.png',
    historyDetails: '<p><i> Go Ask Alice! </i> is named one of the best sites on the web for STD & HIV information by Healthline.</p><p> The site was visited more than 22 million times in 2012. Thanks for reading!</p>'
  },
  {
    gaaHistoryYear: '2013',
    heroimagesrc: '../images/icons/small_Healthline_best_of_the_web_2013.jpg',
    heroimagealt: 'small_Healthline_best_of_the_web_2013.jpg',
    historyDetails: '<p>Once again <i> Go Ask Alice! </i> is named best of the web by Healthline. Two years in a row!</p><p> <i> Go Ask Alice! </i> proudly celebrates 20 years of answering your questions.</p>'
  },
  {

    gaaHistoryYear: '2014',
    heroimagesrc: '../images/icons/small_Best_of  badge  hiv  std_2014.png',
    heroimagealt: 'small_Best_of  badge  hiv  std_2014.png',
    historyDetails: '<p>For the third consecutive year<i> Go Ask Alice! </i > is name best of the web by Healthline.</p><p>Readership continues to rise with over 4 million people a month visiting the site!</p>'
  },
  {
    gaaHistoryYear: '2015',
    heroimagesrc: '',
    historyDetails: 'The site gets another overhaul with a refreshed look and feel. Additionally <i> Go Ask Alice! </i> is built to higher standards of web accessibility to enable better performance for low  vision and blind users.'
  },
  {
    gaaHistoryYear: '2016',
    heroimagesrc: '',
    historyDetails: 'After many requests the <i> Go Ask Alice! </i> team begins a project to make campus  specific versions of the site available to other colleges and universities.'
  },
  {
    gaaHistoryYear: '2017',
    heroimagesrc: '../images/icons/Healthline std_BADGE 2017.png',
    heroimagealt: 'Healthline std_BADGE 2017',
    historyDetails: 'Once again, <i> Go Ask Alice! </i> has been named one of the Best STD Blogs of the Year by Healthline!'
  }
]

export { years, historyitems }