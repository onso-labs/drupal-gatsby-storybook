import { years, historyitems } from './gaa-history';
import React from 'react';
import { Links } from '../../01-atoms/link/link.stories';
import { Image } from '../../01-atoms/images/images.stories';
export const GAAHistory = ({ gaaYear = years, historyData = historyitems }) => (
  <div class="history">
    <ul class="history__years">
      {gaaYear.map((item, i) => {
        return (
          <li class="history__year">
            <Links linkTextValue={item.yearContent} linkRedirecTo={item.yearUrl} />
          </li>
        )
      })}

    </ul>
    <ul class="history__items">
      {historyData.map((item, i) => {
        return (
          <li class="history__item">
            <div id="1993" class="history__itemyear">
              {item.title}
            </div>
            {(item.fieldLogo !== "" && item.fieldLogo !== null) && (
              <div class="history__heroimage">
                <Image imgSrc={item.fieldLogo} alt={item.heroimagealt} />
              </div>
            )}

            <div className="history__details" dangerouslySetInnerHTML={{ __html: item.body?.value }} />
          </li>
        )
      })}

    </ul>
  </div>
);

export default {
  title: 'Organisms/GAAHistory',
  component: GAAHistory,
};


