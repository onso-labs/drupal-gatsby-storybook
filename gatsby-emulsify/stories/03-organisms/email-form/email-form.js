const formData = {
  btnValue: 'Submit',
  paragraph_content: "This question is for testing whether or not you are a human visitor and to prevent automated spam submissions.",
  requireMsg: 'This field is required.',
}

export { formData }



