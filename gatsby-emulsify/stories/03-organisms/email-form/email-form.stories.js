import React from 'react';
import { formData } from './email-form';
import { Primarybtn } from '../../01-atoms/buttons/button.stories';
import { Links } from "../../01-atoms/link/link.stories";
import config from '../../../src/config';
import ReCAPTCHA from "react-google-recaptcha";
// import { Links } from '../../01-atoms/link/link.stories';
export const EmailForm = ({ form = formData, name = '', email = '', message = '', verifyCallback, handlechange, handleSubmit, sendto = '', subject = '', emailError = false, subjectError = false, sendtoError = false, messageError = false, btnName, page = '', path = "#", handleRedirect,reCaptchaError=false }) => (
  <form class="custom-form email-form">
    <div class="form-item">
      <label for="edit-name" class="form-item__label form-item__label--required form-item__label--textfield">Your email <span class="form-required">* <span class="form-required-text">{form.requireMsg}</span></span></label>
      <input autocorrect="none" autocapitalize="none" spellcheck="false" aria-describedby="edit-name--description" type="email" name="name" value={email} size="60" maxlength="60" class="form-item__textfield" className={emailError ? "invalidClass" : ""} aria-required="true" onChange={(e) => handlechange(e, "email")} />
    </div>

    <div class="form-item">
      <label for="name" class="form-item__label form-item__label--textfield">Your name</label>
      <input id="name" value={name} type="text" class="form-item__textfield" onChange={(e) => handlechange(e, "name")} />
    </div>

    <div class="form-item">
      <label for="sendto" class="form-item__label form-item__label--textfield">Send to <span class="form-required">* <span class="form-required-text" >{form.requireMsg}</span></span></label>
      <textarea id="sendto" class="form-item__textfield form-item__textarea" rows="3" cols="60" value={sendto} onChange={(e) => handlechange(e, "sendto")} className={sendtoError ? "invalidClass" : ""}></textarea>
      <div class="description">Enter multiple addresses separated by commas and/or different lines.</div>

    </div>

    <div class="form-item">
      <label for="subject" class="form-item__label form-item__label--textfield">Subject<span class="form-required">* <span class="form-required-text" >{form.requireMsg}</span></span></label>
      <input id="subject" value={subject} type="text" class="form-item__textfield" onChange={(e) => handlechange(e, "subject")} className={subjectError ? "invalidClass" : ""} />
    </div>

    <div class="mail-attach"><label>Page to be sent</label>
      <Links linkTextValue={page} linkRedirectTo={path} /></div>
    <div class="form-item">
      <label for="textarea" class="form-item__label form-item__label--textfield">Your message <span class="form-required">
        <span class="form-required-text">*{form.requireMsg}</span></span></label>
      <textarea id="textarea" class="form-item__textfield form-item__textarea" rows="8" cols="48" value={message} onChange={(e) => handlechange(e, "message")} className={messageError ? "invalidClass" : ""}></textarea>
    </div>
    <div class="form-captcha-content">
      <fieldset className={`form-fieldset ${reCaptchaError ? 'invalidClass' : ''}`}>
        <legend>CAPTCHA</legend>
        {form.paragraph_content}
        <ReCAPTCHA
          sitekey={config.ReCAPTCHA_Key}
          onChange={verifyCallback}
        />
      </fieldset>
    </div>
    <div class="custom-form__btn">
      <Primarybtn buttonTextValue={btnName} value={handleSubmit} />
      <Primarybtn buttonTextValue="Cancel" value={handleRedirect} />
    </div>
  </form>
);

export default {
  title: 'Organisms/EmailForm',
  component: EmailForm,
};


