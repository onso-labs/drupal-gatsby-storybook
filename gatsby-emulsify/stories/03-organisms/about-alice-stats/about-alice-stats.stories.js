import React from 'react';
import { statsTitle, statRows } from './about-alice-stats';
import { Paragraph } from '../../01-atoms/text/paragraph/paragraph.stories';
export const AboutAliceStats = ({ title = statsTitle, rows = statRows }) => (
  <div className="stats">
    <h2 className="stats__title">
      {title.statTitle}
    </h2>
    <table className="stats__table" width="100%" border="0" cellspacing="0" cellpadding="0">
      <tbody>
        {rows.map((item) => {
          return (
            <tr>
              <td>
                <Paragraph paragraphContent={item.statsNumbers} />
                {(rows.statsNumbersUnit !== "" && rows.statsNumbersUnit !== null) && (
                  <span>
                    <Paragraph paragraphContent={item.statsNumbersUnit} />
                  </span>
                )}
              </td>
              <td>
                <Paragraph paragraphContent={item.statsContent} />
              </td>
            </tr>
          )
        })}
      </tbody>
    </table>
  </div>
);

export default {
  title: 'Organisms/AboutAliceStats',
  component: AboutAliceStats,
};


