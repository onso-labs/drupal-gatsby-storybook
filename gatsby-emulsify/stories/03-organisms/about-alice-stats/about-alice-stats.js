const statsTitle = {
  statTitle: 'Alices Vital Stats'
}
const statRows = [
  {
    statsNumbers: '827',
    statsNumbersUnit: '',
    statsContent: 'Questions submitted last month'
  },
  {
    statsNumbers: '2.3',
    statsNumbersUnit: 'million',
    statsContent: 'Visits last month'
  },
  {
    statsNumbers: '389',
    statsNumbersUnit: '',
    statsContent: 'Questions answered and updated this year'
  }
]

export { statsTitle, statRows }
