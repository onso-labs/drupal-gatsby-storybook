import React from 'react';
import { formData, categoryList } from './ask-your-question-form';
import { Successbtn } from '../../01-atoms/buttons/button.stories';
import { Paragraph } from '../../01-atoms/text/paragraph/paragraph.stories';
// import { Links } from '../../01-atoms/link/link.stories';
import config from '../../../src/config';
import ReCAPTCHA from "react-google-recaptcha";
export const AskYourQuestionForm = ({ formContent = formData, category = categoryList, title, question, verifyCallback, handleChange, submitQuestion, categoryitem, btnName, error = false ,reCaptchaError=false}) => (

  <form className="ask-your-question-form custom-form">
    <div className="form-item">
      <label for="edit-name" className="form-item__label form-item__label--required form-item__label--textfield">Title <span className="form-required">* <span className="form-required-text">{formContent.requireMsg}</span></span></label>
      <input autocorrect="none" autocapitalize="none" spellcheck="false" aria-describedby="edit-name--description" type="text" name="name" value={title} size="60" maxlength="60" className="form-item__textfield" aria-required="true" onChange={(e) => { handleChange(e.target.value, "title") }} className={error ? "invalidClass" : ""} />
    </div>
    <div className="form-item">
      <label for="textarea" className="form-item__label form-item__label--textfield">Question</label>
      <textarea id="textarea" className="form-item__textfield form-item__textarea" rows="8" cols="48" value={question} onChange={(e) => { handleChange(e.target.value, "question") }}></textarea>
    </div>
    <div className="form-item select-category">
      <label for="select" className="form-item__label form-item__label--textfield">Health topics</label>
      <div className="form-item__dropdown">
        <select className="form-item__select" id="select" onChange={(e) => { handleChange(e.target.value, "category") }} value={categoryitem}>
          {category.map((item) => {
            return (
              <option value={item.tid}>{item.name}</option>
            )
          })
          }
        </select>
      </div>
    </div>
    <div className="form-captcha-content">
      <fieldset className={`form-fieldset ${reCaptchaError ? 'invalidClass' : ''}`} >
        <legend>CAPTCHA</legend>
        <Paragraph paragraphContent={formContent.paragraphContent} />
        <ReCAPTCHA
          style={{ display: "inline-block" }}
          sitekey={config.ReCAPTCHA_Key}
          onChange={verifyCallback}
        />
      </fieldset>
    </div>
    <div className="ask-your-question-content">
      <Paragraph paragraphContent={formContent.askQuestionContent} />
    </div>

    <div className="ask-your-question-form custom-form__btn">
      <Successbtn buttonTextValue={btnName} value={submitQuestion} />
    </div>
  </form >
);

export default {
  title: 'Organisms/AskYourQuestionForm',
  component: AskYourQuestionForm,
};


