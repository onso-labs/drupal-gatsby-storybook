const formData = {
  btnValue: 'Submit',
  askQuestionContent: '<p>Go Ask Alice! provides health information and should not be considered specific medical advice, a diagnosis, treatment, or a second opinion for health conditions, nor should it be considered a reporting mechanism. If you have an existing ailment that could be adversely affected by information provided on this site, or if you have an urgent health problem, consult with a health care provider before acting on information contained here. Go Ask Alice! is not an instant reply service, due to high volume often there is a delay in reviewing question submissions, and cannot reply directly to individuals. Questions submitted are considered for future publication on the site. Staff members reviewing submissions may be legally required to report them due to their content; the full contents of any reportable submissions will be shared as part of a duty to report.</p>',
  paragraphContent: "<p>This question is for testing whether or not you are a human visitor and to prevent automated spam submissions.</p>",
  requireMsg: 'This field is required.',
}

const categoryList = [
  {
    tid:'1',
    name: 'Alcohol & Other Drugs'
  },
  {
    tid:'6',
    name: 'Emotional Health'
  },
  {
    tid:'11',
    name: 'General Health'
  },
  {
    tid:'16',
    name: 'Nutrition & Physical Activity'
  },
  {
    tid:'21',
    name: 'Relationships'
  },
  {
    tid:'26',
    name: 'Sexual & Reproductive Health'
  }
]


export { formData, categoryList }



