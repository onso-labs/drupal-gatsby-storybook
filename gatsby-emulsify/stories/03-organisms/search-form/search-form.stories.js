import React from 'react';
import { formData, categoryList } from './search-form';
import { Primarybtn } from '../../01-atoms/buttons/button.stories';
import { Paragraph } from '../../01-atoms/text/paragraph/paragraph.stories';
// import { Links } from '../../01-atoms/link/link.stories';
export const SearchForm = ({ formContent = formData, category = categoryList, search, question, handleChange, submit, categoryitem, btnName = "search", error = false, keyPress, show = true }) => (

  <form className="search-form">
    <div className="form-item">
      <label for="edit-name" className="form-item__label form-item__label--required form-item__label--textfield">Enter your keywords</label>
      <input autocorrect="none" autocapitalize="none" spellcheck="false" aria-describedby="edit-name--description" type="text" name="name" value={search} size="40" maxlength="255" className="form-item__textfield" aria-required="true" onChange={(e) => { handleChange(e.target.value) }} onKeyDown={(e) => { keyPress(e) }} className={error ? "invalidClass" : ""} />
    </div>
    {show == true && (
      <div className="search-form-btn custom-form__btn">
        <Primarybtn buttonTextValue={btnName} value={submit} />
      </div>
    )}

  </form >
);

export default {
  title: 'Organisms/SearchForm',
  component: SearchForm,
};


