import React from 'react';
import { Paragraph } from "../../01-atoms/text/paragraph/paragraph.stories";
import { allAboutAlice } from "./all-about-alice.js";
import { Links } from "../../01-atoms/link/link.stories";

export const AllAboutAlice = ({ aboutAlice = allAboutAlice }) => (
    <div className="all-about-alice__wrapper">
        {(aboutAlice.linkAboutAlice !== "" && aboutAlice.linkAboutAlice !== null) && (
            <div className="all-about-alice__header">
                <h2 className="h2">
                    <Links linkTextValue={aboutAlice.linkAboutAlice} linkRedirectTo={aboutAlice.linkAboutAliceUrl} />
                </h2>
            </div>
        )}

        <div className="all-about-alice__body">
            <Paragraph paragraphContent={aboutAlice.paragraphContent} />
        </div>
        {/* {(aboutAlice.linkMoreAlice !== "" && aboutAlice.linkMoreAlice !== null) && (
            <div className="theme-footer">
                <Links linkTextValue={aboutAlice.linkMoreAlice} linkRedirectTo={aboutAlice.linkMoreUrl} />
            </div>
        )} */}
    </div>
);

export default {
    title: 'Molecules/AllAboutAlice',
    component: AllAboutAlice,
};