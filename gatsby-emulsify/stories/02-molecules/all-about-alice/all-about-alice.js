const allAboutAlice =
{
    linkAboutAlice: 'All About Alice!',
    linkAboutAliceUrl: '#',
    linkMoreAlice: 'Read more',
    linkMoreUrl: '#',
    paragraphContent: '<p><b>Who is Alice!?</b></p><p>Alice! is not one person, but a team. The Go Ask Alice! site is supported by a team of Columbia University health promotion specialists, health care providers, and other health professionals, along with a staff of information and research specialists and writers. Our team members have advanced degrees in public health, health education, medicine, counseling, and a number of other relevant fields.</p>'

}
export { allAboutAlice }