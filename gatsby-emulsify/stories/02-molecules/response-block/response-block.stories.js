import React from 'react';
import { Paragraph } from "../../01-atoms/text/paragraph/paragraph.stories";

export const ResponseBlock = ({ Responses }) => (
    <div className="response-block">
        <div className="response__header">
            <h2 className="h2">Responses</h2>
        </div>
        <div class="response-body-wrap">
            {Responses.map((item) => {
                if (item !== null) {
                    return (<React.Fragment>
                        <div className="response__date">
                            <h2 className="h2">{item.created && item.created !== null ? item.created : ''}</h2>
                        </div>
                        <div className="response__body">
                            <Paragraph paragraphContent={item.body} />
                        </div>
                    </React.Fragment>
                    )
                }
            })}
        </div>
    </div>
);

export default {
    title: 'Molecules/ResponseBlock',
    component: ResponseBlock,
};