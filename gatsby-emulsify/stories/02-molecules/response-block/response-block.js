const responseBlock =
{
    responseHeader: 'Responses',
    created: 'Mar 20, 2012',
    body: "<p>I had this problem for years! Little did I know, I was just using razors for too long. Now that I throw them out after 3 maximum 4 uses, I don't have this problem :) I'm so much happier! So maybe this is the case. Worth a shot.</p>"

}
export { responseBlock }