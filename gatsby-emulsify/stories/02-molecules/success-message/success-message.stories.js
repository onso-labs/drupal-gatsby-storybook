import React from 'react';
import { Paragraph } from "../../01-atoms/text/paragraph/paragraph.stories";
import { right } from "./success-message";
import { Image } from '../../01-atoms/images/images.stories';

export const SuccessMessage = ({ message = '', rightarrow = right }) => (
    <div className="success-message">
        <div class="messages-status">
            <div class="messages__icon">
                <Image imgSrc={rightarrow.imgsrc} alt={rightarrow.imgalt} />
            </div>
            {message}
        </div>
    </div>

);

export default {
    title: 'Molecules/SuccessMessage',
    component: SuccessMessage,
};