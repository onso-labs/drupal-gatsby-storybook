import rightIcon from '../../../images/icons/right.svg';

const right = {
    imgsrc: rightIcon,
    imgalt: 'success',
}

export { right }