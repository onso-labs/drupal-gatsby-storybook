import React from 'react';
import { Paragraph } from "../../01-atoms/text/paragraph/paragraph.stories";
// import { Checkbox } from '../../01-atoms/forms/checkbox/checkbox.stories';
import { quizzesAnswer } from "./quizzes-answer-block";
import { Radio } from '../../01-atoms/forms/radio/radio.stories';

export const QuizzesAnswerBlock = ({ quizzesAnswers = quizzesAnswer, checked, loadMore, change }) => (
    <div class="answer__quiz-block">
        <div class="answer__block">
            <div class="answer__quiz">
                {quizzesAnswers.question}
            </div>
            <fieldset class="form-item form-item--checkbox">
                <legend class="h3">Options as Checkboxes</legend>
                <ul class="form-item form-item--radio">
                    {quizzesAnswers.options.map((item, i) => {
                        return (
                            <li className="form-item--radio__item form-item">
                                <Radio className="form-radio" type="radio" checked={checked} id={item} name={quizzesAnswers.question} label={item} value={item} data={true} onchange={change} />
                                {/* <Checkbox type="checkbox" id={item.label} name={item.name} label={item.label} value={item.value} onchange={(e) => { onchange(e.target.value) }} data={true} /> */}
                            </li>
                        )
                    })}
                </ul>
            </fieldset>
            <input type="button" class="btn" value="Reply" onClick={(e) => loadMore(e)} />
        </div>
    </div>
);

export default {
    title: 'Molecules/QuizzesAnswerBlock',
    component: QuizzesAnswerBlock,
};