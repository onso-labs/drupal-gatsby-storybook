const quizzesAnswer = [
    {
        name: "radio",
        value: "Lack of healthy options at home, school, or work.",
        label: "Lack of healthy options at home, school, or work.",
        checked: false
    },
    {
        name: "radio",
        value: "Hefty price tags on healthy foods.",
        label: "Hefty price tags on healthy foods.",
        checked: false
    },
    {
        name: "radio",
        value: "Not enough time to prepare healthy meals.",
        label: "Not enough time to prepare healthy meals.",
        checked: false
    },
    {
        name: "radio",
        value: "Eating out for most meals where there aren't a lot of healthy options.",
        label: "Eating out for most meals where there aren't a lot of healthy options.",
        checked: false
    },
    {
        name: "radio",
        value: "All of the above.",
        label: "All of the above.",
        checked: false
    }
]
export { quizzesAnswer }
