import React from 'react';
import { Paragraph } from "../../01-atoms/text/paragraph/paragraph.stories";
import { themeAnswer, sign } from "./answer-block.js";
import { Image } from '../../01-atoms/images/images.stories';

export const AnswerBlock = ({ themeAnswers = themeAnswer, signs = sign }) => (
    <div className="answer__block">
        {themeAnswers !== null && (
            <div className="theme-answer-wrap">
                <Paragraph paragraphContent={themeAnswers.value} />
            </div>
        )}
        {/* {Ans.paragraphContent} */}
        <div class="alice-signature">
            <Image imgSrc={signs.imgsrc} alt={signs.imgalt} />
        </div>
    </div>
);

export default {
    title: 'Molecules/AnswerBlock',
    component: AnswerBlock,
};