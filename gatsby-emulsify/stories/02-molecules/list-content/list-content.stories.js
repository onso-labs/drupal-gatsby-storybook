import React from 'react';
import { Links } from "../../01-atoms/link/link.stories";
import { Paragraph } from "../../01-atoms/text/paragraph/paragraph.stories";

import { list_content } from "./list-content.js";

export const listContent = ({ list_contents = list_content }) => (
    <div>
        <h3><Links linkTextValue={list_contents.link_text_value} linkRedirectTo={list_contents.link_url} /></h3>
        <Paragraph paragraph_content={list_contents.paragraph_content} />
    </div>
);

export default {
    title: 'Molecules/ListContent',
    component: listContent,
};