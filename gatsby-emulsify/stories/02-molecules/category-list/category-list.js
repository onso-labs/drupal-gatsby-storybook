const category =
{
    "title": "Pub. Cont-2",
    "entityUrl": {
      "routed": true,
      "path": "/node/1081356"
    },
    "fieldQuestion": {
      "value": "<p>Question Pub. Cont-2</p>\r\n",
      "format": "basic_html",
      "processed": "<p>Question Pub. Cont-2</p>\n"
    },
    "fieldAnswer": {
      "value": "<p>Answer Pub. Cont-2</p>\r\n",
      "format": "basic_html",
      "processed": "<p>Answer Pub. Cont-2</p>\n",
      "summary": "",
      "summaryProcessed": ""
    },
    "fieldCategory": [],
    "fieldResources": [
      {
        "targetId": 1081357,
        "entity": {
          "entityLabel": "Reference Content 1",
          "entityUrl": {
            "routed": true,
            "path": "/node/1081357"
          }
        }
      },
      {
        "targetId": 1081359,
        "entity": {
          "entityLabel": "Reference Content 3",
          "entityUrl": {
            "routed": true,
            "path": "/node/1081359"
          }
        }
      },
      {
        "targetId": 1081360,
        "entity": {
          "entityLabel": "Reference Content 4",
          "entityUrl": {
            "routed": true,
            "path": "/node/1081360"
          }
        }
      }
    ]
  }

export { category}


