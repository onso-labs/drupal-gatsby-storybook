import React from 'react';
import { Links } from "../../01-atoms/link/link.stories";
import { Paragraph } from "../../01-atoms/text/paragraph/paragraph.stories";

import { category } from "./category-list.js";

export const CategoryList = ({ categories = category, submit }) => (

    <div className="category__wrapper">
        <div className="category__header">
            <Links linkTextValue={ <div dangerouslySetInnerHTML={{ __html: categories.title!=""?categories.title:categories.block_title!=""?categories.block_title:categories.poll_title!=""?categories.poll_title:'' }} />} linkRedirectTo={ categories.entityUrl ? categories.entityUrl.path : categories.block_type=="Theme Of The Week"?'/themes?theme='+categories.info_1: categories.field_search_result_link_target} />
        </div>
        <div className="category__description">
            <Paragraph paragraphContent={categories.fieldQuestion ? categories.fieldQuestion.value : categories.field_question!=""?categories.field_question:categories.body!=""?categories.body:'...'} />
        </div>
        <div className="category__filter">
            {categories.fieldCategory ?
                categories.fieldCategory.map((item, i) => {
                    return (
                        <Links linkTextValue={item.entityLabel} linkRedirectTo={item.entity.entityUrl.path} key={i} />
                    )
                }) :
                categories.field_category_export.map((item, i) => {
                    return (

                        <p tabindex="0" className="link-orange link" style={{ cursor: 'pointer' }} onClick={() => submit(item.id)} key={i}>
                            {i < categories.field_category_export.length - 1 ? item.title + ', ' : item.title}
                            {/* <Link linkClass="link-orange" linkUrl="/health-answers/categories" linkText={JSON.parse(item).title} key={i} /> */}
                        </p>

                        // <Links linkTextValue={i < categories.field_category_export.length - 1 ? item.title + ', ' : item.title}
                        //     // linkRedirectTo={item.entity.entityUrl.path} 
                        //     key={i} />
                    )
                })
            }

        </div>
    </div>

);

export default {
    title: 'Molecules/CategoryList',
    component: CategoryList,
};