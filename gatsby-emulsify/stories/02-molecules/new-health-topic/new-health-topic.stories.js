import React from 'react';
import { Links } from "../../01-atoms/link/link.stories";
import { linkList, blockTitle } from "./new-health-topic.js";

export const NewHealthTopic = ({ list = linkList, title = blockTitle }) => (
    <div className="new-qa__list-wrapper">
        <div className="new-qa__date">
            <h2 className="h2">{title.title}</h2>
        </div>
        <ul>
            {list.map((item) => {
                return (
                    <li><Links linkTextValue={item.list} /></li>
                )
            })}
        </ul>
    </div>
);

export default {
    title: 'Molecules/NewHealthTopic',
    component: NewHealthTopic,
};