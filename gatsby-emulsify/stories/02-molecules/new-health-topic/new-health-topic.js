const linkList = [
    {
        list: "2019 novel coronavirus (COVID-19)"
    },
    {
        list: "What health risks do humans face from polluted oceans?"
    },
    {
        list: "Comments & Corrections"
    },
]
const blockTitle =
{
    title: "Jan 31, 2020"
}

export { linkList, blockTitle }