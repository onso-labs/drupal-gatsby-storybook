import React from 'react';
import { askQuestion } from './cta';
import { Links } from "../../01-atoms/link/link.stories";

// import { Secondarybtn } from '../../01-atoms/buttons/button.stories';
// import { withA11y } from '@storybook/addon-a11y';
import { Paragraph } from '../../01-atoms/text/paragraph/paragraph.stories';

// import { storiesOf, addDecorator, addParameters } from '@storybook/react';

export const CTA = ({ cta = askQuestion }) => (
  <div className="Ask-question-wapper">
    <div className="ask-question-content">
      <Paragraph paragraphContent={cta.questionInfo} />
      {/* <div dangerouslySetInnerHTML={{ __html: cta.questionInfo }} /> */}
    </div>
    <div className="ask-question-btn">
      <Links linkRedirectTo={cta.linkUrl} linkTextValue={cta.linkValue}></Links>
      {/* <Secondarybtn buttonTextValue={cta.btnValue} /> */}
    </div>
  </div>
);

// export const accessible = () => (
//   <button>
//     Accessible button
//   </button>
// );

// export const inaccessible = () => (
//   <button style={{ backgroundColor: 'red', color: 'darkRed', }}>
//     Inaccessible button
//   </button>
// );

export default {
  title: 'Molecules/CTA',
  component: CTA,
  // decorators: [withA11y],
  // parameters: {
  //   a11y: {
  //     // optional selector which element to inspect
  //     element: '#root',
  //     // axe-core configurationOptions (https://github.com/dequelabs/axe-core/blob/develop/doc/API.md#parameters-1)
  //     config: {},
  //     // axe-core optionsParameter (https://github.com/dequelabs/axe-core/blob/develop/doc/API.md#options-parameter)
  //     options: {},
  //   },
  // }
};


