const askQuestion =
{
  btnValue: "ask your question",
  questionInfo: "<b>Couldn't find an answer</b> to your health issue in the Q&A Library? We're always happy to hear from you, so please send us your question.",
  linkUrl: "/node/add/question",
  linkValue: "Ask your question"
}

export { askQuestion }
