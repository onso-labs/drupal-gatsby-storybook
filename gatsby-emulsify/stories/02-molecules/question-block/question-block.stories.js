import React from 'react';
import { Paragraph } from "../../01-atoms/text/paragraph/paragraph.stories";
import { themeQuestion } from "./question-block";
import { Links } from "../../01-atoms/link/link.stories";

export const QuestionBlock = ({ themeQuestions=themeQuestion, type = "" }) => (
    <div className="question__wrapper">
        {type == "health_topic" ? <React.Fragment>
            {(themeQuestions?.title !== undefined && themeQuestions?.title !== "" && themeQuestions?.title !== null) && (
                < div className="theme-question">
                    <h2 className="h2"><Links linkTextValue={themeQuestions?.title} linkRedirectTo={themeQuestions.entityUrl ? themeQuestions.entityUrl.path : '/'} /></h2>

                </div>
            )}

            <div className="question-block">
                {themeQuestions !== null && (
                    <Paragraph paragraphContent={type == "message" ?
                        themeQuestions.message :
                        themeQuestions.fieldQuestion != undefined ?
                            themeQuestions.fieldQuestion.value :
                            themeQuestions.value} />
                )}
            </div>
            {(themeQuestions.entityUrl !== null) && (
                <div className="theme-footer">
                    <Links linkTextValue="Read more" linkRedirectTo={themeQuestions.entityUrl.path} />
                </div>
            )}

        </React.Fragment> :
            <React.Fragment>
                {(themeQuestions?.title !== undefined && themeQuestions?.title !== "" && themeQuestions?.title !== null) && (
                    < div className="theme-question">
                        <h2 className="h2"><Links linkTextValue={themeQuestions?.title} linkRedirectTo={themeQuestions.entityUrl != undefined ? themeQuestions.entityUrl.path : '/'} /></h2>

                    </div>
                )}

                <div className="question-block">
                    {themeQuestions !== null && (
                        (<React.Fragment>

                            <Paragraph paragraphContent={(type == "message" ?
                                themeQuestions.message :
                                themeQuestions.fieldQuestion != undefined ?
                                    themeQuestions.fieldQuestion.value :
                                    themeQuestions.value)} /></React.Fragment>)
                    )}
                </div>
                {/* {(themeQuestions.link_read_more !== "" && themeQuestions.link_more_url !== null) && (
            <div className="theme-footer">
                <Links linkTextValue={themeQuestions.link_read_more} linkRedirectTo={themeQuestions.link_more_url} />
            </div>
        )} */}
                {(themeQuestions?.view_node !== null && type == "health_topic") && (
                    <div className="theme-footer">
                        <Links linkTextValue="Read more" linkRedirectTo={themeQuestions.view_node} />
                    </div>
                )}
            </React.Fragment>
        } </div >
);

export default {
    title: 'Molecules/QuestionBlock',
    component: QuestionBlock,
};