const themeQuestion =
{
    value: "<p>Hey Alice!</p><p>I've been hearing a lot on the news about the new virus[novel coronavirus, COVID- 19]and I'm seeing students on campus wearing masks. The news seems to have conflicting information and I'm not sure what to do.Should I be worried ? Do I need to wear a mask too ? Thank you.</p > ",
    link_text_value: '2019 novel coronavirus (COVID-19)',
    link_url: '#',
    link_read_more: 'Read more',
    link_more_url: '#'
}
export { themeQuestion }