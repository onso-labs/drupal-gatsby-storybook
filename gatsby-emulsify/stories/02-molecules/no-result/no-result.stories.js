import React from 'react';

export const NoResult = () => (
    <div className="no-result">
        <h2>
            Your search yielded no results
        </h2>
        <ul>
            <li>Check if your spelling is correct.</li>
            <li>Remove quotes around phrases to search for each word individually. <i>bike shed</i> will often show more results than <i>"bike shed"</i>.</li>
            <li>Consider loosening your query with <i>OR</i>. <i>bike OR shed</i> will often show more results than <i>bike shed</i>.</li>
        </ul>
    </div>
);

export default {
    title: 'Molecules/noResult',
    component: NoResult,
};


