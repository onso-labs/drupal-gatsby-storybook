import React from 'react';
import { Links } from "../../01-atoms/link/link.stories";
import { Paragraph } from "../../01-atoms/text/paragraph/paragraph.stories";
import { QuestionBlock } from '../question-block/question-block.stories';
import { theme_week, themeAnswerList, _key } from "./theme-week-collection";
import moment from 'moment';
import Collapse from "rc-collapse";
var Panel = Collapse.Panel;

export const ThemeWeekCollection = ({ themeWeeks = theme_week, themeAnswersList = themeAnswerList, panelkey = _key, activeKey, onKeyChange }) => (

	<Collapse themeWeeks={true} activeKey={activeKey} onChange={(e) => onKeyChange(panelkey)}
		destroyInactivePanel={true} className={`theme-week__wrapper ${themeWeeks?.entityLabel ? "" : "test"} ${panelkey == activeKey ? "theme-active" : ""}`}>
		<div className="theme-week">
			<span className="week-title">Week of:</span>
			<div className="theme-date">{themeWeeks.fieldWeek && themeWeeks.fieldWeek !== null ? moment(themeWeeks.fieldWeek.value).format("MMMM DD, YYYY") : ''}</div>
		</div>
		<Panel key={panelkey} header={<div dangerouslySetInnerHTML={{ __html: themeWeeks?.entityLabel }} />} headerClass="theme__question link h2" className="theme__wrapper">
			{/* <h2 className="h2"><Links linkTextValue={theme_weeks.link_theme_title} linkRedirectTo={theme_weeks.link_theme_url} /></h2> */}

			{themeAnswersList.map((item) => {
				if (item !== null) {
					return (
						<QuestionBlock themeQuestions={item} />
					)
				}

			})}
		</Panel>

	</Collapse>

);
export default {
	title: 'Molecules/ThemeWeekCollection',
	component: ThemeWeekCollection,
};