const accordion =
{
  question: 'What is the mission of Go Ask Alice! ?',
  answer: 'Go Ask Alice! is a health Q&A Internet resource. It provides readers with reliable, accurate, accessible, culturally competent information and a range of thoughtful perspectives so that they can make responsible decisions concerning their health and well-being. Information provided by Go Ask Alice! is not medical advice and not meant to replace consultation with a health care professional.'
}
export { accordion }
