import React from 'react';
import Collapse from "rc-collapse";

import { accordion } from './accordion.js';
import { Paragraph } from '../../01-atoms/text/paragraph/paragraph.stories';
var Panel = Collapse.Panel;

export const Accordion = ({ accordions = accordion, panelkey = null, activeKey, onKeyChange }) => (
  <div id={`faq${panelkey}`}>
    <Collapse accordion={true} activeKey={activeKey} onChange={(e) => onKeyChange(panelkey)}
      destroyInactivePanel={true} className="accordion-demo">
      <Panel key={panelkey} header={accordions.question} headerClass="accordion__question" className="accordion__wrapper" >
        <div className="accordion__body">
          <Paragraph paragraphContent={accordions.answer} />
        </div>
      </Panel>
    </Collapse>



  </div>
);

export default {
  title: 'Molecules/Accordion',
  component: Accordion,
};

