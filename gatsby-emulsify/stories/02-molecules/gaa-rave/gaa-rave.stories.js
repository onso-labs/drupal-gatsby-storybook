import React from 'react';
import { Paragraph } from "../../01-atoms/text/paragraph/paragraph.stories";
import { gaaRaves } from "./gaa-rave";

export const GAARave = ({ gaaRavesData  }) => (
    <li className={gaaRavesData.field_rave_rant_decision == 'rant' ? "gaa-rave gaa-rave--rant" : "gaa-rave gaa-rave--rave"} >
        <span className="gaa-rave__icon"></span>
        <div className="gaa-rave__content">
            <Paragraph paragraphContent={gaaRavesData.field_testimonial.replace("<p>","<div class='paragraph'>").replace("</p>","</div>")} />
        </div>
    </li >
);

export default {
    title: 'Molecules/GAARave',
    component: GAARave,
};