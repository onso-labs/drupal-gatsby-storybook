import React from 'react';
import { Links } from "../../../01-atoms/link/link.stories";
import { socialShareMenu } from "./social-share-menu.js";
import { Image } from '../../../01-atoms/images/images.stories';

export const SocialShare = ({ socialMenu = socialShareMenu, url = null, mailUrl = null, pdfUrl = null }) => (
    <ul className="social-share-menu">
        {socialMenu.map((item, i) => {
            return (
                ((item.imagealt == "print" && url == null) || (item.imagealt == "pdf" && pdfUrl == null) || (item.imagealt == "email" && mailUrl == null)) ? '' :
                    <>
                        {item.imagealt == "print" || item.imagealt == "email" ?
                            <li className="social-share-menu__item">
                                <Links linkTextValue={<Image imgSrc={item.imagesrc} alt={item.imagealt} />} linkRedirectTo={item.imagealt == "print" ? url : item.imagealt == "email" ? mailUrl : item.url} />
                            </li>
                            :
                            <li className="social-share-menu__item" >
                                {/* <Image imgSrc={item.imagesrc} alt={item.imagealt} /> */}
                                {/* <Links linkTextValue={<Image imgSrc={item.imagesrc} alt={item.imagealt} />} linkRedirectTo={pdfUrl} /> */}
                                <a  className={`link ${ item.imagealt != "pdf" ? 'externalLink' : ''}`}
                                    href={item.imagealt == "pdf" ? pdfUrl : item.imagealt == "facebook" ? 
                                    typeof window !== 'undefined'? "https://www.facebook.com/sharer/sharer.php?u="+window.location.href:'' : typeof window !== 'undefined'? "https://twitter.com/share?url="+window.location.href:''} target="_blank"
                                >
                                    <Image imgSrc={item.imagesrc} alt={item.imagealt} />
                                </a>
                            </li>
                        }</>)
        })}
    </ul>
);

export default {
    title: 'Molecules/Menus/SocialShare',
    component: SocialShare,
};