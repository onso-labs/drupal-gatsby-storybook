import FacebookShare from '../../../../images/icons/fbshare.png';
import TweetShare from '../../../../images/icons/twittershare.png';
import PrintShare from '../../../../images/icons/printshare.png';
import EmailShare from '../../../../images/icons/emailshare.png';
import PDFShare from '../../../../images/icons/pdfshare.png';

const socialShareMenu = [
    {
        imagesrc: FacebookShare,
        imagealt: "facebook",
        url: '#'
    },
    {
        imagesrc: TweetShare,
        imagealt: "tweeter",
        url: '#'
    },
    {
        imagesrc: PrintShare,
        imagealt: "print",
        url: '#'
    },
    {
        imagesrc: EmailShare,
        imagealt: "email",
        url: '#'
    },
    {
        imagesrc: PDFShare,
        imagealt: "pdf",
        url: '#'
    },

]
export { socialShareMenu }


