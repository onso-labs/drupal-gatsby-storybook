import React from 'react';
import { Links } from "../../../01-atoms/link/link.stories";

import { breadcrumb } from "./breadcrumbs.js";

export const Breadcrumbs = ({ crumblabel, Title, Link }) => (
    <React.Fragment>

        {(crumblabel !== '' && crumblabel !== undefined)&&(
             <nav aria-label="system-breadcrumb">
             <h2 className="visually-hidden" id="system-breadcrumb">Breadcrumb</h2>
             <ol className="breadcrumb">
                 <div className="breadcrumb__item">
                     <Links linkClass="breadcrumb__link home" linkRedirectTo="/" linkTextValue="" />
                 </div>
     
                    {(Title !== '' && Title !== undefined)?
                    <React.Fragment>
                     <li className="breadcrumb__item">
                         <Links linkClass="breadcrumb__link" linkRedirectTo={Link} linkTextValue={crumblabel}></Links>
                     </li>
     
                     <li className="breadcrumb__item">
                         <span class="breadcrumb-text">
                             {Title}
                         </span>
                     </li>
                     </React.Fragment>:
                     <React.Fragment>
                         <li className="breadcrumb__item">
                             <span class="breadcrumb-text">
                                 {crumblabel}
                             </span>
                         </li>
                     </React.Fragment>
                    }
               
             </ol>
         </nav>
        )}
    </React.Fragment>
);

export default {
    title: 'Molecules/Menus/Breadcrumbs',
    component: Breadcrumbs,
};