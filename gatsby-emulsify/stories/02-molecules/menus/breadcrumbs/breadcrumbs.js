const breadcrumb = [
    {
        linkRedirectTo: '#',
        linValue: ""
    },
    {
        linkRedirectTo: '#',
        linValue: "Parent Page"
    },
    {
        linkRedirectTo: '',
        linValue: "Current Page"
    }
]
export { breadcrumb }