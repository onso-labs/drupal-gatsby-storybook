import FacebookImg from '../../../../images/icons/fb.svg';
import TwitterImg from '../../../../images/icons/twitters.svg';
import SocialShareImg from '../../../../images/icons/socialshare.svg';

const socialMenu = [
    {
        class: 'facebook-icon link',
        imagelogosrc: FacebookImg,
        imagelogoalt: 'facebook',
        url: "https://www.facebook.com/GoAskAliceColumbia/"
    },
    {
        class: 'twitter-icon link',
        imagelogosrc: TwitterImg,
        imagelogoalt: 'twitter',
        url: "https://twitter.com/AliceatColumbia/"
    },
    {
        class: 'socialshare-icon link',
        imagelogosrc: SocialShareImg,
        imagelogoalt: 'social share',
        //  url: "https://goaskalice.columbia.edu/rss.xml"
        url: "https://master-7rqtwti-whlhb3bjsmuos.us-2.platformsh.site/rss.xml"
    }
]

export { socialMenu };