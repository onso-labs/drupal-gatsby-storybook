import React from 'react';
import { Links } from "../../../01-atoms/link/link.stories";
import { socialMenu } from "./social-menu.js";
import { Image } from '../../../01-atoms/images/images.stories';

export const SocialMenu = ({ socialMenus = socialMenu, navigateQuestionpage }) => (
    <ul className="social-menu">
        {socialMenus.map((item, i) => {
            return (
                <li className="social-menu__item">
                    <a onClick={(e) => navigateQuestionpage(e, item.url)} className={item.class}>
                        {/* <Image imgSrc={item.imagelogosrc} alt={item.imagelogoalt} /> */}
                    </a>
                    {/* <Links linkTextValue={<Image imgSrc={item.imagelogosrc} alt={item.imagelogoalt} />} linkRedirecTo={item.url} /> */}
                </li>
            )
        })}
    </ul>
);

export default {
    title: 'Molecules/Menus/Social',
    component: SocialMenu,
};