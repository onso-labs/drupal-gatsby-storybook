import React from 'react';
import { Links } from "../../../01-atoms/link/link.stories";
import { mainMenu } from "./main-menu.js";
// import jquery from "jquery";

// global.$ = jquery;
// global.jQuery = jquery;
// require("./menu.js");
// import { Image } from '../../../01-atoms/images/images.stories';
export const MainMenu = ({ menus = mainMenu }) => (
    <nav className="header-nav-menu">
        {/* < div id="main-nav" className="main-nav" >
            <ul className="main-menu">
                {menus.map((item) => {
                    return (
                        <li className="main-menu__item main-menu__item--with-sub">
                            <Links linkTextValue={item.title} linkRedirectTo={item.url} /> 
                            <a className="link" href={item.url}> {item.title} </a>
                            <span className="expand-sub"></span>
                            <ul className="main-menu main-menu--sub main-menu--sub-1">
                                {item.below.map((ele) => {
                                    return (
                                        <li className="main-menu__item main-menu__item--sub main-menu__item--sub-1">
                                            <Links linkTextValue={ele.subTitle} linkRedirectTo={ele.url} />
                                        </li>
                                    )
                                })}
                            </ul>
                        </li>
                    )
                })}
            </ul>
        </div> */}
    </nav >
);

export default {
    title: 'Molecules/Menus/Menu',
    component: MainMenu,
};