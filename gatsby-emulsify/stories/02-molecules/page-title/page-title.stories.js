import React from 'react';

export const PageTitle = ({ PageTitle = "Page Title", titleClass = "h1" }) => <div className="page-title"><h1 className={titleClass} dangerouslySetInnerHTML={{ __html: PageTitle }} /></div>;

export default {
  title: 'Molecules/PageTitle',
  component: PageTitle,
};


