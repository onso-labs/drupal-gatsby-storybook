import React from 'react';
import { on_campus, on_campus_list } from './card.js';
import { Links } from "../../01-atoms/link/link.stories";
import { Paragraph } from "../../01-atoms/text/paragraph/paragraph.stories";

export const Card = ({ card = on_campus, cardDetail = on_campus_list }) =>
  (

    <div id={card.entityLabel ? card.entityLabel : card.title}>
      <div className="card__wrapper">
        <div className="card__heading">
          <h2 className="h2">{card.entityLabel ? card.entityLabel : card.title}</h2>
        </div>
        <div className="card__body">
          <div className="row display-order">
            <div className="col-lg-6 col-md-6 col-sm-12">
              <div className="campus-phone">
                <div className="phone-wrap">
                  <h3>Phone</h3>
                  <div>{card.fieldPhone}</div>
                </div>
                <h3 className="h3">Website</h3>
                <Links linkTextValue={card.fieldWebsite !== null ? card.fieldWebsite.title : ''} linkRedirectTo={card.fieldWebsite !== null ? card.fieldWebsite.uri : ''} />

              </div>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-12">
              {/* {cardDetail.map((item) => {
              return ( */}
              <Paragraph paragraphContent={cardDetail !== null ? cardDetail.value.replace("<p>","<div class='paragraph'>").replace("</p>","</div>") : ''} />
              {/* )
            })} */}

            </div>
          </div>
        </div>
      </div>
    </div>
  );

export default {
  title: 'Molecules/Cards',
  component: Card,
};
