import React from 'react';
import { Paragraph } from "../../01-atoms/text/paragraph/paragraph.stories";
import { awardItem } from "./award-item";
import { Image } from '../../01-atoms/images/images.stories';
export const AwardItem = ({ awardItemData = awardItem }) => (
    <li class="award-item">
        <Image imgSrc={awardItemData.awardimgsrc} alt={awardItemData.awardimgalt} />
        <Paragraph paragraphContent={awardItemData.awarddetail} />
    </li>
);

export default {
    title: 'Molecules/AwardItem',
    component: AwardItem,
};