import placeholder from '../../../images/icons/badge-placeholder.png';

const awardItem =
{
    awardimgsrc: placeholder,
    awardimgalt: 'Healthline std_BADGE 2017',
    awarddetail: 'Awards'
}

export { awardItem }