/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"

import { Header } from "../03-organisms/site/site-header/site-header.stories"
import { SiteUpperHeader } from "../03-organisms/site/site-upper-header/site-upper-header.stories"
import { Footer } from "../03-organisms/site/site-footer/site-footer.stories"
import { MainMenu } from '../02-molecules/menus/main-menu/main-menu.stories'
import { Breadcrumbs } from "../02-molecules/menus/breadcrumbs/breadcrumbs.stories"
// import "./layout.css"

export const Layout = ({ content, sidebar, pageTitle, newQA, crumbLabel, title, link }) => {

  return (

    <>
      <SiteUpperHeader />
      <div className="container">
        <Header />
        <MainMenu />
        <Breadcrumbs crumblabel={crumbLabel} Title={title} Link={link} />
        <div className="main-layout">
          {newQA}
          <div className="page-wrapper">
            {/* <PageTitle PageTitle={pageTitle} /> */}
            {pageTitle}
            <div className="row">
              <div className="col-sm-12 col-md-8">
                {content}
              </div>
              <div className="col-sm-12 col-md-4">
                {sidebar}
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

// export default Layout
export default {
  title: 'Templates/Layout',
  component: Layout,
};
