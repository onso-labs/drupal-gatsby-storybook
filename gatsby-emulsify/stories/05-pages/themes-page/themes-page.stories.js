import React from 'react'
import { blockTitle, linkList, Title } from './themes-page';
import { CTA } from '../../02-molecules/cta/cta.stories';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';
import { ThemeWeekLandingContent } from '../../03-organisms/theme-week-landing-content/theme-week-landing-content.stories';
import { SecondaryMenu } from '../../03-organisms/secondary-menu/secondary-menu.stories';
import { Layout } from '../../04-templates/layout.stories';

export const ThemePage = ({ title = blockTitle, list = linkList, titleData = Title }) => (

  <Layout
    crumbLabel="Themes of the Week"
    pageTitle={
      <PageTitle PageTitle={titleData.pageTitle} />
    }
    sidebar={
      <>
        <CTA />
        <SecondaryMenu />
        <SecondaryMenu title={blockTitle} list={linkList} />
      </>
    }

    content={
      <>
        <ThemeWeekLandingContent />
      </>
    }

  >


  </Layout >
);

export default {
  title: 'Pages/ThemePage',
  component: ThemePage,
};