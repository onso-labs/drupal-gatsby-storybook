import React from 'react'
import { CTA } from '../../02-molecules/cta/cta.stories';
import { SecondaryMenu } from '../../03-organisms/secondary-menu/secondary-menu.stories';
import { ListContentBlock } from '../../03-organisms/list-content-block/list-content-block.stories';
import { linkList, blockTitle, Title } from './factsheet-page';
import { Layout } from '../../04-templates/layout.stories';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';
export const FactSheetPage = ({ fields, titleData = Title }) => (

  <Layout
    crumbLabel="Fact Sheets"
    link="/fact-sheets"
    title={data.title}
    pageTitle={
      <PageTitle PageTitle={titleData.pageTitle} />
    }
    sidebar={
      <>
        <CTA />
        <SecondaryMenu />
        <SecondaryMenu title={blockTitle} list={linkList} />
      </>
    }
    content={
      <>
        <ListContentBlock searchlist={fields} />
      </>
    } >
    <PageTitle PageTitle={titleData.pageTitle} />
  </Layout >
);

export default {
  title: 'Pages/FactSheetPage',
  component: FactSheetPage,
};