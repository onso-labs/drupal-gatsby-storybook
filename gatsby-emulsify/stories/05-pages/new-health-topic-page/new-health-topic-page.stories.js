import React from 'react'
import { CTA } from '../../02-molecules/cta/cta.stories';
import { SecondaryMenu } from '../../03-organisms/secondary-menu/secondary-menu.stories';
import { NewHealthTopics } from '../../03-organisms/new-health-topics/new-health-topics.stories';
import { linkList, blockTitle, Title } from './new-health-topic-page';
import { Layout } from '../../04-templates/layout.stories';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';

export const NewHealthTopicPage = ({ data, titleData = Title }) => (

  <Layout
    crumbLabel="Health Answers"

    pageTitle={
      <PageTitle PageTitle={titleData.pageTitle} />
    }
    sidebar={
      <>
        <CTA />
        <SecondaryMenu title={blockTitle} list={linkList} />
      </>
    }
    content={
      <>

        <NewHealthTopics newQAs={data} />
      </>
    } >
  </Layout>
);

export default {
  title: 'Pages/NewHealthTopicPage',
  component: NewHealthTopicPage,
};