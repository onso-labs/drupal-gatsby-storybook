import React from 'react'
import { CTA } from '../../02-molecules/cta/cta.stories';
import { QuizzesBlock } from '../../03-organisms/quizzes-block/quizzes-block.stories';
import { Layout } from '../../04-templates/layout.stories';
import { Title } from './quizzes-page';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';
import { SocialShare } from '../../02-molecules/menus/socialshare/social-share-menu.stories';
import { ListContentBlock } from '../../03-organisms/list-content-block/list-content-block.stories';
export const quizzesPage = ({ titleData = Title }) => (
  <div className="quizzes-page">
    <Layout
      pageTitle={
        <PageTitle PageTitle={titleData.pageTitle} />
      }
      sidebar={
        <>
          <CTA />
        </>
      }
      content={
        <>
          <QuizzesBlock />
          <SocialShare />
          <ListContentBlock />
        </>
      } >
    </Layout>
  </div>
);

export default {
  title: 'Pages/quizzesPage',
  component: quizzesPage,
};
