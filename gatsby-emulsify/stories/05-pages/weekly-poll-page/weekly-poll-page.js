import FacebookShare from '../../../images/icons/fbshare.png';
import TweetShare from '../../../images/icons/twittershare.png';
import PrintShare from '../../../images/icons/printshare.png';
import EmailShare from '../../../images/icons/emailshare.png';
import PDFShare from '../../../images/icons/pdfshare.png';

const linkList = [
  {
    "entity": {
      "entityLabel": "Get Alice! In Your Box",
      "entityUrl": {
        "routed": true,
        "path": "#"
      }
    }
  }
]
const blockTitle = "Subscribe to Alice!"
const Title = {
  pageTitle: "Polls"
}
const weeklyPoll = {
  weeklypollReadmore: '',
  weeklypollReadmoreUrl: '',
  weeklypollHeaderTitle: 'Its almost time for finals! What is your essential study tool?',
  weeklypollHeadertitleUrl: '#',
  weeklypollTitle: '',
  buttonContent: 'vote'
}
const weeklyData = [
  {
    resultData: [
      {
        paragraphContent: "Highlighters",
        votePercentage: '30',
        voteNumbers: '21',
      },
      {
        paragraphContent: "Notecards",
        votePercentage: '20',
        voteNumbers: '11',
      },
      {
        paragraphContent: "Sticky notes",
        votePercentage: '60',
        voteNumbers: '17',
      },
      {
        paragraphContent: "Class outlines",
        votePercentage: '70',
        voteNumbers: '32',
      },
      {
        paragraphContent: "Other",
        votePercentage: '3',
        voteNumbers: '14',
      }
    ],
    socialShareMenu: [
      {
        imagesrc: FacebookShare,
        imagealt: "Home",
        url: '#'
      },
      {
        imagesrc: TweetShare,
        imagealt: "Home",
        url: '#'
      },
      {
        imagesrc: PrintShare,
        imagealt: "Home",
        url: '#'
      },
      {
        imagesrc: EmailShare,
        imagealt: "Home",
        url: '#'
      },
      {
        imagesrc: PDFShare,
        imagealt: "Home",
        url: '#'
      },

    ]
  },
  {
    resultData: [
      {
        paragraphContent: "Highlighters",
        votePercentage: '30',
        voteNumbers: '21',
      },
      {
        paragraphContent: "Notecards",
        votePercentage: '20',
        voteNumbers: '11',
      },
      {
        paragraphContent: "Sticky notes",
        votePercentage: '60',
        voteNumbers: '17',
      },
      {
        paragraphContent: "Class outlines",
        votePercentage: '70',
        voteNumbers: '32',
      },
      {
        paragraphContent: "Other",
        votePercentage: '3',
        voteNumbers: '14',
      }
    ],
    socialShareMenu: [
      {
        imagesrc: FacebookShare,
        imagealt: "Home",
        url: '#'
      },
      {
        imagesrc: TweetShare,
        imagealt: "Home",
        url: '#'
      },
      {
        imagesrc: PrintShare,
        imagealt: "Home",
        url: '#'
      },
      {
        imagesrc: EmailShare,
        imagealt: "Home",
        url: '#'
      },
      {
        imagesrc: PDFShare,
        imagealt: "Home",
        url: '#'
      },

    ]
  }
]

export { Title, blockTitle, linkList, weeklyPoll, weeklyData }
