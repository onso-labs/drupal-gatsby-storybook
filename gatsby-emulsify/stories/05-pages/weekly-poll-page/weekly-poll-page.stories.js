import React from 'react'
import { CTA } from '../../02-molecules/cta/cta.stories';
import { SecondaryMenu } from '../../03-organisms/secondary-menu/secondary-menu.stories';
import { MorePolls } from '../../03-organisms/more-polls/more-polls.stories';
import { WeeklyPoll } from '../../03-organisms/weekly-poll/weekly-poll.stories';
import { WeeklyPollResult } from '../../03-organisms/weekly-poll-result/weekly-poll-result.stories';
import { SocialShare } from '../../02-molecules/menus/socialshare/social-share-menu.stories';
import { Layout } from '../../04-templates/layout.stories';
import { Title, blockTitle, linkList, weeklyPoll, weeklyData } from './weekly-poll-page';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';

export const weeklyPollPage = ({ titleData = Title, weeklyDatas = weeklyData }) => (

  <Layout
    pageTitle={
      <PageTitle PageTitle={titleData.pageTitle} />
    }
    sidebar={
      <>
        <CTA />
        <SecondaryMenu title={blockTitle} list={linkList} />

      </>
    }
    content={
      <>
        <WeeklyPoll weeklyPolls={weeklyPoll} />
        {weeklyDatas.map((item, i) => {
          return (
            <div>
              <WeeklyPollResult resultDatas={item.resultData} />
              <SocialShare socialMenu={item.socialShareMenu} />
            </div>
          )
        })}

        <MorePolls />

      </>
    } >
  </Layout>
);

export default {
  title: 'Pages/weeklyPollPage',
  component: weeklyPollPage,
};
