import React from 'react'
import { CTA } from '../../02-molecules/cta/cta.stories';
import { SecondaryMenu } from '../../03-organisms/secondary-menu/secondary-menu.stories';
import { QuestionBlock } from '../../02-molecules/question-block/question-block.stories';
import { AnswerBlock } from '../../02-molecules/answer-block/answer-block.stories';
import { linkList, blockTitle, Content, Title } from './health-topic-detail-page';
import { Layout } from '../../04-templates/layout.stories';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';

export const HealthTopicDetailPage = ({ data = Content, titleData = Title }) => (

  <Layout
    crumbLabel="Health Answers"
    link="/health-answers/updates"
    pageTitle={
      <PageTitle PageTitle={titleData.pageTitle} />
    }
    sidebar={
      <>
        <CTA />
        <SecondaryMenu title={blockTitle} list={linkList} />
      </>
    }
    content={
      <>
        <QuestionBlock themeQuestions={data.fieldQuestion} />
        <AnswerBlock themeAnswers={data.fieldAnswer} />

      </>
    } >
  </Layout>
);

export default {
  title: 'Pages/HealthTopicDetailPage',
  component: HealthTopicDetailPage,
};