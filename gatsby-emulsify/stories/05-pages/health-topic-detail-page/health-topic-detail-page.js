const linkList = [
  {
    "entity": {
      "entityLabel": "Get Alice! In Your Box",
      "entityUrl": {
        "routed": true,
        "path": "#"
      }
    }
  }
]
const blockTitle = "Subscribe to Alice!"
const Content = {
  "title": "Marijuana: How long does it hang out in the body?",
  "fieldQuestion": {
    "value": "<p>Question Pub. Cont-2</p>\r\n",
    "format": "basic_html",
    "processed": "<p>Question Pub. Cont-2</p>\n"
  },
  "fieldAnswer": {
    "value": "<p>Answer Pub. Cont-2</p>\r\n",
    "format": "basic_html",
    "processed": "<p>Answer Pub. Cont-2</p>\n",
    "summary": "",
    "summaryProcessed": ""
  },
}
const Title = {
  pageTitle: "Snorting heroin — Dosage?"
}

export { linkList, blockTitle, Content, Title }