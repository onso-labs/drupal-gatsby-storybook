import React from 'react'
import { blockTitle, linkList, Title } from './history-page';
import { CTA } from '../../02-molecules/cta/cta.stories';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';
import { GAAHistory } from '../../03-organisms/gaa-history/gaa-history.stories';
import { SecondaryMenu } from '../../03-organisms/secondary-menu/secondary-menu.stories';
import { Layout } from '../../04-templates/layout.stories';

export const HistoryPage = ({ title = blockTitle, list = linkList, titleData = Title }) => (

  <Layout
    crumbLabel="About Alice!"
    link="/about-alice/all-about"
    title="Go Ask Alice! History"
    pageTitle={
      <PageTitle PageTitle={titleData.pageTitle} />
    }
    sidebar={
      <>
        <CTA />
        <SecondaryMenu />
        <SecondaryMenu title={blockTitle} list={linkList} />
      </>
    }

    content={
      <>
        <GAAHistory />
      </>
    }

  >


  </Layout >
);

export default {
  title: 'Pages/HistoryPage',
  component: HistoryPage,
};