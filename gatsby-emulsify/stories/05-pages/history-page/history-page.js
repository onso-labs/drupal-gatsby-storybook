const linkList = [
  {
    "entity": {
      "entityLabel": "Get Alice! In Your Box",
      "entityUrl": {
        "routed": true,
        "path": "#"
      }
    }
  }
]
const blockTitle = "Subscribe to Alice!"

const Title = {
  pageTitle: "Go Ask Alice! History"
}

export { linkList, blockTitle, Title }