import React from 'react'
import { CTA } from '../../02-molecules/cta/cta.stories';
import { Layout } from '../../04-templates/layout.stories';
import { Title } from './accessibility-form';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';
import { CustomForm } from '../../03-organisms/custom-form/custom-form.stories';
import { SearchForm } from '../../03-organisms/search-form/search-form.stories';
import { EmailForm } from '../../03-organisms/email-form/email-form.stories';
import { AskYourQuestionTop } from '../../03-organisms/ask-your-question-top/ask-your-question-top.stories';
import { AffiliationForm } from '../../03-organisms/affiliation-form/affiliation-form.stories';

export const AccessibilityFormPage = ({ titleData = Title }) => (
  <div className="all-about-alice-page">
    <Layout
      pageTitle={
        <PageTitle PageTitle={titleData.pageTitle} />
      }
      sidebar={
        <>
          <CTA />
        </>
      }
      content={
        <>
          <AskYourQuestionTop />
          <CustomForm />
          <SearchForm />
          <EmailForm />
          <AffiliationForm />

        </>
      } >
    </Layout>
  </div>

)


export default {
  title: 'Pages/AccessibilityForm',
  component: AccessibilityFormPage,
};