import React from 'react'
import { CTA } from '../../02-molecules/cta/cta.stories';
import { SecondaryMenu } from '../../03-organisms/secondary-menu/secondary-menu.stories';
import { QuestionBlock } from '../../02-molecules/question-block/question-block.stories';
import { AnswerBlock } from '../../02-molecules/answer-block/answer-block.stories';
import { linkList, blockTitle, Title } from './theme-detail-page';
import { Layout } from '../../04-templates/layout.stories';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';

export const ThemeDetailPage = ({ titleData = Title }) => (

  <Layout
    pageTitle={
      <PageTitle PageTitle={titleData.pageTitle} />
    }
    sidebar={
      <>
        <CTA />
        <SecondaryMenu title={blockTitle} list={linkList} />
      </>
    }
    content={
      <>
        <QuestionBlock />
        <AnswerBlock />

      </>
    } >
  </Layout>
);

export default {
  title: 'Pages/ThemeDetailPage',
  component: ThemeDetailPage,
};