const linkList = [
  {
    "entity": {
      "entityLabel": "Get Alice! In Your Box",
      "entityUrl": {
        "routed": true,
        "path": "#"
      }
    }
  }
]
const blockTitle = "Subscribe to Alice!"
const Title = {
  pageTitle: "Planting Seeds of Knowledge"
}
export { linkList, blockTitle, Title }