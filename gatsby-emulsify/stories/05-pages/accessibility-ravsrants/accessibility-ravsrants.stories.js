import React from 'react'
import { CTA } from '../../02-molecules/cta/cta.stories';
import { Layout } from '../../04-templates/layout.stories';
import { GAARaveRants } from '../../03-organisms/gaa-raves-rants/gaa-raves-rants.stories';
import { Title } from './accessibility-ravsrants';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';

export const AccessibilityRavRantsPage = ({ titleData = Title }) => (
  <div className="all-about-alice-page">
    <Layout
      pageTitle={
        <PageTitle PageTitle={titleData.pageTitle} />
      }
      sidebar={
        <>
          <CTA />
        </>
      }
      content={
        <>
          <GAARaveRants />

        </>
      } >
    </Layout>
  </div>

)


export default {
  title: 'Pages/AccessibilityRavRants',
  component: AccessibilityRavRantsPage,
};