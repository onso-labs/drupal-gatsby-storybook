import React from 'react'
import { CTA } from '../../02-molecules/cta/cta.stories';
import { HealthResources } from '../../03-organisms/health-resources/health-resources.stories';

import { Layout } from '../../04-templates/layout.stories';
// import { GAARaveRants } from '../../03-organisms/gaa-raves-rants/gaa-raves-rants.stories';
import { Title } from './accessibility-resources';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';

export const AccessibilityResourcePage = ({ titleData = Title }) => (
  <div className="all-about-alice-page">
    <Layout
      pageTitle={
        <PageTitle PageTitle={titleData.pageTitle} />
      }
      sidebar={
        <>
          <CTA />
        </>
      }
      content={
        <>
          <HealthResources />

          {/* <GAARaveRants /> */}


        </>
      } >
    </Layout>
  </div>

)


export default {
  title: 'Pages/AccessibilityResource',
  component: AccessibilityResourcePage,
};