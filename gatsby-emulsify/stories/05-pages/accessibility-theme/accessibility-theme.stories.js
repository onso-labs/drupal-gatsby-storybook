import React from 'react'
import { CTA } from '../../02-molecules/cta/cta.stories';
import { Layout } from '../../04-templates/layout.stories';
import { Title } from './accessibility-theme';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';
import { ThemeWeekLandingContent } from '../../03-organisms/theme-week-landing-content/theme-week-landing-content.stories';
import { QuestionBlock } from '../../02-molecules/question-block/question-block.stories';
import { AnswerBlock } from '../../02-molecules/answer-block/answer-block.stories';
import { ThemeWeekHero } from '../../03-organisms/theme-week-hero/theme-week-hero.stories';

export const AccessibilityThemePage = ({ titleData = Title }) => (
  <div className="all-about-alice-page">
    <Layout
      pageTitle={
        <PageTitle PageTitle={titleData.pageTitle} />
      }
      sidebar={
        <>
          <CTA />
        </>
      }
      content={
        <>
          <ThemeWeekLandingContent />
          <QuestionBlock />
          <AnswerBlock />
          <ThemeWeekHero />

        </>
      } >
    </Layout>
  </div>

)


export default {
  title: 'Pages/AccessibilityTheme',
  component: AccessibilityThemePage,
};