import React from 'react'
import { CTA } from '../../02-molecules/cta/cta.stories';
import { Layout } from '../../04-templates/layout.stories';
import { Accordion } from '../../02-molecules/accordion/accordion.stories';
// import { WeeklyPoll } from '../../03-organisms/weekly-poll/weekly-poll.stories';
import { Title } from './accessibility-faq';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';
import { SocialShare } from '../../02-molecules/menus/socialshare/social-share-menu.stories';
import { AllAboutAlice } from '../../02-molecules/all-about-alice/all-about-alice.stories';
import { AboutAliceStats } from '../../03-organisms/about-alice-stats/about-alice-stats.stories';

export const AccessibilityFAQPage = ({ titleData = Title }) => (
  <div className="all-about-alice-page">
    <Layout
      pageTitle={
        <PageTitle PageTitle={titleData.pageTitle} />
      }
      sidebar={
        <>
          <CTA />
        </>
      }
      content={
        <>
          <Accordion />
          <SocialShare />
          <AboutAliceStats />
          {/* <AllAboutAlice /> */}

        </>
      } >
    </Layout>
  </div>

)


export default {
  title: 'Pages/AccessibilityFAQ',
  component: AccessibilityFAQPage,
};