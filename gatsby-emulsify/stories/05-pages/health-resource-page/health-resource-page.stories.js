import React from 'react'
import { CTA } from '../../02-molecules/cta/cta.stories';
import { HealthResources } from '../../03-organisms/health-resources/health-resources.stories';
import { resourceData, Title } from './health-resource-page';
import { Layout } from '../../04-templates/layout.stories';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';
import { SocialShare } from '../../02-molecules/menus/socialshare/social-share-menu.stories';

export const HealthResourcesPage = ({ data = resourceData, titleData = Title }) => (

  <Layout
    crumbLabel={titleData.pageTitle}

    pageTitle={
      <PageTitle PageTitle={titleData.pageTitle} />
    }
    sidebar={
      <>
        <CTA />
      </>
    }
    content={
      <>
        <HealthResources cardCampusResources={data} />
        <div className="social-menus">
          <SocialShare />
        </div>
      </>
    } >
  </Layout>
);

export default {
  title: 'Pages/HealthResourcesPage',
  component: HealthResourcesPage,
};