const linkList = [
  {
    list: "Get Alice! In Your Box"
  }
]
const blockTitle = {
  title: "Subscribe to Alice!"
}

const resourceData = [
  {
    title: "red University general health Resource",
    phone_title: "Phone",
    fieldPhone: "(555) 555-2222",
    website_title: "Website",
    fieldWebsite: {
      title: "General service Resource new",
      uri: "#"
    },
    body: {
      value: "Replace this text with a description of this service."
    }
  },
  {
    title: "Blue University general health Resource",
    phone_title: "Phone",
    fieldPhone: "(444) 444-3333",
    website_title: "Website",
    fieldWebsite: {
      title: "General service Resource new",
      uri: "#"
    },
    body: {
      value: "Replace this text with a description of this service."
    }
  },
  {

    title: "yellow University general health Resource",
    phone_title: "Phone",
    fieldPhone: "(444) 444-3333",
    website_title: "Website",
    fieldWebsite: {
      title: "General service Resource new",
      uri: "#"
    },
    body: {
      value: "Replace this text with a description of this service."
    }
  }
]
const Title = {
  pageTitle: "On-campus Resources"
}
export { linkList, blockTitle, resourceData, Title }