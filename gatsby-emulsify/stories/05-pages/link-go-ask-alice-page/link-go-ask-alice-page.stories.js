import React from 'react'
import { CTA } from '../../02-molecules/cta/cta.stories';
import { CustomForm } from '../../03-organisms/custom-form/custom-form.stories';
import { Layout } from '../../04-templates/layout.stories';
import { Title, pageContent, linkList, blockTitle } from './link-go-ask-alice-page';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';
import { SocialShare } from '../../02-molecules/menus/socialshare/social-share-menu.stories';
export const linkGoAskAlicePage = ({ titleData = Title }) => (
  <Layout
    pageTitle={
      <PageTitle PageTitle={titleData.pageTitle} />
    }
    sidebar={
      <>
        <CTA />
      </>
    }
    content={
      <>
        <div class="page-content">
          <div dangerouslySetInnerHTML={{ __html: pageContent.contentData }} />
        </div>
        <SocialShare />
      </>
    } >
  </Layout>
);

export default {
  title: 'Pages/linkGoAskAlicePage',
  component: linkGoAskAlicePage,
};
