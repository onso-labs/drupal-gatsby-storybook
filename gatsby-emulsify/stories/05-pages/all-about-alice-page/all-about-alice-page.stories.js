import React from 'react'
import { CTA } from '../../02-molecules/cta/cta.stories';
import { Layout } from '../../04-templates/layout.stories';
import { Title, paragraphContent, aboutAlice, linkList, blockTitle } from './all-about-alice-page';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';
import { AboutAliceStats } from '../../03-organisms/about-alice-stats/about-alice-stats.stories';
import { AllAboutAlice } from '../../02-molecules/all-about-alice/all-about-alice.stories';
import { Testimonial } from '../../03-organisms/testimonial/testimonial.stories';
import { SocialShare } from '../../02-molecules/menus/socialshare/social-share-menu.stories';
import { AwardBlock } from '../../03-organisms/award-block/award-block.stories';

export const AllAboutAlicePage = ({ titleData = Title, paragraphValue = paragraphContent, aboutData = aboutAlice }) => (
  <div className="all-about-alice-page">
    <Layout
      pageTitle={
        <PageTitle PageTitle={titleData.pageTitle} />
      }
      sidebar={
        <>
          <CTA />
        </>
      }
      content={
        <>
          <div className="row">
            <div className="col-lg-5 display-order">
              <AboutAliceStats />
            </div>
            <div className="col-lg-7">
              <AllAboutAlice aboutAlice={aboutData} />
            </div>
          </div>
          <Testimonial />
          <div className="allabout-description">
            <div dangerouslySetInnerHTML={{ __html: paragraphValue.paragraph }} />
          </div>
          <AwardBlock />
          <SocialShare />
        </>
      } >
    </Layout>
  </div>

)


export default {
  title: 'Pages/AllAboutAlicePage',
  component: AllAboutAlicePage,
};