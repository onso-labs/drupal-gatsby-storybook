
const Title = {
  pageTitle: "All About Alice!"
}
const aboutAlice = {
  linkAboutAlice: '',
  linkAboutAliceUrl: '',
  linkMoreAlice: '',
  linkMoreUrl: '',
  paragraphContent: '<p><b>Who is Alice!?</b>,</p><p>Alice! is not one person, but a team. The Go Ask Alice! site is supported by a team of Columbia University health promotion specialists, health care providers, and other health professionals, along with a staff of information and research specialists and writers. Our team members have advanced degrees in public health, health education, medicine, counseling, and a number of other relevant fields.</p>'

}
const paragraphContent = {
  paragraph: "<h3 class='description-header'>Go Ask Alice!, According to the Research</h3><p> A number of independent studies have been conducted over the years that included Go Ask Alice! Here are few examples of what researchers had to say about our work:</p><ul><li>A study from the University of Pittsburgh presented at the 2011 iConference and published in Online Information Review noted that <b>59% of the hyperlinks to health information from blogs and other sites reviewed were linked to Go Ask Alice!</b></li> <li>A Stanford University study published in January/February 2010 Knowledge Quest: Journal of the American Association of School Librarians lists <b>Go Ask Alice! first among websites for reputable and credible reproductive health information</b> for adolescents on the web.</li> <li>In a 2009 chapter published in Pediatric Informatics, <b>Go Ask Alice! is listed as a recommended website for adolescent health.</b></li> <li>Information from Go Ask Alice! is cited in a 2007 article in the British Medical Journal discussing hookah use.</li> <li><b>Go Ask Alice! is described as 'one of the most useful sites in a health educator's book of tricks,'</b> in a 2007 article published in the journal Health Promotion Practice.</li> <li>A University of Michigan study published in the December 2000 Health Education & Behavior journal <b>names Go Ask Alice! number one for access to specific sexual health information on the Internet.</b></li></ul> <p>Additionally, Go Ask Alice! has been included in academic research including doctoral dissertations and many other student research projects.</p>"
}
const linkList = [
  {
    "entity": {
      "entityLabel": "Get Alice! In Your Box",
      "entityUrl": {
        "routed": true,
        "path": "#"
      }
    }
  }
]
const blockTitle = "Subscribe to Alice!"
export { Title, paragraphContent, aboutAlice, linkList, blockTitle }
