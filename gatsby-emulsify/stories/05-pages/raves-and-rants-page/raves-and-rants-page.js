
const Title = {
  pageTitle: "Raves & Rants"
}
const ravesDetail = {
  paragraph: "<p>Dear Readers,</p> <p><i>Go Ask Alice!</i> would not be what it is had it not been for all of you, its readers. Without you, no questions would be asked, no Q&As would be browsed and read, no feedback/comments would be submitted, no site would remain in publication. In recognition of Go Ask Alice!'s readership, which helps keep the site going and continue to serve as a health information resource, it's only appropriate to share some kudos and criticisms from readers. Thank you, Alice!</p>"
}
export { Title, ravesDetail }