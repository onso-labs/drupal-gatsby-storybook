import React from 'react'
import { ravesDetail, Title } from './raves-and-rants-page';
import { CTA } from '../../02-molecules/cta/cta.stories';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';
import { GAARaveRants } from '../../03-organisms/gaa-raves-rants/gaa-raves-rants.stories';
import { Layout } from '../../04-templates/layout.stories';
import { Paragraph } from '../../01-atoms/text/paragraph/paragraph.stories';
export const RaveAndRantsPage = ({ raveData = ravesDetail, titleData = Title }) => (

  <Layout
    pageTitle={
      <PageTitle PageTitle={titleData.pageTitle} />
    }
    sidebar={
      <>
        <CTA />

      </>
    }

    content={
      <>
        <Paragraph paragraphContent={raveData.paragraph} />

        <GAARaveRants />
      </>
    }

  >

  </Layout >
);

export default {
  title: 'Pages/RaveAndRantsPage',
  component: RaveAndRantsPage,
};