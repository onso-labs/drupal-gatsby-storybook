import React from 'react'
import { CTA } from '../../02-molecules/cta/cta.stories';
import { SecondaryMenu } from '../../03-organisms/secondary-menu/secondary-menu.stories';
// import { FactDetail } from '../../03-organisms/FactDetail/fact-detail.stories';
import { Title, pageContent } from './factsheet-detail-page';
import { Card } from '../../02-molecules/card/card.stories';
import { Layout } from '../../04-templates/layout.stories';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';
import { SocialShare } from '../../02-molecules/menus/socialshare/social-share-menu.stories';

export const FactSheetDetailPage = ({ fields, titleData = Title }) => (

  <Layout
    pageTitle={
      <PageTitle PageTitle={titleData.pageTitle} />
    }
    sidebar={
      <>
        <CTA />
        <SecondaryMenu />
      </>
    }
    content={
      <>
        <div class="page-content">
          <div dangerouslySetInnerHTML={{ __html: pageContent.contentData }} />
        </div>
        <Card />
        <div className="social-menus">
          <SocialShare />
        </div>

      </>
    } >
  </Layout>

);

export default {
  title: 'Pages/FactSheetDetailPage',
  component: FactSheetDetailPage,
};