const linkList = [
  {
    "entity": {
      "entityLabel": "Get Alice! In Your Box",
      "entityUrl": {
        "routed": true,
        "path": "#"
      }
    }
  }
]
const blockTitle = "Subscribe to Alice!"
const Title = {
  pageTitle: "Alcohol: Lowering Risk"
}

const pageContent = {
  contentData: "<p>Alcohol can be a way to enhance social experiences, especially when consumed in a way that reduces the risk of negative health effects. Most Columbia students who choose to drink practice lower-risk drinking behaviors. Lower-risk drinking helps you maximize the fun and minimize the potential for harm that can occur due to alcohol intoxication (e.g., acute dehydration, legal troubles, etc.). Below are some ways you can reduce risk:</p><h2><strong>Lower-risk drinking is:</strong></h2>    <ul><li>Determining the number of drinks you plan to consume before you start to drink.</li><li>Using counting strategies such as tallying drinks on your phone, or putting bottle caps in your pockets.</li> <li>Consuming no more than four drinks for women / five drinks for men in one sitting.</li><li>Hydrating and eating before and while you are drinking.</li> <li>Drinking at a pace of no more than one standard drink per hour. A standard drink = 12 oz. for beer, 5 oz. for wine, and 1.5 oz. for liquor.</li>  <li>Alternating alcohol-free drinks and drinks containing alcohol. If you want an alcohol look-alike, consider “dressing up” your water by ordering a seltzer with lime, or asking for a non-alcoholic beer or daiquiri.</li>  <li>Avoiding drinking games; or, if you choose to play, setting a limit for how long you’ll take part or how much you’ll drink.</li> <li>Avoiding shots.</li> <li>Stopping drinking at a pre-determined time.</li> <li>Making an exit plan before going out. How will you get home?</li> <li>Asking friends for help sticking to your drinking “rules”.</li><li>Knowing what is in your drink.</li><li>Never leaving a drink unattended.</li></ul><h2><strong>Recognizing risk:</strong></h2><p>Occasionally a person may consume more than planned. In some cases, this can result in alcohol poisoning.&nbsp; If you can recognize the signs, you can help keep yourself and your friends safe. Alcohol depresses the nervous system. At high levels of consumption this can impact a person’s ability to breathe and the gag reflex, which prevents choking. Even after someone stops drinking, alcohol is in the system and can be fatal. If you see someone experiencing the following symptoms, call for help immediately:</p><ul><li>Semi-consciousness or unconsciousness and cannot be awakened</li>	<li>Cold, clammy, pale, or bluish skin</li><li>Slowed breathing (fewer than eight breaths per minute)</li><li>Irregular breathing (10 seconds or more between breaths)</li><li>Vomiting while 'sleeping' or passed out, and not waking up after vomiting</li></ul><p>If a person has any of these symptoms, don’t just let them “sleep it off,” get emergency help immediately and do not leave the person alone. While waiting for help to arrive, turn the person on her/his side to prevent choking in case of vomiting.</p><p>Last reviewed/updated: July 30, 2015</p>"
}

export { linkList, blockTitle, Title, pageContent }