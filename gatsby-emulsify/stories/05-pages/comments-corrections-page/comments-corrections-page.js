
const Title = {
  pageTitle: "Comments & Corrections"
}
const pageContent = {
  contentData: "<p><i>Go Ask Alice!</i> and Columbia University welcome opportunities to work with non-profit institutions, websites, media outlets, and other companies and organizations to increase access to accurate, reliable, and culturally competent health information.</p><p>Collaborations and use of <i>Go Ask Alice!</i> content can take many forms. If you would like more information on linking to the site, licensing Q&A for reproduction (in whole or part), getting a feed of <i>Go Ask Alice!</i> content on your site, or licensing the full site, please use the respective forms on the Syndication & Licensing section of the site. To inquire about all other forms of collaboration please submit your request via the form below.</p><p>** Please note that health questions submitted via this form will not be considered for answering and publication on the site. Please submit all health questions via the <a>Ask Alice!</a> page.</p>"
}

const linkList = [
  {
    "entity": {
      "entityLabel": "Get Alice! In Your Box",
      "entityUrl": {
        "routed": true,
        "path": "#"
      }
    }
  }
]
const blockTitle = "Subscribe to Alice!"
export { Title, pageContent, linkList, blockTitle }
