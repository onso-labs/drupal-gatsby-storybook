import React from 'react'
import { ThemeWeekHero } from '../../03-organisms/theme-week-hero/theme-week-hero.stories';
import { Layout } from '../../04-templates/layout.stories';
import { WeeklyPoll } from '../../03-organisms/weekly-poll/weekly-poll.stories';
import { Quizzes } from '../../03-organisms/quizzes/quizzes.stories';
import { QuestionBlock } from '../../02-molecules/question-block/question-block.stories';
import { Testimonial } from '../../03-organisms/testimonial/testimonial.stories';
import { AllAboutAlice } from '../../02-molecules/all-about-alice/all-about-alice.stories';
import { NewHealthTopicSection } from '../../03-organisms/new-health-topic-section/new-health-topic-section.stories';

// import { PageTitle } from '../../02-molecules/page-title/page-title.stories';

export const HomePage = ({ }) => (

  <Layout
    newQA={
      <NewHealthTopicSection />
    }
    sidebar={
      <>
        <WeeklyPoll />
        <Quizzes />

      </>
    }
    content={
      <>
        <QuestionBlock />
        <AllAboutAlice />
        <Testimonial />
        <ThemeWeekHero />

      </>
    } >
  </Layout>
);

export default {
  title: 'Pages/HomePage',
  component: HomePage,
};