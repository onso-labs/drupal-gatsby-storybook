import React from 'react'
import { blockTitle, linkList, Title } from './health-topic-landing-page';
import { CTA } from '../../02-molecules/cta/cta.stories';
import { CategoryListContent } from '../../03-organisms/category-list-content/category-list-content.stories';
import { SecondaryMenu } from '../../03-organisms/secondary-menu/secondary-menu.stories';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';
import { Layout } from '../../04-templates/layout.stories';


export const HealthTopicLandingPage = ({ data, titleData = Title }) => (
  <Layout
    crumbLabel="Health Answers"
    pageTitle={
      <PageTitle PageTitle={titleData.pageTitle} />
    }
    sidebar={
      <>
        <CTA />
        <SecondaryMenu />
        <SecondaryMenu title={blockTitle} list={linkList} />
      </>
    }
    content={
      <>
        <CategoryListContent categoryList={data} />

      </>
    } >
  </Layout>

);

export default {
  title: 'Pages/HealthTopicLandingPage',
  component: HealthTopicLandingPage,
};
