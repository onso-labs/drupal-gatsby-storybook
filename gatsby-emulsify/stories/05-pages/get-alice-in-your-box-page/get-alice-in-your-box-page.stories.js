import React from 'react'
import { CTA } from '../../02-molecules/cta/cta.stories';
import { Layout } from '../../04-templates/layout.stories';
import { Title, paragraphContent, linkList, blockTitle } from './get-alice-in-your-box-page';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';
import { AffiliationForm } from '../../03-organisms/affiliation-form/affiliation-form.stories';
import { SecondaryMenu } from '../../03-organisms/secondary-menu/secondary-menu.stories';

export const GetAliceInYourBoxPage = ({ titleData = Title, paragraphValue = paragraphContent }) => (
  <Layout
    pageTitle={
      <PageTitle PageTitle={titleData.pageTitle} />
    }
    sidebar={
      <>
        <CTA />
        <SecondaryMenu />
        <SecondaryMenu title={blockTitle} list={linkList} />
      </>
    }
    content={
      <>
        <div className="page-content" dangerouslySetInnerHTML={{ __html: paragraphValue.paragraph }} />
        <AffiliationForm />
      </>
    } >
  </Layout>
)

export default {
  title: 'Pages/GetAliceInYourBoxPage',
  component: GetAliceInYourBoxPage,
};