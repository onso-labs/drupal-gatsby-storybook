
const Title = {
  pageTitle: "Get Alice! In Your Box"
}

const paragraphContent = {
  paragraph: "<p>There's no need to remember to check in each week, because <i>Go Ask Alice!</i> can come to you. Just share a little bit about yourself by completing the subscription form and you’ll be all set to get your fix of new and updated Q&As in an email newsletter every Friday. You'll also get links to <i>Go Ask Alice!</i>'s newest Theme of the Week (a collection of topical questions) and a bi-weekly poll, plus occasional GAA! news you can use to meet your personal health goals. Bet you didn’t think all of that could fit into one email!</p>"
}
const linkList = [
  {
    "entity": {
      "entityLabel": "Get Alice! In Your Box",
      "entityUrl": {
        "routed": true,
        "path": "#"
      }
    }
  }
]
const blockTitle = "Subscribe to Alice!"
export { Title, paragraphContent, linkList, blockTitle }
