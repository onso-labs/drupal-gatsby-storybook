import React from 'react'
import { CTA } from '../../02-molecules/cta/cta.stories';
import { Layout } from '../../04-templates/layout.stories';
import { Quizzes } from '../../03-organisms/quizzes/quizzes.stories';
import { WeeklyPoll } from '../../03-organisms/weekly-poll/weekly-poll.stories';
import { Title, weeklyPoll, weeklyData } from './accessibility-quiz';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';

export const AccessibilityQuizPage = ({ titleData = Title, weeklyDatas = weeklyData }) => (
  <div className="all-about-alice-page">
    <Layout
      pageTitle={
        <PageTitle PageTitle={titleData.pageTitle} />
      }
      sidebar={
        <>
          <CTA />
        </>
      }
      content={
        <>
          <Quizzes />
          <WeeklyPoll weeklyPolls={weeklyPoll} />
          {weeklyDatas.map((item, i) => {
            return (
              <div>
                {/* <WeeklyPollResult resultDatas={item.resultData} /> */}
                {/* <SocialShare socialMenu={item.socialShareMenu} /> */}
              </div>
            )
          })}
        </>
      } >
    </Layout>
  </div>

)


export default {
  title: 'Pages/AccessibilityQuiz',
  component: AccessibilityQuizPage,
};