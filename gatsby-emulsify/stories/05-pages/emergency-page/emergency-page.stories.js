import React from 'react'
import { CTA } from '../../02-molecules/cta/cta.stories';
import { HealthResources } from '../../03-organisms/health-resources/health-resources.stories';
import { Title } from './emergency-page';
import { Layout } from '../../04-templates/layout.stories';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';
import { SocialShare } from '../../02-molecules/menus/socialshare/social-share-menu.stories';

export const EmergencyPage = ({ titleData = Title }) => (

  <Layout
    pageTitle={
      <PageTitle PageTitle={titleData.pageTitle} />
    }
    sidebar={
      <>
        <CTA />
      </>
    }
    content={
      <>
        <HealthResources />
        <div className="social-menus">
          <SocialShare />
        </div>
      </>
    } >
  </Layout>
);

export default {
  title: 'Pages/EmergencyPage',
  component: EmergencyPage,
};