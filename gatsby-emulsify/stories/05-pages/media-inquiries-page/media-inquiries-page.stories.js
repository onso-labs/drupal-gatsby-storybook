import React from 'react'
import { CTA } from '../../02-molecules/cta/cta.stories';
import { CustomForm } from '../../03-organisms/custom-form/custom-form.stories';
import { Layout } from '../../04-templates/layout.stories';
import { Title, pageContent, linkList, blockTitle } from './media-inquiries-page';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';
import { SecondaryMenu } from '../../03-organisms/secondary-menu/secondary-menu.stories';

export const MediaInquiriesPage = ({ titleData = Title }) => (
  <Layout
    pageTitle={
      <PageTitle PageTitle={titleData.pageTitle} />
    }
    sidebar={
      <>
        <CTA />
        <SecondaryMenu />
        <SecondaryMenu title={blockTitle} list={linkList} />
      </>
    }
    content={
      <>
        <div class="page-content">
          <div dangerouslySetInnerHTML={{ __html: pageContent.contentData }} />
        </div>
        <CustomForm />
      </>
    } >
  </Layout>
);

export default {
  title: 'Pages/MediaInquiriesPage',
  component: MediaInquiriesPage,
};
