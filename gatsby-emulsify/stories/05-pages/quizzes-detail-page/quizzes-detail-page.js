const Title = {
  pageTitle: "Eat Well, Feel Well?: A Nutritiously Delicious Quiz"
}
const quizTitle = {
  quizTitle: "",
  quizReadMore: ""
}
const quizData = {
  paragraphContent: "<p>Test your knowledge to see if you know what it takes to maintain a healthy diet.</p>"
}

export { Title, quizTitle, quizData }
