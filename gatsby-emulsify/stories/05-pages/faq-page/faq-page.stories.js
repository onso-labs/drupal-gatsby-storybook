import React from 'react'
import { CTA } from '../../02-molecules/cta/cta.stories';
import { FAQsContent } from '../../03-organisms/faq-content/faq-content.stories';
import { Layout } from '../../04-templates/layout.stories';
import { Title, blockTitle, linkList } from './faq-page';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';
import { SocialShare } from '../../02-molecules/menus/socialshare/social-share-menu.stories';
import { SecondaryMenu } from '../../03-organisms/secondary-menu/secondary-menu.stories';

export const FAQsPage = ({ data, titleData = Title }) => (

  <Layout
    crumbLabel="About Alice!"
    link="/about-alice/all-about"
    title="FAQs"
    pageTitle={
      <PageTitle PageTitle={titleData.pageTitle} />
    }
    sidebar={
      <>
        <CTA />
        <SecondaryMenu />
        <SecondaryMenu title={blockTitle} list={linkList} />
      </>
    }
    content={
      <>
        <FAQsContent faqs={data} />
        <div className="social-menus">
          <SocialShare pdfUrl={`
              ${config.REST_API_URL}print/view/pdf/gq_faqs/page_2?`} />
        </div>
      </>
    } >
  </Layout>
);

export default {
  title: 'Pages/FAQsPage',
  component: FAQsPage,
};
