import React from 'react'
import { Layout } from '../../04-templates/layout.stories';
import { Title, linkList, blockTitle } from './ask-your-question-page';
import { SecondaryMenu } from '../../03-organisms/secondary-menu/secondary-menu.stories';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';
import { AskYourQuestionForm } from '../../03-organisms/ask-your-question-form/ask-your-question-form.stories';
import { AskYourQuestionTop } from '../../03-organisms/ask-your-question-top/ask-your-question-top.stories';


class Form extends React.Component {

  constructor(props) {
    super(props);
    this.state = { show: false }
  }

  change = (value) => {
    this.setState({ show: true })
  }

  render() {
    return (
      <Layout
        pageTitle={
          <PageTitle PageTitle={this.props.titleData.pageTitle} />
        }
        sidebar={
          <>
            <SecondaryMenu />
            <SecondaryMenu title={blockTitle} list={linkList} />
          </>
        }
        content={
          <>
            {this.state.show == true ?
              <AskYourQuestionForm /> :
              <AskYourQuestionTop showForm={this.change} />
            }



          </>
        } >
      </Layout>
    )
  }
}

export const AskYourQuestionPage = ({ data = Title }) => (
  <Form titleData={data} />
)


export default {
  title: 'Pages/AskYourQuestionPage',
  component: AskYourQuestionPage,
};