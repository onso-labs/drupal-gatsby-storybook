import React from 'react'
import { CTA } from '../../02-molecules/cta/cta.stories';
import { SecondaryMenu } from '../../03-organisms/secondary-menu/secondary-menu.stories';
import { ListContentBlock } from '../../03-organisms/list-content-block/list-content-block.stories';
import { linkList, blockTitle, Title } from './search-page';
import { Layout } from '../../04-templates/layout.stories';
import { PageTitle } from '../../02-molecules/page-title/page-title.stories';
import { SearchForm } from '../../03-organisms/search-form/search-form.stories';
export const SearchPage = ({ fields, titleData = Title }) => (
  <div className="search-page">
    <Layout
      crumbLabel="search"
      link="/content"
      pageTitle={
        <PageTitle PageTitle={titleData.pageTitle} />
      }
      sidebar={
        <>
          <CTA />
          <SecondaryMenu />
          <SecondaryMenu title={blockTitle} list={linkList} />
        </>
      }
      content={
        <>
          <SearchForm />
          <h2>Search Results</h2>
          <div className="search-result-list">
            <ListContentBlock searchlist={fields} />
          </div>
        </>
      } >
      <PageTitle PageTitle={titleData.pageTitle} />
    </Layout >
  </div>
);

export default {
  title: 'Pages/SearchPage',
  component: SearchPage,
};