
import React from 'react';
import { Button } from 'reactstrap';
import { checkPropTypes } from 'prop-types';

export const Primarybtn = ({ buttonTextValue = "Primary Button", btnClass = "btn btn--primary", value }) => <button className={btnClass} onClick={(e) => { value(e) }}> {buttonTextValue} </button>;

export const Secondarybtn = ({ buttonTextValue = "Secondary Button", btnClass = "btn btn--secondary" }) => <button className={btnClass}> {buttonTextValue} </button>;

export const Successbtn = ({ buttonTextValue = "Success Button", btnClass = "btn btn--success",value, }) => <button className={btnClass} onClick={(e) => { value(e) }}> {buttonTextValue} </button>;

export default {
  title: 'Atoms/Button',
  component: Button,
};