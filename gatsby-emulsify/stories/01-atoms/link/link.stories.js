import React from 'react';
import { Link } from 'gatsby';
// import { Link } from "react-router-dom";

export const Links = ({ linkTextValue = "test one", linkRedirectTo = "#", linkClass = "link" }) => <Link to={linkRedirectTo} className={linkClass}> {linkTextValue} </Link>;

export default {
  title: 'Atoms/Link',
  component: Links,
};