import React from 'react';
import imageFile from '../../../images/logo-main.svg';

export const Image = ({
  imgSrc = imageFile, imgAlt = "dummy image", imgClass = "image"
}) => <img src={imgSrc} alt={imgAlt} className={imgClass} />;

export default {
  title: 'Atoms/Images',
  component: Image,
};