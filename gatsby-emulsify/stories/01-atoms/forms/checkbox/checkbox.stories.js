import React from 'react';
import { CustomInput, Label } from 'reactstrap';
import { checkbox } from './checkbox';

export const Checkbox = ({ checkboxs = checkbox, onchange, checked, label = "Checkbox", name, value }) => (

    <CustomInput type="checkbox" checked={checked} id={label} name={name} label={label} value={value} onChange={(e) => { onchange(e.target.value) }} />

);

export default {
    title: 'Atoms/Form/Checkbox',
    component: Checkbox,
};