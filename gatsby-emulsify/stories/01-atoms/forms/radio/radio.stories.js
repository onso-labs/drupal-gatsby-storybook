import React from 'react';
import { CustomInput, Label } from 'reactstrap';
import { radio } from './radio.js';

export const Radio = ({ radios = radio, onchange, checked, label = "Radio", name, value }) => (

    <CustomInput type="radio" checked={checked} id={label} name={name} label={label} value={value} onChange={onchange} value={value} />

);

export default {
    title: 'Atoms/Form/Radio',
    component: Radio,
};