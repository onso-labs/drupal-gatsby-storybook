import React from 'react';

export const Paragraph = ({ paragraphContent = "", paragraphClass = "paragraph" }) => <div className={paragraphClass} dangerouslySetInnerHTML={{ __html: paragraphContent }} />;


export default {
  title: 'Atoms/Text/Paragraph',
  component: Paragraph,
};
