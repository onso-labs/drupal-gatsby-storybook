import { addDecorator } from '@storybook/react';
import { useEffect } from '@storybook/client-api';
// import Twig from 'twig';
// import { setupTwig } from './setupTwig';

// Theming
import emulsifyTheme from './emulsifyTheme';

// GLOBAL CSS
import '../stories/style.scss';

// If in a Drupal project, it's recommended to import a symlinked version of drupal.js.
// import './_drupal.js';

export const parameters = {
  options: {
    theme: emulsifyTheme,
  },
};

global.__PATH_PREFIX__ = '';

const req = require.context('../stories', true, /.stories.js$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}
// addDecorator deprecated, but not sure how to use this otherwise.
// addDecorator((storyFn) => {
//   useEffect(() => Drupal.attachBehaviors(), []);
//   return storyFn();
// });

// setupTwig(Twig);